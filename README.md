# Cacao Admin API
Web API built with Express and Awilix (to resolve containers and controllers), using dependency injection and CLEAN architecture concepts (controller / use-case / domain). Sequelize was used for the SQL ORM and Mongoose for MongoDB.
This app handles the resources for an ERP system.

### API Version

:dizzy: `/api/v1`

:sparkles: Release version: 1.0.0

### Requirements

- **Node.js**

You can download the lasted Node version [here](https://nodejs.org/es/)

- **Erlang**

You can download the lasted Erlang version [here](https://www.erlang.org/)

- **RabbitMQ**

You can download the lasted RabbitMQ version [here](https://www.rabbitmq.com/)

### Setup

- Install dependencies:

```bash

npm i

```

- Setup .env file:

You should follow `.env.example` to see required variables

- Setup databases:

1. Create your databases, under the same name you put in `.env`. _Note: dont't worry about tables or collections, the API will set them up._

2. Create your no sql database, under the same name you put in `.env`

- Setup first user:

When setting up from scratch, you have to create your first user:

1. Create a `user.json` file with your user data inside src. _(there's a `user.example.json` file to copy its format into `src` directory)_

2. Run the API (`npm run dev`). Then, make a request to `API_ROOT/API_PREFIX/setup-user` with GET method.

3. Check the response to see if the result is successful, otherwise an error message will be displayed.

4. Now, you can start using our API by getting a token from `API_ROOT/API_PREFIX/auth/access`, providing your _user_ and _email_.

### Scripts

- Run the project locally:

```bash

npm run dev

```

- Build only:

```bash

npm run build

```

- Run build with nodemon:

```bash

npm run watch:dev

```

### API Reference

#### :bulb: Go to /api/v1/api-docs for documentation.
