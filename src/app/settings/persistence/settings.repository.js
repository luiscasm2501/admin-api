const mongoRepo = require('../../common/persistence/mongo/mongoRepo')

module.exports = class settingsRepository extends mongoRepo {
	constructor(nosqlDb) {
		super(nosqlDb, 'settings')
	}
}
