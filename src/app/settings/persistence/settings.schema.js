function makeSchema(Schema) {
	const settingsSchema = new Schema(
		{
			entities: {
				type: Array,
			},
			type: {
				type: String,
			},
			name: {
				type: String,
				required: true,
			},
		},
		{ strict: false }
	)
	return {
		name: 'settings',
		schema: settingsSchema,
	}
}
module.exports = makeSchema
