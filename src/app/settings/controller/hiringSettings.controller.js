const { createController } = require('awilix-router-core')
const NotFoundError = require('../../common/controllers/error-handling/notFoundError')

const consultControllers = (getSettingsService, updateSettingsService) => ({
	getHiringSettings: async (req, res, next) => {
		try {
			const result = await getSettingsService.getSetting('hiring-notifications')
			res.send(result)
		} catch (error) {
			next(error)
		}
	},
	updateHiringSettings: async (req, res, next) => {
		try {
			const result = await updateSettingsService.updateSettings(req.body)
			res.send(result)
		} catch (error) {
			next(error)
		}
	},
})

module.exports = createController(consultControllers)
	.prefix('/settings/hirings')
	.get('', 'getHiringSettings')
	.put('', 'updateHiringSettings')
