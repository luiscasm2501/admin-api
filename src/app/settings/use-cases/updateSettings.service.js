module.exports = class updateSettingsService {
	constructor(settingsRepository) {
		this.settingsRepository = settingsRepository
	}
	async updateSettings(settings) {
		return await this.settingsRepository.update(settings)
	}
}
