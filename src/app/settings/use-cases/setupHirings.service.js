module.exports = class setupHiringsService {
	constructor(settingsRepository, roleRepository) {
		this.settingsRepository = settingsRepository
		this.roleRepository = roleRepository
	}
	async setupHirings() {
		const hiringSettings = await this.settingsRepository.getAll({
			conditions: { name: 'hiring-notifications' },
		})
		if (hiringSettings && hiringSettings.length == 0) {
			const role = await this.roleRepository.getOne({
				conditions: { name: 'master' },
			})
			this.settingsRepository.create({
				name: 'hiring-notifications',
				type: 'role',
				entities: [role.roleId],
				advance_days: 3,
			})
		}
	}
}
