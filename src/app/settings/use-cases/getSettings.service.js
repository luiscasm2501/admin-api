module.exports = class getSettingsService {
	constructor(settingsRepository) {
		this.settingsRepository = settingsRepository
	}
	async getSettings(options) {
		return await this.settingsRepository.getAll(options)
	}
	async getSetting(name) {
		return await this.settingsRepository.getOne({ conditions: { name } })
	}
}
