const categories = {
	template: {
		RENEWAL: 'RENEWAL',
		REGULAR: 'REGULAR',
		CONTRACT: 'CONTRACT',
		TEMPLATE: 'TEMPLATE',
		AFFILIATE: 'AFFILIATE',
		OTHER: 'OTHER',
	},
	ticket: {
		'DATA (DATOS)': {
			description:
        'Issues encountered when loading, adding or modifying information. (Problemas encontrados al registrar o consultar información)',
		},
		'UI (INTERFAZ DE USUARIO)': {
			description:
        'Related to issues encountered while using the client application. (Problemas dentro de las páginas de la aplicación)',
		},
		'PRODUCTS (PRODUCTOS)': {
			description:
        'Issues with acquired products or services. (Problemas con productos o servicios adquiridos)',
		},
		'OTHER (OTROS)': {
			description:
        'Miscellaneous problems or questions. (Consultas y preblemas misceláneos)',
		},
	},
}
module.exports = categories
