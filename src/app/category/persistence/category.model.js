const { DataTypes } = require('sequelize')
const logger = require('../../common/controllers/logger/logger')
const categories = require('../domain/categories')
//PENDING: add status FK

const categoryKinds = ['ticket', 'template']

function makeModel(apiDb) {
	const Category = apiDb.define(
		'Category',
		{
			name: {
				type: DataTypes.STRING(50),
				allowNull: false,
			},
			description: {
				type: DataTypes.STRING(500),
				allowNull: true,
			},
			kind: {
				type: DataTypes.STRING(50),
				allowNull: false,
				validate: {
					isIn: [categoryKinds],
				},
			},
			categoryId: {
				type: DataTypes.UUID,
				allowNull: false,
				primaryKey: true,
				defaultValue: DataTypes.UUIDV4,
			},
		},
		{
			timestamps: false,
			indexes: [{ unique: true, fields: ['name', 'kind'] }],
		}
	)
	Category.sync()
		.then(async () => {
			const statusCount = await Category.count()
			let categoryNames = 0
			Object.keys(categories).forEach((key) => {
				categoryNames += Object.keys(categories[key]).length
			})
			if (statusCount < categoryNames) {
				Object.keys(categories).forEach((KindKey) => {
					Object.entries(categories[KindKey]).forEach(([key, value]) => {
						let description
						if (typeof value == 'object') {
							description = value.description
							key = value.name || key
						}
						Category.create({
							name: key,
							kind: KindKey,
							...(description && { description }),
						}).catch((e) =>
							logger.warn(`Couldn't create "${key}" category [${e.message}]`)
						)
					})
				})
			}
		})
		.catch((err) => logger.error(err.message))
	return Category
}

module.exports = makeModel
