module.exports = class createCategoryService {
	constructor(categoriesRepository) {
		this.categoriesRepository = categoriesRepository
	}

	async createCategory(cat) {
		cat.kind = cat.kind || 'ticket'
		return await this.categoriesRepository.create(cat)
	}
}
