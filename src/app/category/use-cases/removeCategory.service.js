const NotFoundError = require('../../common/controllers/error-handling/notFoundError')
const ValidationException = require('../../common/domain/validationException')

const CATEGORY_CHECKS = ['Tickets', 'SupportTips']

module.exports = class updateCategoryService {
	constructor(categoriesRepository) {
		this.categoriesRepository = categoriesRepository
	}

	async removeCategory(catId) {
		const category = await this.categoriesRepository.getById(catId)
		if (category) {
			await this.checkRelations(category)

			return await this.categoriesRepository.remove(catId)
		}
		throw new NotFoundError('category')
	}
	async checkRelations(category) {
		for await (let entity of CATEGORY_CHECKS) {
			const usedCategory = await category[`get${entity}`]()
			if (usedCategory.length) {
				throw new ValidationException(
					`${category.name} is related to some ${entity}`
				)
			}
		}
		return true
	}
}
