module.exports = class updateCategoryService {
	constructor(categoriesRepository) {
		this.categoriesRepository = categoriesRepository
	}

	async updateCategory(cat) {
		delete cat.kind
		return await this.categoriesRepository.update(cat)
	}
}
