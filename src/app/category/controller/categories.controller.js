const { createController } = require('awilix-router-core')
const NotFoundError = require('../../common/controllers/error-handling/notFoundError')

const consultControllers = (
	getCategoriesService,
	createCategoryService,
	updateCategoryService,
	removeCategoryService
) => ({
	getAllCategories: async (req, res, next) => {
		try {
			const result = await getCategoriesService.getCategories(req.query)
			next(result)
		} catch (error) {
			next(error)
		}
	},
	createCategory: async (req, res, next) => {
		try {
			const result = await createCategoryService.createCategory(req.body)
			res.send(result)
		} catch (error) {
			next(error)
		}
	},
	updateCategory: async (req, res, next) => {
		try {
			const result = await updateCategoryService.updateCategory(req.body)
			res.send(result)
		} catch (error) {
			next(error)
		}
	},
	removeCategory: async (req, res, next) => {
		try {
			const catId = req.params.id
			const result = await removeCategoryService.removeCategory(catId)
			if (result) res.send(`Category ${catId} removed`)
			else throw new NotFoundError(`Category ${catId}`)
		} catch (error) {
			next(error)
		}
	},
})

module.exports = createController(consultControllers)
	.prefix('/categories')
	.get('', 'getAllCategories')
	.post('', 'createCategory')
	.put('/:id', 'updateCategory')
	.delete('/:id', 'removeCategory')
