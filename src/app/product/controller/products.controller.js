const { createController } = require('awilix-express')
const NotFoundError = require('../../common/controllers/error-handling/notFoundError')

const API = (registerProductService, getAllProductsService) => ({
	registerProduct: (req, res, next) => {
		registerProductService
			.registerProduct(req.body)
			.then((result) => {
				res.send(result)
			})
			.catch((err) => {
				next(err)
			})
	},
	getAllProducts: (req, res, next) => {
		getAllProductsService
			.getAllProducts(req.query)
			.then((result) => {
				//res.send(result)
				//next(result)
				next(result)
			})
			.catch((err) => {
				next(err)
			})
	},
	getProduct: (req, res, next) => {
		const id = req.params.id
		getAllProductsService
			.getProduct(id)
			.then((result) => {
				if (result) {
					res.status(200).send(result)
				} else {
					next(new NotFoundError(`Product ${id}`))
				}
			})
			.catch((err) => {
				next(err)
			})
	},
})

module.exports = createController(API)
	.prefix('/products')
	.post('', 'registerProduct')
	.get('', 'getAllProducts')
	.get('/:id', 'getProduct')

/**
 * @swagger
 * /products:
 *  post:
 *    tags: [Products]
 *    summary: register a new product record
 *    requestBody:
 *      required: true
 *      content:
 *        application/json:
 *          schema:
 *            $ref: '#/components/schemas/Product'
 *    responses:
 *      200:
 *        description: created product record
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/Product'
 *  get:
 *    tags: [Products]
 *    summary: get list of products
 *    parameters:
 *      - $ref: '#/components/parameters/page'
 *      - $ref: '#/components/parameters/size'
 *      - $ref: '#/components/parameters/sort'
 *    responses:
 *      200:
 *        description: list of product records
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/Product'
 *              type: array
 * /products/{productId}:
 *  get:
 *    tags: [Products]
 *    summary: get list of products
 *    parameters:
 *      - $ref: '#/components/parameters/productId'
 *    responses:
 *      200:
 *        description: found product record
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/Product'
 */
