const { createController } = require('awilix-express')
const NotFoundError = require('../../common/controllers/error-handling/notFoundError')
const UpdateError = require('../../common/controllers/error-handling/updateError')

const ValidationException = require('../../common/domain/validationException')

const API = (updateProductService, deleteProductService) => ({
	updateProduct: (req, res, next) => {
		if (req.params.id === req.body.productId) {
			updateProductService
				.updateProduct(req.body)
				.then((result) => {
					res.send(result)
				})
				.catch((err) => {
					next(err)
				})
		} else {
			next(new UpdateError(req.params.id, req.body.productId))
		}
	},
	deleteProduct: (req, res, next) => {
		const id = req.params.id
		if (id)
			deleteProductService
				.deleteProduct(id)
				.then((result) => {
					if (result) {
						res.status(200).send(`Succesfully deleted product ${id}`)
					} else {
						next(new NotFoundError(`Product ${id}`))
					}
				})
				.catch((err) => {
					next(err)
				})
		else throw new ValidationException('Missing id')
	},
	restoreProduct: (req, res, next) => {
		const id = req.params.id
		if (id)
			deleteProductService
				.restoreProduct(id)
				.then((result) => {
					if (result) {
						res.status(200).send(`Succesfully restored product ${id}`)
					} else {
						next(new NotFoundError(`Product ${id}`))
					}
				})
				.catch((err) => {
					next(err)
				})
		else throw new ValidationException('Missing id')
	},
})

module.exports = createController(API)
	.prefix('/products')
	.put('/:id', 'updateProduct')
	.delete('/:id', 'deleteProduct')
	.patch('/:id', 'restoreProduct')

/**
 * @swagger
 * /products/{productId}:
 *  put:
 *    tags: [Products]
 *    summary: update a product record by id
 *    parameters:
 *      - $ref: '#/components/parameters/productId'
 *    requestBody:
 *      required: true
 *      content:
 *        application/json:
 *          schema:
 *            $ref: '#/components/schemas/Product'
 *    responses:
 *      200:
 *        description: updated product record
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/Product'
 *
 *  delete:
 *    tags: [Products]
 *    summary: remove a product record
 *    description: On the first call, it marks the product as deleted. If used after being marked, it permanently deletes it.
 *    parameters:
 *      - $ref: '#/components/parameters/productId'
 *    responses:
 *      200:
 *        description: product sucessfully deleted
 *  patch:
 *    tags: [Products]
 *    summary: restores a product marked as "deleted" to its original state
 *    parameters:
 *      - $ref: '#/components/parameters/productId'
 *    responses:
 *      200:
 *        description: product successfully restored
 */
