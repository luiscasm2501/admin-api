class getAllProductsService {
	constructor(productsRepository, sendNotificationService) {
		this.productsRepository = productsRepository
		this.sendNotificationService = sendNotificationService
	}
	async getAllProducts(options) {
		return this.productsRepository.getAll({
			...options,
			include: ['Status'],
		})
	}
	async getProduct(id) {
		return this.productsRepository.getById(id, { include: ['Status'] })
	}
}
module.exports = getAllProductsService
