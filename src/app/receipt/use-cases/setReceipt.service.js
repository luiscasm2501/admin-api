const statuses = require('../../common/persistence/status/statuses')
const receiptEntity = require('../domain/receiptEntity')

module.exports = class setReceiptService {
	constructor(
		receiptRepository,
		invoiceRepository,
		contractRepository,
		hiringRepository,
		createFileReceiptService,
		updateReceiptService,
		createFileMetaService,
		bus
	) {
		this.receiptRepository = receiptRepository
		this.invoiceRepository = invoiceRepository
		this.createFileReceiptService = createFileReceiptService
		this.updateReceiptService = updateReceiptService
		this.contractRepository = contractRepository
		this.hiringRepository = hiringRepository
		this.createFileMetaService = createFileMetaService
		this.repo = undefined
		this.bus = bus
	}

	async setReceipt(
		receipt,
		invoiceId,
		invoiceType = 'ContractInvoices',
		proxy = 'contract'
	) {
		const invoice = await this.invoiceRepository.getById(invoiceId, {
			include: ['Status', invoiceType],
		})
		const validReceipt = receiptEntity(receipt, invoice)
		const result = await this.receiptRepository.create({
			...validReceipt,
			invoiceFk: invoiceId,
			automatic: false,
		})
		receipt.receiptId = result.receiptId
		await this.invoiceRepository.updateStatus(
			invoice.invoiceId,
			statuses.PENDING_REVIEW
		)
		const isHiring =
      invoiceType.includes('Hiring') || invoiceType.includes('Affiliate')
		this.repo = isHiring ? this.hiringRepository : this.contractRepository
		if (
			proxy !== 'contractAffiliate' &&
      invoice[invoiceType] &&
      invoice[invoiceType][0]
		) {
			await this.repo.updateStatus(
				invoice[invoiceType][0][proxy + 'Fk'],
				statuses.REVIEW_PAYMENT
			)
		}
		const fileReceipt = await this.createFileReceiptService.createFileReceipt(
			result.receiptId
		)

		await this.createFileMetaService.createFileMeta({
			fileReceiptId: fileReceipt.fileReceiptId,
			...receipt,
		})
		if (isHiring) {
			this.updateReceiptService.updateReceipt(
				{ ...result.dataValues, approved: true },
				invoiceId,
				invoiceType,
				proxy
			)
			this.bus.emit(proxy + 'ReceiptCreated', {
				fileReceiptId: fileReceipt.fileReceiptId,
				...receipt,
				Invoice: invoice,
				[invoiceType]: invoice[invoiceType][0],
			})
		} else {
			// If file receipt was created and it's a contract payment, create notification
			this.bus.emit('fileReceiptCreated', {
				fileReceiptId: fileReceipt.fileReceiptId,
				...receipt,
				Invoice: invoice,
			})
		}
		return result
	}
}
