const { QueryTypes } = require('sequelize')

module.exports = class getDeployReceiptInfoService {
	constructor(apiDb) {
		this.db = apiDb
	}
	async getDeployReceiptInfo(receiptId) {
		try {
			const info = await this.db.query(
				`select r.amount, s.name as "product", c.name as "client"
          from "MembershipInvoices" mi join "Invoices" i on mi."invoiceFk" = i."invoiceId"
          join "Receipts" r on r."invoiceFk" = mi."invoiceFk"
          join "Memberships" m on m."membershipId" = mi."membershipFk"
          join "ServicePaymentTimes" spt on spt."servicePaymentId" = m."servicePaymentTimeFk"
          join "Services" s on s."serviceId" = spt."serviceFk"
          join "Clients" c on c."clientId" = m."clientFk"
          where r."receiptId" = :receiptId`,
				{
					replacements: { receiptId: receiptId },
					type: QueryTypes.SELECT,
					plain: true,
					raw: true,
				}
			)
			return info
		} catch (error) {
			return undefined
		}
	}
}
