const ValidationException = require('../../common/domain/validationException')
const statuses = require('../../common/persistence/status/statuses')

module.exports = class updateReceiptService {
	constructor(
		receiptRepository,
		invoiceRepository,
		contractRepository,
		hiringRepository,
		bus
	) {
		this.receiptRepository = receiptRepository
		this.invoiceRepository = invoiceRepository
		this.contractRepository = contractRepository
		this.hiringRepository = hiringRepository
		this.repo = undefined
		this.bus = bus
	}

	async updateReceipt(
		receipt = { receiptId, approved: true },
		invoiceId,
		invoiceType = 'ContractInvoices',
		proxy = 'contract'
	) {
		const isContract = proxy == 'contract'
		this.repo = !isContract ? this.hiringRepository : this.contractRepository
		const invoice = await this.invoiceRepository.getById(invoiceId, {
			include: ['Status', invoiceType, ['Receipts', ['Status']]],
		})
		const fullReceipt = await this.receiptRepository.getById(receipt.receiptId)
		if (invoice.Status?.name !== statuses.PENDING_REVIEW) {
			throw new ValidationException(
				'this invoice is not marked for reviewing receipts'
			)
		}
		if (receipt.approved) {
			return await this.handleApproved(
				fullReceipt.dataValues,
				invoice,
				invoiceType,
				proxy,
				isContract
			)
		}
		return await this.handleRejected(fullReceipt.dataValues, invoice)
	}
	async handleApproved(
		receipt,
		invoice,
		invoiceType = 'ContractInvoices',
		proxy = 'contract',
		isContract = true
	) {
		await this.receiptRepository.updateStatus(
			receipt.receiptId,
			statuses.APPROVED
		)

		const contract = await this.repo.getById(
			invoice[invoiceType][0][`${proxy}Fk`]
		)
		if (contract?.currentPayment >= 0) {
			const updatedContract = await this.repo.update({
				[`${proxy}Id`]: contract[`${proxy}Id`],
				currentPayment: contract.currentPayment + receipt.amount,
			})
			let contractStatus = statuses.ACTIVE
			if (
				updatedContract &&
        updatedContract.currentPayment >= updatedContract.totalPayment
			) {
				contractStatus = statuses.CLOSED
			}
			await this.repo.updateStatus(contract[`${proxy}Id`], contractStatus)
		}
		let amount = receipt.amount
		invoice.Receipts.forEach((receipt) => {
			if (receipt.Status.name == statuses.APPROVED) {
				amount += receipt.amount
			}
		})
		const newInvoiceStatus =
      amount >= invoice.amount ? statuses.PAID : statuses.PARTIAL
		const result = await this.invoiceRepository.updateStatus(
			invoice.invoiceId,
			newInvoiceStatus
		)
		if (isContract) {
			this.bus.emit('receiptProcessed', receipt)
			this.bus.emit('receiptApproved', receipt)
		}
		return result
	}
	async handleRejected(receipt, invoice) {
		await this.receiptRepository.updateStatus(
			receipt.receiptId,
			statuses.REJECTED
		)
		const result = await this.invoiceRepository.updateStatus(
			invoice.invoiceId,
			statuses.SENT
		)
		this.bus.emit('receiptProcessed', receipt)

		return result
	}
}
