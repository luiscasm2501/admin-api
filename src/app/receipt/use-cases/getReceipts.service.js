module.exports = class getReceiptsService {
	constructor(receiptRepository) {
		this.receiptRepository = receiptRepository
	}

	async getReceipts(options) {
		return await this.receiptRepository.getAll(options)
	}
	async getReceiptsInvoice(invoiceId, options) {
		return await this.receiptRepository.getAll({
			...options,
			invoiceFk: invoiceId,
			include: ['Status', 'PaymentMethod'],
		})
	}

	async getContractReceipts(
		contractId,
		options,
		invoiceType = 'ContractInvoices',
		proxy = 'contract'
	) {
		const receipts = await this.receiptRepository.getAll({
			include: [
				'Status',
				{
					model: 'Invoice',
					required: true,
					include: [
						{
							model: invoiceType,
							required: true,
							conditions: {
								[`${proxy}Fk`]: contractId,
							},
						},
					],
				},
			],
			attr: ['amount', 'receiptId', 'concept', 'paymentMethod'],
			...options,
		})
		return receipts
	}

	async getAffiliateReceipts(affiliateId, options) {
		return await this.receiptRepository.getAll({
			include: [
				{
					model: 'Invoice',
					required: true,
					include: [
						{
							model: 'AffiliateInvoices',
							required: true,
							include: [
								{
									model: 'ContractAffiliate',
									required: true,
									conditions: { affiliateFk: affiliateId },
								},
							],
						},
					],
				},
			],
			...options,
		})
	}
}
