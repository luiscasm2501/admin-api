const { DateTime, Interval } = require('luxon')
const statuses = require('../../common/persistence/status/statuses')
const { sendEmail } = require('../../common/services/mailer/mailer')
const mapInvoiceInfo = require('../../invoice/domain/mapInvoiceInfo')

module.exports = class NotifyReceiptProcessedService {
	constructor(
		clientProductRepository,
		invoiceRepository,
		setInvoiceTokenService
	) {
		this.clientProductRepository = clientProductRepository
		this.invoiceRepository = invoiceRepository
		this.setInvoiceTokenService = setInvoiceTokenService
	}
	async notifyReceiptProcessed(receipt) {
		const invoice = await this.invoiceRepository.getById(receipt.invoiceFk, {
			include: ['Contracts', 'Status'],
		})
		if (invoice && invoice.Contracts?.length) {
			const clientProduct = await this.clientProductRepository.getAll({
				clientProductId: invoice.Contracts[0].clientProductFk,
				include: ['Client', 'Product'],
			})
			const end = DateTime.fromISO(invoice.deadline)
			const interval = Interval.fromDateTimes(DateTime.now(), end)
			const token = this.setInvoiceTokenService.setInvoiceToken(
				invoice,
				interval.isValid ? interval.length('hours') : undefined
			)
			const invoiceInfo = mapInvoiceInfo(invoice, clientProduct.rows[0], token)
			const status = invoice.Status.name
			const extra =
        status == statuses.PARTIAL ? 'Please keep in mind that you haven\'t fully paid this invoice. You can do  so as well by clicking below. / Tenga en cuenta que no ha pagado completamente esta factura. También puede hacerlo haciendo clic a continuación' : ''
			const fullInfo = {
				...receipt,
				...invoiceInfo,
				extra,
				receiptAmount: receipt.amount,
				button: 'Manage Payment / Administrar Pago',
				title: status == 'SENT' ? 'Payment Rejected / Pago Rechazado' : 'Payment Approved / Pago Aprobado',
			}
			await sendEmail(
				invoiceInfo.client.email,
				` Payment for invoice ${invoiceInfo.startDate} [${
					status == 'SENT' ? 'REJECTED' : status
				}] / Pago de factura ${invoiceInfo.startDate} [${
					status == 'SENT' ? 'RECHAZADA' : status
				}]`,
				status == 'PAID' || status == 'PARTIAL'
					? 'approved_receipt'
					: 'rejected_receipt',
				fullInfo
			)
		}
	}
}
