const NotFoundError = require('../../common/controllers/error-handling/notFoundError')

module.exports = class getFileReceiptMetaService {
	constructor(fileReceiptRepository, fileReceiptMetaRepository) {
		this.fileReceiptRepository = fileReceiptRepository
		this.fileReceiptMetaRepository = fileReceiptMetaRepository
	}

	async getFileReceiptMeta(receiptId) {
		let fileReceipts = await this.fileReceiptRepository.getAll({
			receiptFk: receiptId,
		})
		if (fileReceipts.count) {
			const id = fileReceipts.rows[0].fileReceiptId
			const fileMeta = await this.fileReceiptMetaRepository.getById(id)
			if (fileMeta) return fileMeta
		}
		throw new NotFoundError('File receipt')
	}
}
