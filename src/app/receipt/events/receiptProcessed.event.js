const trycatchevent = require('../../common/events/trycatchevent')

function registerEvent(bus, container) {
	const event = 'receiptProcessed'
	const ctx = 'notifyReceiptProcessedService'
	const context = container.resolve(ctx)
	bus.register(event, (receipt) => {
		trycatchevent(() => {
			context.notifyReceiptProcessed(receipt)
		})
	})
}
module.exports = registerEvent
