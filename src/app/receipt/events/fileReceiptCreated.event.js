const trycatchevent = require('../../common/events/trycatchevent')

function registerEvent(bus, container) {
	const event = 'fileReceiptCreated'
	const ctx = 'sendNotificationService'
	const sendNotificationService = container.resolve(ctx)
	const getContractsService = container.resolve('getContractsService')
	const getDeployReceiptInfoService = container.resolve(
		'getDeployReceiptInfoService'
	)
	bus.register(event, (receipt) => {
		trycatchevent(async () => {
			let client = undefined
			let product = undefined
			if (receipt.ContractInvoices) {
				const contract = await getContractsService.getContract(
					receipt.Invoice.ContractInvoices[0].contractFk
				)
				client = contract?.ClientProduct
					? contract.ClientProduct.Client.name
					: 'A client'
				product = contract?.ClientProduct
					? contract.ClientProduct.Product.name
					: 'product'
			} else {
				const info = await getDeployReceiptInfoService.getDeployReceiptInfo(
					receipt.receiptId
				)
				if (info) {
					client = info.client
					product = `${info.product} (deploy)`
				}
			}
			const notification = {
				title: `${client} sent a  $${receipt.amount} receipt for an invoice`,
				content: {
					description: `Check out the receipt sent by ${client} for their ${product} subscription.`,
					client: client || 'a client',
					product: product || 'products',
					amount: `$${receipt.amount}`,
					receivedOn: receipt.paymentDate,
				},
			}
			sendNotificationService.sendNotification(notification)
		})
	})
}
module.exports = registerEvent
