const trycatchevent = require('../../common/events/trycatchevent')

function registerEvent(bus, container) {
	const event = 'hiringReceiptCreated'
	const ctx = 'sendHiringReceiptService'
	const context = container.resolve(ctx)
	bus.register(event, (receipt) => {
		trycatchevent(() => {
			context.sendHiringReceipt(receipt)
		})
	})
}
module.exports = registerEvent
