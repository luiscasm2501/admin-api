//AffiliateInvoices
const trycatchevent = require('../../common/events/trycatchevent')

function registerEvent(bus, container) {
	const event = 'contractAffiliateReceiptCreated'
	const ctx = 'sendAffiliateReceiptService'
	const context = container.resolve(ctx)
	bus.register(event, (receipt) => {
		trycatchevent(() => {
			context.sendAffiliateReceipt(receipt)
		})
	})
}
module.exports = registerEvent
