const { createController } = require('awilix-router-core')
const ValidationException = require('../../common/domain/validationException')
const upload = require('../../files/config/files.config')

const registerControllers = (setReceiptService, getReceiptsService) => ({
	createReceipt: async (req, res, next) => {
		try {
			if (!req.file) {
				throw new ValidationException('A receipt file is required')
			}
			const result = await setReceiptService.setReceipt(
				{ ...req.body, ...req.file, token: req.headers.authorization },
				req.params.invoiceId,
				'AffiliateInvoices',
				'contractAffiliate'
			)
			next(result)
		} catch (error) {
			next(error)
		}
	},
	getAffiliateReceipts: async (req, res, next) => {
		try {
			const id = req.params.id
			const result = await getReceiptsService.getAffiliateReceipts(id,req.query)
			next(result)
		} catch (error) {
			next(error)
		}
	},
	
})
module.exports = createController(registerControllers)	
	.post('/contract-affiliates/:id/invoices/:invoiceId/receipts', 'createReceipt')
	.before(upload.single('receipt'))
	.get('/affiliates/:id/receipts', 'getAffiliateReceipts')

/**
 * @swagger
 *  
 * /affiliates/{employeeId}/receipts:
 *  get:
 *    tags: [Employees]
 *    summary: get receipts related to a certain affiliate
 *    parameters:
 *      - $ref: '#/components/parameters/employeeId'
 *    responses:
 *      200:
 *        description: employee record
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/Employee'
 * 
 */
	