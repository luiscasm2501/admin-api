const { createController } = require('awilix-router-core')
const NotFoundError = require('../../common/controllers/error-handling/notFoundError')

const consultControllers = (getReceiptsService, getFileReceiptMetaService) => ({
	getReceiptContract: async (req, res, next) => {
		try {
			const result = await getReceiptsService.getContractReceipts(
				req.params.contractId,
				req.query
			)
			next(result)
		} catch (error) {
			next(error)
		}
	},
	getReceiptHiring: async (req, res, next) => {
		try {
			const result = await getReceiptsService.getContractReceipts(
				req.params.hiringId,
				req.query,
				'HiringInvoices',
				'hiring'
			)
			next(result)
		} catch (error) {
			next(error)
		}
	},
	getReceiptAffiliate: async (req, res, next) => {
		try {
			const result = await getReceiptsService.getContractReceipts(
				req.params.contractId,
				req.query,
				'AffiliateInvoices',
				'contractAffiliate'
			)
			next(result)
		} catch (error) {
			next(error)
		}
	},
	getReceiptInvoice: async (req, res, next) => {
		try {
			const result = await getReceiptsService.getReceiptsInvoice(
				req.params.invoiceId,
				req.query
			)
			next(result)
		} catch (error) {
			next(error)
		}
	},
	getReceiptMeta: async (req, res, next) => {
		try {
			const result = await getFileReceiptMetaService.getFileReceiptMeta(
				req.params.id
			)
			res.send(result)
		} catch (error) {
			next(error)
		}
	},
})

module.exports = createController(consultControllers)
	.get('/contracts/:contractId/receipts', 'getReceiptContract')
	.get('/hirings/:hiringId/receipts', 'getReceiptHiring')
	.get('/contract-affiliates/:contractId/receipts', 'getReceiptAffiliate')
	.get('/receipts/:id/file', 'getReceiptMeta')
	.get('/invoices/:invoiceId/receipts', 'getReceiptInvoice')

/**
 * @swagger
 * /contracts/{contractId}/receipts:
 *  get:
 *    tags: [Invoices]
 *    summary: get receipts by contract
 *    parameters:
 *      - $ref: '#/components/parameters/contractId'
 *      - $ref: '#/components/parameters/page'
 *      - $ref: '#/components/parameters/size'
 *      - $ref: '#/components/parameters/sort'
 *    responses:
 *      200:
 *        description: list of receipts associated to a certain contract
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/Receipt'
 *              type: array
 * /invoices/{invoiceId}/receipts:
 *  get:
 *    tags: [Invoices]
 *    summary: get receipts by invoice
 *    parameters:
 *      - $ref: '#/components/parameters/invoiceId'
 *    responses:
 *      200:
 *        description: list of receipts created for an invoice
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/Receipt'
 *              type: array
 */
