const { createController } = require('awilix-router-core')

const registerControllers = (updateReceiptService) => ({
	updateReceipt: async (req, res, next) => {
		try {
			const result = await updateReceiptService.updateReceipt(
				{ ...req.body, receiptId: req.params.receiptId },
				req.params.invoiceId
			)
			res.send(result)
		} catch (error) {
			next(error)
		}
	},
})

module.exports = createController(registerControllers)
	.put('/invoices/:invoiceId/receipts/:receiptId', 'updateReceipt')
	.put(
		'/contracts/:contractId/invoices/:invoiceId/receipts/:receiptId',
		'updateReceipt'
	)

/**
 * @swagger
 * /invoices/{invoiceId}/receipts/{receiptId}:
 *  put:
 *    tags: [Invoices]
 *    summary: mark a receipt as approved or rejected.
 *    description: Used to indicate wether the receipt submitted by a client was a valid payment confirmation. Once approved or rejected, a receipt cannot be modified
 *    requestBody:
 *      required: true
 *      content:
 *        application/json:
 *          schema:
 *            type: object
 *            properties:
 *              approved:
 *                type: boolean
 *    parameters:
 *      - $ref: '#/components/parameters/invoiceId'
 *      - $ref: '#/components/parameters/receiptId'
 *    responses:
 *      200:
 *        description: receipt status updated
 *
 */
