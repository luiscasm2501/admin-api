const { createController } = require('awilix-router-core')
const ValidationException = require('../../common/domain/validationException')
const upload = require('../../files/config/files.config')

const registerControllers = (setReceiptService) => ({
	createReceipt: async (req, res, next) => {
		try {
			if (!req.file) {
				throw new ValidationException('A receipt file is required')
			}
			const result = await setReceiptService.setReceipt(
				{ ...req.body, ...req.file },
				req.params.invoiceId
			)
			next(result)
		} catch (error) {
			next(error)
		}
	},
})

module.exports = createController(registerControllers)
	.prefix('/invoices/:invoiceId/receipts')
	.post('', 'createReceipt')
	.before(upload.single('receipt'))

/**
 * @swagger
 * /invoices/{invoiceId}/receipts:
 *  post:
 *    tags: [Invoices]
 *    summary: create a receipt signifying a payment made by a client for a certain invoice
 *    requestBody:
 *      required: true
 *      content:
 *        application/json:
 *          schema:
 *            $ref: '#/components/schemas/Receipt'
 *    responses:
 *      200:
 *        description: receipt created succesfully
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/Receipt'
 */
