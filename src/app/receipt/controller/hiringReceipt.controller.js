const { createController } = require('awilix-router-core')
const ValidationException = require('../../common/domain/validationException')
const upload = require('../../files/config/files.config')

const registerControllers = (setReceiptService) => ({
	createReceipt: async (req, res, next) => {
		try {
			if (!req.file) {
				throw new ValidationException('A receipt file is required')
			}
			const result = await setReceiptService.setReceipt(
				{ ...req.body, ...req.file, token: req.headers.authorization },
				req.params.invoiceId,
				'HiringInvoices',
				'hiring'
			)
			next(result)
		} catch (error) {
			next(error)
		}
	},
})
module.exports = createController(registerControllers)
	.prefix('/hirings/:hiringId/invoices/:invoiceId/receipts')
	.post('', 'createReceipt')
	.before(upload.single('receipt'))
