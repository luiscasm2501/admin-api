const cron = require('node-cron')
const logger = require('../controllers/logger/logger')

function job(updateContractStatusService, expression) {
	cron.schedule(expression, () => {
		try {
			updateContractStatusService
				.updateContractStatus()
				.then(([started, expired]) => {
					logger.info(
						`running contract update job [started: ${started}, expired: ${expired}]`
					)
					updateContractStatusService
						.updateContractStatus(true)
						.then(([started, expired]) => {
							logger.info(
								`running hiring update job [started: ${started}, expired: ${expired}]`
							)
						})
				})
		} catch (error) {
			logger.error(`updating contract status error [${error.message}]`)
		}
	})
}
const service = 'updateContractStatusService'
module.exports = { job, service }
