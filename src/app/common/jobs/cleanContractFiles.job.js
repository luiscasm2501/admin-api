const cron = require('node-cron')
const cleanContractFiles = require('../../files/use-cases/cleanContractFiles')
const logger = require('../controllers/logger/logger')

cron.schedule('0 */6 * * *', () => {
	try {
		cleanContractFiles()
	} catch (error) {
		logger.error(`deleting contract files error [${error.message}]`)
	}
})
