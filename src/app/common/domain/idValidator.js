const { stringBetween } = require('./betweenValidator')

function validateId(id, name = 'id') {
	stringBetween(id, 1, Infinity, name)
}

module.exports = validateId
