const logger = require('../../controllers/logger/logger')
const axios = require('axios').default

module.exports = async function getImg(url, authorization) {
	const imgBuffer = await (
		await axios.get(url, {
			headers: { authorization: authorization },
			responseType: 'arraybuffer',
		})
	).data
	return `data:image/png;base64,${Buffer.from(imgBuffer, 'binary').toString(
		'base64'
	)}`
}
