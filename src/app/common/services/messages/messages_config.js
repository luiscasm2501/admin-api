const rabbit = require('amqplib/callback_api')

require('dotenv').config()

const LOGGER = require('../../controllers/logger/logger')

const queues = ['notifications']
/*
  Create rabbitMQ to handle events
*/
LOGGER.info('Connecting to RabbitMQ')
rabbit.connect('amqp://localhost', (error0, connection) => {
	if (error0) {
		throw error0
	}
	LOGGER.info('Creating default channel on default exchange')
	connection.createChannel((error, channel) => {
		if (error) {
			throw error
		}
		rabbit.channel = channel

		queues.forEach((queue) => {
			channel.assertQueue(queue, {
				durable: false,
			})
			LOGGER.info(`Created ${queue} on channel`)
		})

		channel.send = (queue, message) => {
			channel.sendToQueue(queue, Buffer.from(JSON.stringify(message)))
		}
	})
})
module.exports = rabbit
