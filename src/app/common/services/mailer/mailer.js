require('dotenv').config()
const nodemailer = require('nodemailer')
const Email = require('email-templates')
const email = new Email()
const path = require('path')
const { IMG_LINKS } = require('./constant')

const transporter = nodemailer.createTransport({
	service: process.env.EMAIL_SERVICE,
	auth: {
		user: process.env.BASE_EMAIL,
		pass: process.env.BASE_EMAIL_PASSWORD,
	},
})

function loadTemplate(templateName, context) {
	return new Promise((resolve, reject) => {
		email
			.render(path.join(__dirname, '/templates', templateName), context)
			.then((result) => {
				resolve(result)
			})
			.catch((error) => {
				reject(error.message)
			})
	})
}
async function sendEmail(to, subject, templatePath, data, attachments = []) {
	let template = await loadTemplate(templatePath, {
		...data,
		app_url: process.env.WEB_APP_URL,
		...(!data.url && !!data.button && { url: process.env.WEB_APP_URL }),
		...(data.img && { img: IMG_LINKS[data.img] || data.img }),
	})

	const mailOptions = {
		from: process.env.BASE_EMAIL,
		to: to,
		subject: subject,
		html: template,
		attachments,
	}
	console.log('FALLBACK URL: ', data.url || 'F')
	return new Promise((resolve, reject) => {
		transporter.sendMail(mailOptions, function (error, info) {
			if (error) {
				reject(error)
			} else {
				resolve('Email sent: ' + info.response)
			}
		})
	})
}

module.exports = { sendEmail }
