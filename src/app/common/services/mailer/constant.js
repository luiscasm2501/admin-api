const IMGS = {
	SUCCESS: 'success',
	ERROR: 'error',
}

const IMG_LINKS = {
	[IMGS.SUCCESS]:
    'https://firebasestorage.googleapis.com/v0/b/bitgobi-assets.appspot.com/o/check-icon.png?alt=media&token=6edcdd6b-528e-4566-a72c-e357372fa32d',
	[IMGS.ERROR]:
    'https://firebasestorage.googleapis.com/v0/b/bitgobi-assets.appspot.com/o/x-icon.png?alt=media&token=6edcdd6b-528e-4566-a72c-e357372fa32d',
}

const MAIL_TEMPLATES = {
	TICKET_SOLVED: {
		subject: (ticketId) => `Support ticket [${ticketId}] solved / Ticket de soporte [${ticketId}] resuelto`,
		template: 'ticket_solved.pug',
	},
}

module.exports = { IMGS, IMG_LINKS, MAIL_TEMPLATES }
