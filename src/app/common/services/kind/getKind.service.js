module.exports = class getKindService {
	constructor(kindRepository) {
		this.kindRepository = kindRepository
	}

	async getKinds(options) {				
		return await this.kindRepository.getAll({						
			...options
		})
	}

	async getKind(id, options) {
		return await this.kindRepository.getById(id, {						
			...options,
		})
	}
}