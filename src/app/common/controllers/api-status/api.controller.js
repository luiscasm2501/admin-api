const { createController } = require('awilix-router-core')

const consultControllers = () => ({
	getStatus: async (req, res, next) => {
		try {
			res.send({ message: 'Admin API active', status: 200, date: new Date() })
		} catch (error) {
			next(error)
		}
	},
})

module.exports = createController(consultControllers)
	.prefix('/api-status')
	.get('', 'getStatus')

/**
 * @swagger
 * /api-status:
 *  get:
 *    tags: [General]
 *    security: []
 *    summary: simple endpoint to check if the API is up
 *    responses:
 *      200:
 *        description: API is up and running
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              properties:
 *                message:
 *                  type: string
 *                status:
 *                  type: number
 *                date:
 *                  type: string
 *                  format: date-time
 *              example:
 *                message: Admin API active
 *                status: 200
 *                date: 2021-11-18T16:46:58.381Z
 */
