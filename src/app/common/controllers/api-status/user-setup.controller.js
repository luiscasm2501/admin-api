const { createController } = require('awilix-router-core')

const consultControllers = (createBaseUserService) => ({
	createUser: async (req, res, next) => {
		try {
			await createBaseUserService.createBaseUser()
			res.send({ message: 'User created', status: 200, date: new Date() })
		} catch (error) {
			next(error)
		}
	},
})

module.exports = createController(consultControllers)
	.prefix('/setup-user')
	.all('', 'createUser')

/**
 * @swagger
 * /setup-user:
 *  post:
 *    security: []
 *    tags: [General]
 *    summary: create the first user in the API
 *    description: When the API is getting started, there's no users yet, so access to most endpoints is restricted. Create a user.json file inside the project src folder and call this endpoint.
 *    responses:
 *      200:
 *        description: user created successfully
 *      400:
 *        description: either there's already an user created, or the user.json file is not set up
 *
 */
