const { createController } = require('awilix-router-core')
const NotFoundError = require('../../controllers/error-handling/notFoundError')

const consultControllers = (getKindService) => ({
	getAllKinds: async (req, res, next) => {
		try {
			const result = await getKindService.getKinds(req.query)			
			next(result)
		} catch (error) {
			next(error)
		}
	},
	getKind: async (req, res, next) => {
		try {
			const id = req.params.id
			const result = await getKindService.getKind(id)
			if (result) res.send(result)
			else next(new NotFoundError(`Kind ${id}`))
		} catch (error) {
			next(error)
		}
	},
})

module.exports = createController(consultControllers)
	.prefix('/kinds')
	.get('', 'getAllKinds')
	.get('/:id', 'getKind')

/**
 * @swagger
 * /kinds:
 *  get:
 *    tags: [Kinds]
 *    summary: get a list of kind records
 *    parameters:
 *      - $ref: '#/components/parameters/page'
 *      - $ref: '#/components/parameters/sort'
 *    responses:
 *      200:
 *        description: list of kinds
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/Kind'
 *              type: array
 * /kinds/{kindId}:
 *  get:
 *    tags: [Kinds]
 *    summary: get a kind record by id
 *    parameters:
 *      - $ref: '#/components/parameters/kindId'
 *    responses:
 *      200:
 *        description: kind record
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/Kind'
 */
