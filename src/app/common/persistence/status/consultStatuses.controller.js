const { createController } = require('awilix-router-core')

const consultControllers = (getStatusesService) => ({
	getStatuses: async (req, res, next) => {
		try {
			const result = await getStatusesService.getStatuses()
			res.send(result)
		} catch (error) {
			next(error)
		}
	},
})

module.exports = createController(consultControllers)
	.prefix('/statuses')
	.get('', 'getStatuses')

/**
 * @swagger
 * /statuses:
 *  get:
 *    security: []
 *    tags: [General]
 *    summary: list of all statuses used in various entities
 *    responses:
 *      200:
 *        description: list of status, of key-value objects, where 'key' is the status name, and 'value' is its id.
 *        content:
 *          application/json:
 *            schema:
 *              type: array
 *              properties:
 *                statusName:
 *                  type: string
 *                  format: uuid
 *            example:
 *              - ACTIVE: <uuid>
 *              - APPROVED: <uuid>
 *              - SUSPENDED: <uuid>
 */
