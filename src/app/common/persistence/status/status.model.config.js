const logger = require('../../controllers/logger/logger')
const statuses = require('./statuses')

module.exports = function setStatuses(db) {
	const { Status } = db.models
	Status.count()
		.then((statusCount) => {
			const statusNames = Object.keys(statuses)
			if (statusCount < statusNames.length) {
				statusNames.forEach((key) => {
					Status.create({ name: key }).catch(() =>
						logger.warn(`${key} status already exists`)
					)
				})
			}
		})
		.catch((err) => {
			logger.error(`Couldn't update status table [${err.message}]`)
		})
}
