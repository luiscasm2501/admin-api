const { DataTypes } = require('sequelize')

function makeModel(apiDb) {
	const Status = apiDb.define(
		'Status',
		{
			name: {
				type: DataTypes.STRING(20),
				allowNull: false,
			},
			statusId: {
				type: DataTypes.UUID,
				allowNull: false,
				primaryKey: true,
				defaultValue: DataTypes.UUIDV4,
			},
		},
		{
			timestamps: false,
			indexes: [{ unique: true, fields: ['name'] }],
		}
	)

	return Status
}

module.exports = makeModel
