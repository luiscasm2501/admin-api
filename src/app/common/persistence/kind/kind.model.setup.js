const logger = require('../../controllers/logger/logger')
const kinds = require('./kinds')

module.exports = function setkinds(db) {
	const { Kind } = db.models
	Kind.sync()
		.then(async () => {
			const KindCount = await Kind.count()
			const KindNames = Object.keys(kinds)
			if (KindCount < KindNames.length) {
				KindNames.forEach((key) => {
					Kind.create({ name: key }).catch(() =>
						logger.warn(`${key} Kind already exists`)
					)
				})
			}
		})
		.catch((err) => {
			logger.error(`Couldn't update Kind table [${err.message}]`)
		})
}
