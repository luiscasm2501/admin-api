const { DataTypes } = require('sequelize')

function makeModel(apiDb) {
	const Kind = apiDb.define(
		'Kind',
		{
			name: {
				type: DataTypes.STRING(20),
				allowNull: false,
			},
			kindId: {
				type: DataTypes.UUID,
				allowNull: false,
				primaryKey: true,
				defaultValue: DataTypes.UUIDV4,
			},
		},
		{
			timestamps: false,
			indexes: [{ unique: true, fields: ['name'] }],
		}
	)

	return Kind
}

module.exports = makeModel