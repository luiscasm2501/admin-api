const SequelizeRepo = require('../sequilize/sequelizeRepo')

module.exports = class kindRepository extends SequelizeRepo {
	constructor(apiDb) {
		super(apiDb.models.Kind, apiDb)
	}
}
