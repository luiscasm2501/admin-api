function findAssociations(model, associationNames = []) {
	if (!Array.isArray(associationNames)) {
		return associationNames
	}
	const associations = mapAssociation(model, associationNames)
	return associations
}
/*
  Recursive function to find models to be included in GET request. "includes" examples:
    -Basic: ['Status','Contract',..etc]
    -Nested: [['Client',['Status','Contract']] ]
  
*/
function mapAssociation(model, includes = [], collection = []) {
	if (model) {
		includes.forEach((name) => {
			if (Array.isArray(name)) {
				const nestedModel = model.associations[name[0]]
				collection.push({
					model: nestedModel.target,
					include: mapAssociation(nestedModel.target, name[1]),
				})
			} else {
				const assoc = model.associations[name.model || name]
				if (assoc) {
					const includeObject = {
						model: assoc.target,
						required: name.required,
						...(name.conditions && { where: name.conditions }),
						...(name.duplicating!=undefined && { duplicating: name.duplicating }),
						...(name.attr && { attributes: name.attr }),
						...(name.include && {
							include: mapAssociation(assoc.target, name.include),
						}),
					}
					collection.push(includeObject)
				}
			}
		})
	}
	return collection
}

module.exports = findAssociations
