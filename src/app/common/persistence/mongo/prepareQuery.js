module.exports = function prepareQuery(options) {
	return {
		...paginateRequest(options),
		sort: setSort(options),
	}
}
const paginateRequest = (options = {}) => {
	//Set pagination
	const { size, page } = options
	const limit = size ? parseInt(size) : undefined
	const skip = page && limit ? page * parseInt(limit) : 0

	return {
		limit,
		skip,
	}
}
function setSort(options = {}) {
	const { sort } = options
	const sortSet = {}
	if (typeof sort === 'string') {
		const sortParams = sort.split(',')
		sortParams.forEach((param) => {
			let [attr, dir] = param.split(':')
			switch (dir) {
			case 'a':
				dir = 'asc'
				break
			case 'd':
				dir = 'desc'
				break
			default:
				dir = 'asc'
			}
			sortSet[attr] = dir
		})
	}
	return sortSet
}
