const repo = require('../repo')
const prepareConditions = require('./prepareConditions')
const prepareQuery = require('./prepareQuery')

module.exports = class mongoRepo extends repo {
	constructor(nosqlDb, collection) {
		super()
		if (typeof nosqlDb === 'undefined') {
			throw new Error('MongoDB connection is a required parameter.')
		}

		if (typeof collection === 'undefined') {
			throw new Error('Collection is a required parameter.')
		}
		this.connection = nosqlDb
		this.collection = this.connection.models[collection]
		if (!this.collection) {
			throw new Error(`collection [${collection}] couldn't be found`)
		}
	}

	async getAll(
		options = { attr: '', conditions: {}, settings: {}, count: false }
	) {
		const rows = await this.collection
			.find(options.conditions, options.attr, prepareQuery(options.settings))
			.lean()
		if (options.count) {
			const count = options.conditions
				? await this.collection.countDocuments(options.conditions)
				: await this.collection.estimatedDocumentCount()
			return {
				rows,
				count,
			}
		}
		return rows
	}

	async getById(id, options = { attr: '' }) {
		return await this.collection.findById(id, options.attr)
	}

	async getOne(options = { attr: '', conditions: {} }) {
		const result = await this.collection
			.findOne(options.conditions, options.attr)
			.exec()
		if (result) return result._doc
		return result
	}

	async create(documents) {
		if (documents._id) {
			const upsert = await this.collection.replaceOne(
				{ _id: documents._id },
				documents
			)
			if (upsert.modifiedCount > 0) {
				return documents
			}
		}
		const result = await this.collection.create(documents)
		return result
	}

	async remove(id) {
		return await this.collection.deleteOne({ _id: id })
	}

	async removeMany(conditions = {}) {
		conditions = prepareConditions(conditions)
		const deleted = await this.collection.deleteMany(conditions)
		return deleted.deletedCount
	}
	async update(item) {
		return await this.collection
			.findOneAndUpdate({ _id: item.id || item._id }, item, { new: true })
			.exec()
	}
}
