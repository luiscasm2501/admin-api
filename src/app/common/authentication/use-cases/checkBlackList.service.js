const UnauthorizedError = require('../../controllers/error-handling/unauthorizedError')

module.exports = class checkBlackListService {
	constructor(tokenRepository) {
		this.tokenRepository = tokenRepository
	}
	async checkBlackList(token) {
		const blackListed = await this.tokenRepository.getOne({
			conditions: { token: token },
			attr: ['_id'],
		})
		if (blackListed) {
			throw new UnauthorizedError('blackListed token')
		}
		return true
	}
}
