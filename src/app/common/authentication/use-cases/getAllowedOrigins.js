const getAllowedOrigins = () => {
	let ALLOWED_ORIGINS = process.env.ALLOWED_ORIGINS
	const ORIGINS = []
	function appendOrigins(keys = []) {
		keys.forEach((key) => {
			const origin = process.env[key]
			if (origin) {
				ORIGINS.push(origin)
			}
		})
	}
	ALLOWED_ORIGINS = ALLOWED_ORIGINS
		? ALLOWED_ORIGINS.split(',').map((origin) => origin.trim())
		: []
	appendOrigins(['CLIENT_AM_WEB_APP_URL', 'WEB_APP_URL'])

	return ORIGINS.concat(ALLOWED_ORIGINS)
}

module.exports = getAllowedOrigins
