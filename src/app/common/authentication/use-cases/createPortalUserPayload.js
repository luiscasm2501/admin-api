function createPortalUserPayload(portalUserId,userContext) {	
	return {
		portalUser: portalUserId,
		context:userContext		
	}
}
module.exports = createPortalUserPayload