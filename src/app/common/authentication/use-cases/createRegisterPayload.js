function createRegisterPayload(toRegisterId,userContext) {	
	return {
		toRegisterId: toRegisterId,
		context: userContext		
	}
}
module.exports = createRegisterPayload