/* exclude paths from middleware */
const unless = (middleware, ...paths) => {
	return function (req, res, next) {
		let pathCheck = paths.find((path) =>
			path.regex ? path.regex.test(req.path) : path.route === req.path
		)
		if (pathCheck && pathCheck.origins) {
			if (!(req.get('origin') in pathCheck.origins)) {
				pathCheck = undefined
			}
		}
		const methodCheck =
      pathCheck && pathCheck.methods
      	? pathCheck.methods.some(
      		(method) => method.toUpperCase() === req.method
      	)
      	: true
    ;(pathCheck && methodCheck) || req.path.includes('/public')
			? next()
			: middleware(req, res, next)
	}
}

module.exports = unless
