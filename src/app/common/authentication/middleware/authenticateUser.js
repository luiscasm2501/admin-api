const verifyToken = require('../use-cases/verifyToken.service')
const logger = require('../../controllers/logger/logger')

function validateToken(
	verifyUserService,
	checkBlackListService,
	validateInvoiceTokenService,
	verifyContractService,
	verifyPortalUserService,
	verifyPortalRegisterService
) {
	return async (req, res, next) => {
		try {
			if (!req.headers.authorization) {
				logger.error('request without authorization')
				return res.status(401).end()
			}
			const token = req.headers.authorization.split(' ')[1]
			const payload = new verifyToken().verifyToken(token)

			await checkBlackListService.checkBlackList(token)
			if (payload.invoice) {
				validateInvoiceTokenService.validateInvoiceToken(payload.invoice)
				req._invoice = payload.invoice
			} else if (payload.contract) {
				await verifyContractService.verifyContract(payload.contract)
				req._contract = payload.contract
			} else if (payload.portalUser) {
				const result = await verifyPortalUserService.verifyUser(
					payload.portalUser
				)
				req._portalUser = payload.portalUser
				req._context = payload.context
				req._client = result.client
			} else if (payload.toRegisterId) {
				await verifyPortalRegisterService.verifyRegister(
					payload.toRegisterId,
					payload.context
				)
				req._toRegisterId = payload.toRegisterId
				req._context = payload.context
			} else {
				//verify user and set attributes in request
				await verifyUserService.verifyUser(payload)
				req._user = payload.user
				req._role = payload.role
			}

			next()
		} catch (error) {
			next(error)
		}
	}
}
module.exports = validateToken
