module.exports = class readNotificationService {
	constructor(notificationRepository) {
		this.repo = notificationRepository
	}
	async readNotification({ id, read }) {
		return await this.repo.update({ _id: id, read })
	}
}
