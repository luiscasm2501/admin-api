const { DateTime } = require('luxon')

module.exports = class getNotificationsService {
	constructor(notificationRepository) {
		this.notificationRepository = notificationRepository
	}
	async getNotifications(options = {}) {
		const result = await this.notificationRepository.getAll({
			settings: options,
			count: true,
			conditions: {
				...(options.unread?.length && { read: options.unread === 'false' }),
			},
		})
		result.rows = result.rows.map((notif) => {
			return {
				...notif,
				created_time: DateTime.fromJSDate(
					new Date(notif.created_time)
				).toLocaleString(DateTime.DATETIME_SHORT),
			}
		})
		return result
	}
	async getPendingCount() {
		const result = await this.getNotifications({ unread: 'true' })
		return {
			count: result.count,
		}
	}
}
