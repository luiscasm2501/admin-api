const { DateTime } = require('luxon')
const makeNotification = require('../domain/makeNotification')
/*
  Send notification in real time to client 
*/
module.exports = class sendNotificationService {
	constructor(queue, notificationRepository) {
		this.queue = queue
		this.notificationRepository = notificationRepository
	}
	async sendNotification({ title, content, kind = null }) {
		const notification = makeNotification(title, content, kind)
		await this.notificationRepository.create(notification)
		this.queue.channel.send('notifications', {
			...notification,
			created_time: DateTime.fromISO(notification.created_time).toLocaleString(
				DateTime.DATETIME_SHORT
			),
		})
	}
}
