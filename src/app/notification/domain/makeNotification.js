const { DateTime } = require('luxon')

module.exports = function makeNotification(title, content, kind = 'INFO') {
	return Object.freeze({
		// expected: kinds: 'success','warning','error','info', 'reminder'
		kind: kind?.toUpperCase() || 'INFO',
		title,
		content,
		created_time: DateTime.now().toISO(),
	})
}
