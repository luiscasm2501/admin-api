const { createController } = require('awilix-router-core')

const consultControllers = (
	getNotificationsService,
	readNotificationService,
	sendNotificationService
) => ({
	getNotifications: async (req, res, next) => {
		try {
			const result = await getNotificationsService.getNotifications(req.query)
			next(result)
		} catch (error) {
			next(error)
		}
	},
	readNotification: async (req, res, next) => {
		try {
			const result = await readNotificationService.readNotification({
				id: req.params.id,
				...req.body,
			})
			res.send(result)
		} catch (error) {
			next(error)
		}
	},
	getPendingCount: async (req, res, next) => {
		try {
			const result = await getNotificationsService.getPendingCount()
			res.send(result)
		} catch (error) {
			next(error)
		}
	},
	sendNotification: async (req, res, next) => {
		try {
			await sendNotificationService.sendNotification(req.body)
			res.send('ok')
		} catch (error) {
			next(error)
		}
	},
})
const PREFIX = '/notifications'
module.exports = createController(consultControllers)
	.get(PREFIX, 'getNotifications')
	.get('/pending-notifications', 'getPendingCount')
	.put(`${PREFIX}/:id`, 'readNotification')
	.post(PREFIX, 'sendNotification')
/**
 * @swagger
 * /notifications:
 *  get:
 *    tags: [Notifications]
 *    summary: get a list of system notifications
 *    parameters:
 *      - $ref: '#/components/parameters/page'
 *      - $ref: '#/components/parameters/size'
 *      - $ref: '#/components/parameters/sort'
 *    responses:
 *      200:
 *        description: list of notifications records
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/Notification'
 *              type: array
 */
