function makeSchema(Schema) {
	const notificationSchema = new Schema({
		title: {
			type: String,
			required: true,
		},
		kind: { type: String, required: true },
		created_time: {
			type: Date,
			required: true,
		},
		content: {
			type: Object,
		},
		read: {
			type: Boolean,
			default: false,
		},
	})
	return {
		name: 'notifications',
		schema: notificationSchema,
	}
}
module.exports = makeSchema
