const mongoRepo = require('../../common/persistence/mongo/mongoRepo')

module.exports = class tokenRepository extends mongoRepo {
	constructor(nosqlDb) {
		super(nosqlDb, 'notifications')
	}
}
