const betweenValidator = require('../../common/domain/betweenValidator.js')
const validateId = require('../../common/domain/idValidator.js')

module.exports = function clientEntity(
	{ title, description, bulletinId, date, statusFk,kindFk, attachment },
	update = false
) {
	(update && !title) || betweenValidator.stringBetween(title, 2, 100, 'title')
	;(update && !description) || betweenValidator.stringBetween(title, 2, 1000, 'description')	
	update && validateId(bulletinId)
	return Object.freeze({
		title, 
		description, 
		bulletinId, 
		date,
		statusFk,
		kindFk,
		attachment
	})
}