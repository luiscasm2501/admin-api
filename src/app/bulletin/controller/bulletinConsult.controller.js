const { createController } = require('awilix-router-core')
const NotFoundError = require('../../common/controllers/error-handling/notFoundError')

const consultControllers = (getBulletinsService, getBulletinService) => ({
	getAllBulletins: async (req, res, next) => {
		try {
			const result = await getBulletinsService.getBulletins(req.query)
			next(result)
		} catch (error) {
			next(error)
		}
	},
	getBulletin: async (req, res, next) => {
		try {
			const id = req.params.id
			const result = await getBulletinService.getBulletin(id)
			if (result) res.send(result)
			else next(new NotFoundError(`Bulletin ${id}`))
		} catch (error) {
			next(error)
		}
	},
})

module.exports = createController(consultControllers)
	.prefix('/bulletins')
	.get('', 'getAllBulletins')
	.get('/:id', 'getBulletin')

/**
 * @swagger
 * /bulletins:
 *  get:
 *    tags: [Bulletins]
 *    summary: get a list of bulletin records
 *    parameters:
 *      - $ref: '#/components/parameters/page'
 *      - $ref: '#/components/parameters/sort'
 *    responses:
 *      200:
 *        description: list of bulletins
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/Bulletin'
 *              type: array
 * /bulletins/{bulletinId}:
 *  get:
 *    tags: [Bulletins]
 *    summary: get a bulletin record by id
 *    parameters:
 *      - $ref: '#/components/parameters/bulletinId'
 *    responses:
 *      200:
 *        description: bulletin record
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/Bulletin'
 */
