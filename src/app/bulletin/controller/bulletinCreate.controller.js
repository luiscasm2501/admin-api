const { createController } = require('awilix-router-core')
const upload = require('../../files/config/files.config')

const registerControllers = (createBulletinService) => ({
	createBulletin: async (req, res, next) => {
		try {
			const result = await createBulletinService.createBulletin(req.body)
			if (req.file) {
				if(result.bulletinId) result['entity_id'] = result.bulletinId
				result['entity_type'] = 'Bulletin'
				next(result)
			}else{
				res.send(result)
			}						
		} catch (error) {
			next(error)
		}
	},
})

module.exports = createController(registerControllers)
	.prefix('/bulletins')
	.post('', 'createBulletin')
	.before(upload.single('bulletins'))

/**
 * @swagger
 * /bulletins:
 *  post:
 *    tags: [Bulletins]
 *    summary: create a new bulletin record
 *    requestBody:
 *      required: true
 *      content:
 *        application/json:
 *          schema:
 *            $ref: '#/components/schemas/Bulletin'
 *    responses:
 *      200:
 *        description: created client record
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/Bulletin'
 */
