const { createController } = require('awilix-router-core')
const NotFoundError = require('../../common/controllers/error-handling/notFoundError')
const UpdateError = require('../../common/controllers/error-handling/updateError')
const upload = require('../../files/config/files.config')

const alterControllers = (updateBulletinService, removeBulletinService) => ({
	updateBulletin: async (req, res, next) => {
		try {
			const paramId = req.params.id
			const bodyId = req.body.bulletinId			
			if (paramId === bodyId) {
				const result = await updateBulletinService.updateBulletin(req.body, req.file)
				if(result){
					res.send(result)
				}
				else{
					next(new NotFoundError(`Bulletin ${paramId} not found`))
				}
			} else {
				next(new UpdateError(paramId, bodyId))
			}
		} catch (error) {
			next(error)
		}
	},
	removeBulletin: async (req, res, next) => {
		try {
			const id = req.params.id
			const result = await removeBulletinService.removeBulletin(id)
			if (result) {
				res.send(`Bulletin ${id} succesfully marked as deleted`)
			} else {
				next(new NotFoundError(`Bulletin ${id} not found`))
			}
		} catch (error) {
			next(error)
		}
	},
	restoreBulletin: async (req, res, next) => {
		try {
			const id = req.params.id
			const result = await removeBulletinService.restoreBulletin(id)
			if (result) {
				res.send(`Bulletin ${id} succesfully restored`)
			} else {
				next(new NotFoundError(`Bulletin ${id} not found`))
			}
		} catch (error) {
			next(error)
		}
	},
})

module.exports = createController(alterControllers)
	.prefix('/bulletins/:id')
	.put('', 'updateBulletin')
	.before(upload.single('bulletins'))
	.delete('', 'removeBulletin')
	.patch('', 'restoreBulletin')
	

/**
 * @swagger
 * /bulletins/{bulletinId}:
 *  put:
 *    tags: [Bulletins]
 *    summary: update a bulletin's information by id
 *    parameters:
 *      - $ref: '#/components/parameters/bulletinId'
 *    requestBody:
 *      required: true
 *      content:
 *        application/json:
 *          schema:
 *            $ref: '#/components/schemas/Bulletin'
 *    responses:
 *      200:
 *        description: updated bulletin information
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/Bulletin'
 *  delete:
 *    tags: [Bulletins]
 *    summary: delete a bulletin record by id
 *    description: on the first go, it marks it a deleted, if used again, deletes it permanently
 *    parameters:
 *      - $ref: '#/components/parameters/bulletinId'
 *    responses:
 *      200:
 *        description: bulletin successfully deleted / marked as "deleted"
 *  patch:
 *    tags: [Bulletins]
 *    summary: restores a bulletin record marked as "deleted"
 *    parameters:
 *      - $ref: '#/components/parameters/bulletinId'
 *    responses:
 *      200:
 *        description: bulletin successfully restored
 */