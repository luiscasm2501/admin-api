const { DataTypes } = require('sequelize')

module.exports = function makeModel(apiDb) {
	const Bulletin = apiDb.define(
		'Bulletin',
		{
			title: {
				type: DataTypes.STRING(100),
				allowNull: false,
			},
			description: {
				type: DataTypes.STRING(1000),
				allowNull: false,
			},
			date: {
				type: DataTypes.DATE,
				allowNull: false,				
			},      
			bulletinId: {
				type: DataTypes.UUID,
				allowNull: false,
				primaryKey: true,
				defaultValue: DataTypes.UUIDV4,
			},
		},
		{ paranoid: true, createdAt: false }
	)
	return Bulletin
}