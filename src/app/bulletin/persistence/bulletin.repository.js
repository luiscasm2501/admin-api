const SequelizeRepo = require('../../common/persistence/sequilize/sequelizeRepo')
const statuses = require('../../common/persistence/status/statuses')

class bulletinRepository extends SequelizeRepo {
	constructor(apiDb) {
		super(apiDb.models.Bulletin, apiDb, statuses.ACTIVE)
	}
}

module.exports = bulletinRepository