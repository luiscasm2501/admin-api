module.exports = function setupModel(db) {
	const { Status, Bulletin, Kind } = db.models  

	Status.hasMany(Bulletin, {
		foreignKey: 'statusFk',
	})

	Bulletin.belongsTo(Status, {
		foreignKey: 'statusFk',
	})

	Kind.hasMany(Bulletin,{
		foreignKey: 'kindFk',
	})

	Bulletin.belongsTo(Kind,{
		foreignKey: 'kindFk',
	})
}
