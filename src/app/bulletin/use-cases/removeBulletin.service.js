module.exports = class RemoveBulletinService {
	constructor(bulletinRepository, fileSystemRepository, deleteInFileSystemService) {
		this.bulletinRepository = bulletinRepository
		this.fileSystemRepository = fileSystemRepository
		this.deleteInFileSystemService = deleteInFileSystemService
	}

	async removeBulletin(id) {		
		if(id){	
			const found = await this.bulletinRepository.getOne({conditions:{bulletinId: id},paranoid:false})
			if (found) {
				const force = found.deletedAt ? true : false
				if(force){					
					const resultBulletin = await this.bulletinRepository.remove(id)					
					const relatedFile = await this.fileSystemRepository.getOne({conditions:{entity_id: id}})					
					if(relatedFile) {
						const resultFile = await this.deleteInFileSystemService.deleteInFileSystem(relatedFile._id,force)
						return 	 {...resultBulletin.dataValues,...resultFile}
					}
					return resultBulletin
						
				}else{
					return await this.bulletinRepository.remove(id)	
				}
			}				
		}		
	}
	async restoreBulletin(id) {
		if (id) return await this.bulletinRepository.restore(id)
	}
}
