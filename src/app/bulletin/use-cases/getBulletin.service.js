module.exports = class getBulletinService {
	constructor(bulletinRepository, fileSystemRepository) {
		this.bulletinRepository = bulletinRepository
		this.fileSystemRepository = fileSystemRepository
	}

	async getBulletin(id) {
		const bulletin = await this.bulletinRepository.getById(id, {
			include: ['Kind', 'Status'],
		})
		if(bulletin) {
			const relatedfile = await this.fileSystemRepository.getOne({conditions:{
				entity_id: bulletin.bulletinId}
			})
			if(relatedfile) return {...bulletin.dataValues, attachment: relatedfile}
			return bulletin
		}		
	}
}
