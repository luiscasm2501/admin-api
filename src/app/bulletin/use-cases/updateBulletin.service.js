const bulletinEntity = require('../domain/bulletinEntity')

class updateBulletinService {
	constructor(bulletinRepository, updateInFileSystemService, fileSystemRepository, createInFileSystemService) {
		this.bulletinRepository = bulletinRepository
		this.updateInFileSystemService = updateInFileSystemService
		this.fileSystemRepository = fileSystemRepository
		this.createInFileSystemService = createInFileSystemService
	}
	async updateBulletin(bulletin,file) {		
		bulletinEntity(bulletin, true)
		const updatedBulletin = await this.bulletinRepository.update(bulletin)			
		if(updatedBulletin){
			if(file){
				if(bulletin.bulletinId) bulletin['entity_id'] = bulletin.bulletinId
				bulletin['entity_type'] = 'Bulletin'
				const relatedFile = await this.fileSystemRepository.getOne({conditions:{entity_id: bulletin.bulletinId}})			
				if(relatedFile){				
					const updatedFile = await this.updateInFileSystemService.updateFile(relatedFile._id, file, bulletin)			
					return {...updatedBulletin.dataValues, attachment: updatedFile._doc.path}
				}
				else{				
					const createdFile = await this.createInFileSystemService.createFile(file, undefined, bulletin)
					return {...updatedBulletin.dataValues, attachment: createdFile._doc.path}
				}
			}					 						
		}
		return updatedBulletin		
	}
}
module.exports = updateBulletinService
