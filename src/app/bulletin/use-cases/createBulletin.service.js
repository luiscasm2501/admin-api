const bulletinEntity = require('../domain/bulletinEntity')

class createBulletinService {
	constructor(bulletinRepository,bus) {
		this.bulletinRepository = bulletinRepository
		this.bus = bus
	}
	async createBulletin(bulletin) {
		bulletinEntity(bulletin)		
		let registeredBulletin = await this.bulletinRepository.create(bulletin)
		if (registeredBulletin) {			
			this.bus.emit('bulletinRegistered', registeredBulletin)
		}
		return registeredBulletin 
	}
}

module.exports = createBulletinService