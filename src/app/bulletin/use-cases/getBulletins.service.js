module.exports = class getBulletinsService {
	constructor(bulletinRepository, fileSystemRepository) {
		this.bulletinRepository = bulletinRepository
		this.fileSystemRepository = fileSystemRepository
	}


	async getBulletins(options = {}) {
		const bulletins = await this.bulletinRepository.getAll({
			include: ['Kind', 'Status'],
			...options,
		})
		const files = await this.fileSystemRepository.getAll({
			conditions: {entity_type:'Bulletin'}			
		})							
		for(let i in bulletins.rows) {
			for(let j in files){
				if(bulletins.rows[i].bulletinId == files[j].entity_id) {
					bulletins.rows[i] = {...bulletins.rows[i].dataValues, attachment: files[j]}
					break
				} 				
			}			
		}
		return bulletins
	}
}
