const generatePassword = require('../../common/authentication/use-cases/passwordGenerator')
const {
	encryptPassword,
} = require('../../common/controllers/encryption/encryptor')
const statuses = require('../../common/persistence/status/statuses')
const userEntity = require('../domain/userEntity')

module.exports = class registerUserService {
	constructor(userRepository, bus) {
		this.userRepository = userRepository
		this.bus = bus
	}

	async registerUser(user, emit = true) {
		userEntity(user)
		let password = ''
		if (!user.password) {
			password = generatePassword()
			user.password = password
		}
		user.password = await encryptPassword(user.password)
		const result = await this.userRepository.create(user)
		if ((result && password) || emit) {
			await this.userRepository.updateStatus(
				result.userId,
				statuses.PENDING_REGISTER
			)
			this.bus.emit('userRegistered', {
				...user,
				userId: result.userId,
				password,
			})
		}
		return result
	}
}
