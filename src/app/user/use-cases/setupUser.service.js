const createToken = require('../../common/authentication/use-cases/createToken')
const createUserPayload = require('../../common/authentication/use-cases/createUserPayload')
const logger = require('../../common/controllers/logger/logger')
const { sendEmail } = require('../../common/services/mailer/mailer')
require('dotenv').config()
module.exports = class setupUserService {
	constructor(loginUserService) {
		this.loginUserService = loginUserService
	}
	async setupUser(
		user,
		title = 'Cacao\'s Admin sign up / Registrarse como administrador de Cacao',
		template = 'register'
	) {
		const token = createToken(createUserPayload(user), '72h')
		try {
			const url = `${process.env.WEB_APP_URL}/register/${token}`
			return await sendEmail(user.email, title, template, {
				user,
				url,
				button: 'Sign Up / Registrarse',
				title: 'Complete your Cacao account / Completa tu cuenta Cacao',
			})
		} catch (error) {
			logger.error(`At setting up user sign up [${error.message}]`)
			console.log(error)
		}
	}
}
