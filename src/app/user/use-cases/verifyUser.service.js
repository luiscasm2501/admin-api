const UnauthorizedError = require('../../common/controllers/error-handling/unauthorizedError')
const statuses = require('../../common/persistence/status/statuses')

class verifyUserService {
	constructor(userRepository, roleRepository) {
		this.userRepository = userRepository
		this.roleRepository = roleRepository
	}
	async verifyUser(data = { user, role }) {
		const userRecord = await this.userRepository.getById(data.user, {
			include: ['Status'],
		})
		if (userRecord) {
			if (userRecord.Status.name == statuses.SUSPENDED) {
				throw new UnauthorizedError('this user is blocked')
			}
			return true
		}
		throw new UnauthorizedError('invalid user in token')
	}
}
module.exports = verifyUserService
