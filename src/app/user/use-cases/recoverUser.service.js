const createToken = require('../../common/authentication/use-cases/createToken')
const createUserPayload = require('../../common/authentication/use-cases/createUserPayload')
const logger = require('../../common/controllers/logger/logger')
const ValidationException = require('../../common/domain/validationException')
const { sendEmail } = require('../../common/services/mailer/mailer')
require('dotenv').config()
module.exports = class recoverUserService {
	constructor(userRepository) {
		this.userRepository = userRepository
	}
	async recoverUser(user) {
		const foundUser = await this.userRepository.getAll({ email: user.email })
		if (foundUser.count) {
			user = foundUser.rows[0]
			const token = createToken(createUserPayload(user), '24h')
			const url = `${process.env.WEB_APP_URL}/login/forgot-password/${token}`
			return await sendEmail(
				user.email,
				'Recover your Cacao\'s Admin password / Recupera tu contraseña de administrador de Cacao',
				'forgot_password',
				{
					user,
					url,
					button: 'Change Password / Cambiar la contraseña',
					title: 'Recover Password / Recuperar contraseña',
				}
			)
		} else {
			throw new ValidationException('Couldn\'t find user')
		}
	}
}
