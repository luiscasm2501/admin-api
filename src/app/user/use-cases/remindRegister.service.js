const { DateTime, Interval } = require('luxon')
const NotFoundError = require('../../common/controllers/error-handling/notFoundError')
const ValidationException = require('../../common/domain/validationException')
const statuses = require('../../common/persistence/status/statuses')
module.exports = class remindRegisterService {
	constructor(setupUserService, userRepository) {
		this.setupUserService = setupUserService
		this.userRepository = userRepository
	}
	async remindRegister(userId) {
		const user = await this.userRepository.getById(userId, {
			include: ['Status'],
		})
		if (user) {
			if (user.Status.name != statuses.PENDING_REGISTER) {
				throw new ValidationException(
					`This user is fully registered.${user.Status.name} status`
				)
			}

			const userDate = DateTime.fromJSDate(new Date(user.updatedAt))
			const timeInterval = Interval.fromDateTimes(userDate, DateTime.now())

			if (!timeInterval.isValid || timeInterval.length('days') < 3) {
				throw new ValidationException(
					'Please wait 3 days until issuing a new email'
				)
			}
			await this.setupUserService.setupUser(
				user.dataValues,
				'[Reminder] Setup your Cacao Admin account'
			)
			return this.userRepository.updateStatus(
				user.userId,
				statuses.PENDING_REGISTER
			)
		}
		throw new NotFoundError('User')
	}
}
