require('dotenv').config()
const UnauthorizedError = require('../../common/controllers/error-handling/unauthorizedError')
const { verify } = require('hcaptcha')

class hCaptchaService {
	async hCaptchaValidate(token) {
		const secret = process.env.HCAPTCHA_SECRET
		await verify(secret, token)
			.then((data) => {
				if (!(data.success === true)) {
					throw new UnauthorizedError()
				}
			})
			.catch((error) => {
				throw new UnauthorizedError(error)
			})
	}
}
module.exports = hCaptchaService
