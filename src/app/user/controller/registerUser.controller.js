const { createController } = require('awilix-router-core')

const registerControllers = (
	registerUserService,
	recoverUserService,
	remindRegisterService
) => ({
	registerUser: async (req, res, next) => {
		try {
			const result = await registerUserService.registerUser(req.body)
			res.send(result)
		} catch (error) {
			next(error)
		}
	},
	recoverUser: async (req, res, next) => {
		try {
			await recoverUserService.recoverUser(req.body)
			res.send('ok')
		} catch (error) {
			next(error)
		}
	},
	remindUser: async (req, res, next) => {
		try {
			await remindRegisterService.remindRegister(req.params.id)
			res.send('ok')
		} catch (error) {
			next(error)
		}
	},
})

module.exports = createController(registerControllers)
	.prefix('/users')
	.post('', 'registerUser')
	.post('/recover', 'recoverUser')
	.all('/:id/register-mail', 'remindUser')

/**
 * @swagger
 * /users:
 *  post:
 *    summary: Register a new user
 *    tags: [Users]
 *    requestBody:
 *      required: true
 *      content:
 *        application/json:
 *          schema:
 *            $ref: '#/components/schemas/User'
 *    responses:
 *      200:
 *        description: registered user record
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/User'
 *
 * /users/recover:
 *  post:
 *    summary: Recover an user's password
 *    tags: [Users]
 *    requestBody:
 *      required: true
 *      content:
 *        application/json:
 *          schema:
 *            type: object
 *            parameters:
 *              email:
 *                type: string
 *            example:
 *                email: user@email.com
 *    description: sends an email for the user to change their password.
 *
 * /users/{userId}/register-mail:
 *  get:
 *    summary: Re-sends the registration email to this user
 *    tags: [Users]
 *    description: Sends email containing a token-ready link to setup their account via the Admin-Site wep application.
 *    parameters:
 *      - $ref: '#/components/parameters/userId'
 *
 */
