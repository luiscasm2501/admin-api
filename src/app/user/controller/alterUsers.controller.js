const { createController } = require('awilix-router-core')
const NotFoundError = require('../../common/controllers/error-handling/notFoundError')
const UpdateError = require('../../common/controllers/error-handling/updateError')

const alterControllers = (updateUserService, deleteUserService) => ({
	/**
   * @swagger
   * /users/{userId}:
   *  put:
   *    summary: Update an user's information
   *    tags: [Users]
   *    parameters:
   *      - $ref: '#/components/parameters/userId'
   *    requestBody:
   *      required: true
   *      content:
   *        application/json:
   *          schema:
   *            $ref: '#/components/schemas/User'
   *    responses:
   *      200:
   *        description: updated user
   *        content:
   *          application/json:
   *            schema:
   *              $ref: '#/components/schemas/User'
   *      404:
   *        $ref: '#/components/responses/notFound'
   * 
   *  delete:
   *   summary: Marks user record as "deleted". If used on a marked user, deletes it completely.
   *   tags: [Users]
   *   parameters:
   *    - $ref: '#/components/parameters/userId'
   *   responses:
   *    200:
   *      description: user succesfully deleted
   *    404:
   *      $ref: '#/components/responses/notFound'
   *  patch:
   *   summary: Restores an user record marked as "deleted" to its original state.
   *   tags: [Users]
   *   parameters:
   *    - $ref: '#/components/parameters/userId'
   *   responses:
   *    200:
   *      description: user succesfully restored
   *    404:
   *      $ref: '#/components/responses/notFound'
   */
	updateUser: async (req, res, next) => {
		try {
			const paramId = req.params.id
			const bodyId = req.body.userId
			if (paramId === bodyId) {
				const result = await updateUserService.updateUser(req.body)
				res.send(result)
			} else {
				next(new UpdateError(paramId, bodyId))
			}
		} catch (error) {
			next(error)
		}
	},

	removeUser: async (req, res, next) => {
		try {
			const id = req.params.id
			const result = await deleteUserService.deleteUser(id)
			if (result) {
				res.send(`User ${id} succesfully marked as deleted`)
			} else {
				next(new NotFoundError(`User ${id}`))
			}
		} catch (error) {
			next(error)
		}
	},
	restoreUser: async (req, res, next) => {
		try {
			const id = req.params.id
			const result = await deleteUserService.restoreUser(id)
			if (result) {
				res.send(`User ${id} succesfully restored`)
			} else {
				next(new NotFoundError(`User ${id}`))
			}
		} catch (error) {
			next(error)
		}
	},
})

module.exports = createController(alterControllers)
	.prefix('/users/:id')
	.put('', 'updateUser')
	.delete('', 'removeUser')
	.patch('', 'restoreUser')
