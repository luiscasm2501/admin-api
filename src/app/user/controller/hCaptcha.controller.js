const { createController } = require('awilix-router-core')

const hCaptchaController = (
	hCaptchaService,
) => ({
	hCaptchaValidate: async (req, res, next) => {
		try {
			const { token } = req.body
			await hCaptchaService.hCaptchaValidate(token)
			res.send('ok')
		} catch (error) {
			next(error)
		}
	},
})

module.exports = createController(hCaptchaController)
	.prefix('/hcaptcha')
	.post('/validate', 'hCaptchaValidate')