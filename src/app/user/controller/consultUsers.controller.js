const { createController } = require('awilix-router-core')
const NotFoundError = require('../../common/controllers/error-handling/notFoundError')

const consultControllers = (getUsersService) => ({
	getUsers: async (req, res, next) => {
		try {
			const result = await getUsersService.getUsers(req.query)
			next(result)
		} catch (error) {
			next(error)
		}
	},
	getUser: async (req, res, next) => {
		try {
			const id = req.params.id
			const result = await getUsersService.getUser(id)
			if (result) res.send(result)
			else next(NotFoundError(`user ${id}`))
		} catch (error) {
			next(error)
		}
	},
})

module.exports = createController(consultControllers)
	.prefix('/users')
	.get('', 'getUsers')
	.get('/:id', 'getUser')	

/**
 * @swagger
 *  /users:
 *    get:
 *      summary: Returns a list of user records
 *      tags: [Users]
 *      parameters:
 *        - $ref: '#/components/parameters/page'
 *        - $ref: '#/components/parameters/size'
 *        - $ref: '#/components/parameters/sort'
 *      responses:
 *        200:
 *          description: list of users
 *          content:
 *            application/json:
 *              schema:
 *                type: array
 *                $ref: '#/components/schemas/User'
 * /users/{userId}:
 *  get:
 *    summary: get an user's information by id
 *    tags: [Users]
 *    parameters:
 *      - $ref: '#/components/parameters/userId'
 *    responses:
 *      200:
 *        description: user record (password isn't retrieved)
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/User'
 *      404:
 *        $ref: '#/components/responses/notFound'
 */
