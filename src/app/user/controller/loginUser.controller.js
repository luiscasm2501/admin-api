const { createController } = require('awilix-router-core')

const loginController = (
	loginUserService,
	logoutUserService,
	getAuthUserService,
	updateUserService
) => ({
	loginUser: async (req, res, next) => {
		try {
			const result = await loginUserService.loginUser(req.body)
			res.send(result)
		} catch (error) {
			next(error)
		}
	},
	updateUser: async (req, res, next) => {
		try {
			const result = await updateUserService.updateUser({
				...req.body,
				userId: req._user,
			})
			res.send(result)
		} catch (error) {
			next(error)
		}
	},
	getUser: async (req, res, next) => {
		try {
			const userId = req._user
			const result = await getAuthUserService.getAuthUser(userId)
			res.send(result)
		} catch (error) {
			next(error)
		}
	},
	logoutUser: async (req, res, next) => {
		try {
			if (req.headers.authorization) {
				const token = req.headers.authorization.split(' ')[1]
				const result = await logoutUserService.logoutUser(token)
				res.send('ok')
			} else {
				res.status(403).end()
			}
		} catch (error) {
			next(error)
		}
	},
})

module.exports = createController(loginController)
	.prefix('/auth')
	.all('/access', 'loginUser')
	.all('/logout', 'logoutUser')
	.get('/me', 'getUser')
	.put('/me', 'updateUser')

/**
 * @swagger
 * /auth/access:
 *  put:
 *    summary: get an access token by providing valid credentials
 *    tags: [Auth]
 *    requestBody:
 *      description: user's credentials
 *      required: true
 *      content:
 *        application/json:
 *          schema:
 *            type: object
 *            properties:
 *              email:
 *                type: string
 *              password:
 *                type: string
 *            example:
 *              email: user@email.com
 *              password: aBc12*3X
 *      responses:
 *        200:
 *          description: fresh access token
 *          content:
 *            application/json:
 *              schema:
 *                type: object
 *                properties:
 *                  token:
 *                    type: string
 *                example:
 *                  token: "Bearer <token>"
 * /auth/logout:
 *  get:
 *    summary: stops current token from being used further
 *    tags: [Auth]
 *    parameters:
 *      - $ref: '#/components/headers/authorization'
 *
 * /auth/me:
 *  get:
 *    tags: [Auth]
 *    summary: get current user's information inside token
 *    parameters:
 *      - $ref: '#/components/headers/authorization'
 *    responses:
 *      200:
 *        description: current user's record
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/User'
 *  put:
 *    tags: [Auth]
 *    summary: update current user from token
 *    parameters:
 *      - $ref: '#/components/headers/authorization'
 *    requestBody:
 *      required: true
 *      content:
 *        application/json:
 *          schema:
 *            $ref: '#/components/schemas/User'
 *    responses:
 *      200:
 *        description: updated current user
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/User'
 */
