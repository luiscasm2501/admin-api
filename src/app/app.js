const express = require('express')
const bodyParser = require('body-parser')
const cors = require('cors')
const kill = require('kill-port')
require('dotenv').config()
const { loadControllers, scopePerRequest } = require('awilix-express')
const { resolve } = require('path')
const {
	logErrorMiddleware,
	returnError,
	databaseErrors,
} = require('./common/controllers/error-handling/errorHandler')
const httpLogger = require('./common/controllers/logger/httpLogger')
const logger = require('./common/controllers/logger/logger')
const validateToken = require('./common/authentication/middleware/authenticateUser')
const unless = require('./common/authentication/middleware/unless')
const bootstrapJobs = require('./common/jobs')
const paginateResponse = require('./common/controllers/pagination/paginateResponse')
const expressHandlebars = require('express-handlebars')
const layouts = require('handlebars-layouts')
const path = require('path')
const makeFileMiddleware = require('./files/config/digitalizeFile.middleware')
const { makeStorageFolders } = require('./files/config/folders')
const swaggerJSDoc = require('swagger-jsdoc')
const swaggerOptions = {
	swaggerDefinition: {
		openapi: '3.0.0',
		info: {
			title: 'Admin-Site API',
			description: 'Admin-Site Documentation',
		},
		security: [{ BearerAuth: [] }],
		servers: [{ url: `${process.env.API_ROOT}${process.env.API_PREFIX}` }],
	},
	apis: [
		`${__dirname}/**/*.controller.js`,
		path.join(__dirname, 'docs/**/*.js'),
	],
}
const swaggerDocs = swaggerJSDoc(swaggerOptions)
const swaggerUI = require('swagger-ui-express')
const getAllowedOrigins = require('./common/authentication/use-cases/getAllowedOrigins')
const ALLOWED_ORIGINS = getAllowedOrigins()
class App {
	constructor(appConfig) {
		this.appConfig = appConfig
	}

	async start(container, callback) {
		const app = this._create(container)
		const port = this.appConfig.port
		await kill(port)
		app.listen(port, callback(port))
	}
	_create(container) {
		const app = express()
		const corsOptions = {
			exposedHeaders: ['X-Total-Count', 'Last-Page', '*'],
			origin: ALLOWED_ORIGINS,
		}

		app.use(bodyParser.json())
		app.use(cors(corsOptions))
		app.use(httpLogger)

		app.use(
			this.appConfig.prefix + '/api-docs',
			swaggerUI.serve,
			swaggerUI.setup(swaggerDocs)
		)

		app.engine(
			'handlebars',
			expressHandlebars({
				defaultLayout: path.join(
					__dirname,
					'files/static/layouts/base.handlebars'
				),
				layoutsDir: path.join(__dirname, 'files/static/layouts'),
				partialsDir: path.join(__dirname, 'files/static/layouts'),
				helpers: Object.assign({}, layouts()),
			})
		)
		makeStorageFolders(container)
		app.set('view engine', 'handlebars')
		app.set('views', path.join(this.appConfig.storage, 'template'))

		app.set('json spaces', 2)
		app.set('json replacer', (k, v) => (v === null ? undefined : v))
		//authentication middleware
		app.use(
			unless(
				validateToken(
					container.resolve('verifyUserService'),
					container.resolve('checkBlackListService'),
					container.resolve('validateInvoiceTokenService'),
					container.resolve('verifyContractService'),
					container.resolve('verifyPortalUserService'),
					container.resolve('verifyPortalRegisterService')
				),
				...this.appConfig.freeRoutes
			)
		)

		app.use(scopePerRequest(container))

		bootstrapJobs(container)

		app.use(
			`${this.appConfig.prefix}/storage`,
			express.static(`${this.appConfig.storage}`)
		)

		app.use('/public', express.static(path.join(__dirname, '../../src/public')))

		const loadedRoutes = loadControllers(
			`${resolve('src')}/**/*.controller.js`,
			{
				cwd: __dirname,
			}
		)
		//Automatically load all controller routes
		app.use(this.appConfig.prefix, loadedRoutes)
		app.use(paginateResponse)
		app.use(makeFileMiddleware(container))
		app.use(logErrorMiddleware)
		app.use(databaseErrors)
		app.use(returnError)
		return app
	}
}

module.exports = App
