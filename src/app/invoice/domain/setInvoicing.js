const { DateTime, Interval } = require('luxon')
const { numberBetween } = require('../../common/domain/betweenValidator')
const parseScheduleType = require('../../templates/contract-template/domain/parsePaymentSchedule')
const getMaxDeadline = require('./getMaxDeadline')

const DEADLINE_DAYS = 15

function setInvoicing(contract, contractTemplate, deadlineDays = undefined) {
	let startDate = DateTime.fromISO(contract.startDate)
	const endDate = DateTime.fromISO(contract.expirationDate)
	const interval = Interval.fromDateTimes(startDate, endDate)
	const maxDeadline =
    getMaxDeadline(contractTemplate.scheduleType, startDate) *
      contractTemplate.paymentSchedule -
    1
	if (!deadlineDays) {
		deadlineDays = DEADLINE_DAYS > maxDeadline ? maxDeadline : DEADLINE_DAYS
	}
	numberBetween(deadlineDays, 2, maxDeadline, 'deadline interval')

	const intervalUnit = parseScheduleType(contractTemplate.scheduleType)
	const intervalNumber = Math.floor(
		interval.length(intervalUnit) / contractTemplate.paymentSchedule
	)

	const invoices = []

	const amount =
    Math.round((contract.totalPayment / intervalNumber) * 100) / 100

	for (let invoiceNum = 0; invoiceNum < intervalNumber; invoiceNum++) {
		const invoice = {
			contractFk: contract.contractId,
			startDate: startDate.toSQLDate(),
			deadline: startDate.plus({ days: deadlineDays }).toSQLDate(),
			amount: amount,
		}
		invoices.push(invoice)
		startDate = startDate.plus({
			[intervalUnit]: contractTemplate.paymentSchedule,
		})
	}
	return invoices
}

module.exports = setInvoicing
