const { DateTime } = require('luxon')
require('dotenv').config()
const mapInvoiceInfo = (
	invoice,
	clientProduct = { Client, Product },
	token
) => {
	return {
		client: {
			name: clientProduct.Client.name,
			email: clientProduct.Client.email,
		},
		product: {
			name: clientProduct.Product.name,
		},
		amount: invoice.amount,
		startDate: DateTime.fromSQL(invoice.startDate).toLocaleString(),
		deadline: DateTime.fromSQL(invoice.deadline).toLocaleString(),
		url: `${process.env.WEB_APP_URL}/register-receipt/invoice/${invoice.invoiceId}/${token}`,
	}
}
module.exports = mapInvoiceInfo
