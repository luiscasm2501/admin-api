const { createController } = require('awilix-router-core')

const registerControllers = (createInvoiceService, updateInvoiceService) => ({
	createInvoices: async (req, res, next) => {
		try {
			const result = await createInvoiceService.createInvoice(
				req.body,
				req.params.contractId
			)
			res.send(result)
		} catch (error) {
			next(error)
		}
	},
	updateInvoices: async (req, res, next) => {
		try {
			const result = await updateInvoiceService.updateInvoice(
				req.body,
				req.params.contractId
			)
			res.send(result)
		} catch (error) {
			next(error)
		}
	},
})

module.exports = createController(registerControllers)
	.prefix('/contracts/:contractId/invoices')
	.post('', 'createInvoices')
	.put('', 'updateInvoices')

/**
 * @swagger
 * /contracts/{contractId}/invoices:
 *  post:
 *    tags: [Invoices]
 *    parameters:
 *      $ref: '#/components/parameters/contractId'
 *    summary: create a new invoice
 *    description: this endpoint is only available for contracts with automatic-invoicing disabled
 *    requestBody:
 *      required: true
 *      content:
 *        application/json:
 *          schema:
 *            $ref: '#/components/schemas/Invoice'
 *    responses:
 *      200:
 *        description: created invoice record
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/Invoice'
 *              type: array
 *  put:
 *    tags: [Invoices]
 *    parameters:
 *      - $ref: '#/components/parameters/contractId'
 *    summary: update an invoice record
 *    description: this endpoint can be used on invoices with 'DRAFT' status
 *    requestBody:
 *      required: true
 *      content:
 *        application/json:
 *          schema:
 *            type: object
 *          examples:
 *            'automatic invoice':
 *              summary: 'automatic: only time interval is updatable'
 *              value:
 *                deadlineDays: 31
 *            'manual invoice':
 *              summary: 'manual: all values are updatable'
 *              value:
 *                amount: 500
 *                startDate: 21-10-2021
 *                deadline: 22-11-2021
 *                invoiceId: <uuid>
 *      responses:
 *        200:
 *          description: updated invoice(s)
 */
