const { createController } = require('awilix-router-core')
const NotFoundError = require('../../common/controllers/error-handling/notFoundError')

const consultControllers = (getInvoicesService) => ({
	getInvoice: async (req, res, next) => {
		try {
			const result = await getInvoicesService.getInvoice(req.params.id)
			if (!result) {
				throw new NotFoundError('invoice')
			}
			res.send(result)
		} catch (error) {
			next(error)
		}
	},
})

module.exports = createController(consultControllers)
	.prefix('/invoices')
	.get('/:id', 'getInvoice')

/**
 * @swagger
 * /invoices/{invoiceId}:
 *  get:
 *    tags: [Invoices]
 *    summary: get an invoice record by id
 *    parameters:
 *      - $ref: '#/components/parameters/Invoice'
 *    responses:
 *      200:
 *        description: fetched invoice record
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/Invoice'
 */
