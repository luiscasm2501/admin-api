const { createController } = require('awilix-router-core')
const NotFoundError = require('../../common/controllers/error-handling/notFoundError')

const consultControllers = (getInvoicesService, getPendingInvoicesService) => ({
	getInvoices: async (req, res, next) => {
		try {
			const result = await getInvoicesService.getInvoices(
				{
					model: 'ContractInvoices',
					conditions: {
						contractFk: req.params.contractId,
					},
				},
				req.query
			)
			next(result)
		} catch (error) {
			next(error)
		}
	},
	getHiringInvoices: async (req, res, next) => {
		try {
			const hiringId = req.params.hiringId
			const result = await getInvoicesService.getInvoices(
				{
					model: 'HiringInvoices',
					conditions: {
						hiringFk: hiringId,
					},
				},
				req.query
			)
			next(result)
		} catch (error) {
			next(error)
		}
	},
	getAffiliateInvoices: async (req, res, next) => {
		try {
			const id = req.params.id
			const result = await getInvoicesService.getInvoices(
				{
					model: 'AffiliateInvoices',
					conditions: {
						contractAffiliateFk: id,
					},
				},
				req.query
			)
			next(result)
		} catch (error) {
			next(error)
		}
	},
	getPendingHiringInvoices: async (req, res, next) => {
		try {
			const result = await getPendingInvoicesService.getPendingInvoices(
				req.query
			)
			next(result)
		} catch (error) {
			next(error)
		}
	},
	getPendingAffiliateInvoices: async (req, res, next) => {
		try {
			const result = await getPendingInvoicesService.getPendingInvoices(
				req.query,
				false,
				'Affiliate'
			)
			next(result)
		} catch (error) {
			next(error)
		}
	},
	getClientInvoices: async (req, res, next) => {
		try {
			const result = await getInvoicesService.getInvoices(
				{
					model: 'ContractInvoices',
					duplicating:false,
					include: [
						{
							model: 'Contract',
							duplicating:false, 
							required: true,
							include: [
								{
									model: 'ClientProduct',
									duplicating:false, 
									required: true,
									include: [
										'Product',
										{
											
											model: 'Client',
											duplicating:false,
											required: true,
											conditions: {
												clientId: req.params.clientId,
											},
										},
									],
								},
							],
						},
					],
				},
				req.query
			)
			next(result)
		} catch (error) {
			next(error)
		}
	},
})

module.exports = createController(consultControllers)
	.get('/contracts/:contractId/invoices', 'getInvoices')
	.get('/hirings/:hiringId/invoices', 'getHiringInvoices')
	.get('/contract-affiliates/:id/invoices', 'getAffiliateInvoices')
	.get('/pending-invoices/hirings', 'getPendingHiringInvoices')
	.get('/pending-invoices/contract-affiliates', 'getPendingAffiliateInvoices')
	.get('/clients/:clientId/invoices', 'getClientInvoices')

/**
 * @swagger
 * /contracts/{contractId}/invoices:
 *  get:
 *    tags: [Invoices]
 *    summary: get list of invoices of a contract
 *    parameters:
 *      - $ref: '#/components/parameters/contractId'
 *      - $ref: '#/components/parameters/page'
 *      - $ref: '#/components/parameters/sort'
 *    responses:
 *      200:
 *        description: list of invoices
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/Invoice'
 *              type: array
 */
