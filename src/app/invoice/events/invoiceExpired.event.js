const logger = require('../../common/controllers/logger/logger')
function registerEvent(bus, container) {
	const event = 'invoiceExpired'
	const ctx = 'sendStartInvoiceService'
	const context = container.resolve(ctx)
	const sendNotificationService = container.resolve('sendNotificationService')

	bus.register(event, (invoice) => {
		context
			.sendStartInvoice(invoice, true)
			.then((result) => {
				const notification = {
					title: `Invoice from ${result.startDate} for ${result.client.name} has reached its deadline`,
					kind: 'WARNING',
					content: {
						description: `$${result.amount} Invoice issued for their ${result.product.name} subscription hasn't been payed yet.`,
						client: result.client.name,
						product: result.product.name,
						amount: result.amount,
						sentOn: result.startDate,
						deadline: result.deadline,
					},
				}
				sendNotificationService.sendNotification(notification)
			})
			.catch((error) => {
				logger.error(`error at sending invoice email [${error.message}]`)
			})
	})
}
module.exports = registerEvent
