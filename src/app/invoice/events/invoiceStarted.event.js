const logger = require('../../common/controllers/logger/logger')
function registerEvent(bus, container) {
	const event = 'invoiceStarted'
	const ctx = 'sendStartInvoiceService'
	const context = container.resolve(ctx)
	const sendNotificationService = container.resolve('sendNotificationService')
	bus.register(event, (invoice) => {
		context
			.sendStartInvoice(invoice)
			.then((result) => {
				const notification = {
					title: `Invoice sent to ${result.client.name} for $${result.amount}`,
					content: {
						description: `Invoice issued for their ${result.product.name} subscription`,
						client: result.client.name,
						product: result.product.name,
						amount: result.amount,
						deadline: result.deadline,
					},
				}
				sendNotificationService.sendNotification(notification)
			})
			.catch((error) => {
				logger.error(`error at sending invoice email [${error.message}]`)
			})
	})
}
module.exports = registerEvent
