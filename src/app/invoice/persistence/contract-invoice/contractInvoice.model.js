const { DataTypes } = require('sequelize')

module.exports = function makeModel(apiDb) {
	const ContractInvoice = apiDb.define(
		'ContractInvoices',
		{},
		{
			paranoid: false,
			timestamps: false,
		}
	)
	return ContractInvoice
}
