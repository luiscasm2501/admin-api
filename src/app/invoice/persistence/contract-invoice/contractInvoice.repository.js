const SequelizeRepo = require('../../../common/persistence/sequilize/sequelizeRepo')

module.exports = class ContractInvoiceRepository extends SequelizeRepo {
	constructor(apiDb) {
		super(apiDb.models.ContractInvoice, apiDb)
	}
}
