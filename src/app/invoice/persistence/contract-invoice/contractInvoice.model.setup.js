module.exports = function setupModel(db) {
	const { Invoice, Contract, ContractInvoices } = db.models

	Invoice.belongsToMany(Contract, {
		through: ContractInvoices,
		foreignKey: 'invoiceFk',
		otherKey: 'contractFk',
		timestamps: false,
	})
	Contract.belongsToMany(Invoice, {
		through: ContractInvoices,
		foreignKey: 'contractFk',
		otherKey: 'invoiceFk',
		timestamps: false,
	})
	ContractInvoices.belongsTo(Contract, { foreignKey: 'contractFk' })
	Contract.hasMany(ContractInvoices, { foreignKey: 'contractFk' })
	ContractInvoices.belongsTo(Invoice, { foreignKey: 'invoiceFk' })
	Invoice.hasMany(ContractInvoices, { foreignKey: 'invoiceFk' })
}
