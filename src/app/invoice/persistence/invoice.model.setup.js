module.exports = function setupModel(db) {
	const { Invoice, Status, Hiring } = db.models

	Status.hasMany(Invoice, {
		foreignKey: 'statusFk',
	})

	Invoice.belongsTo(Status, {
		foreignKey: 'statusFk',
	})

	Invoice.belongsToMany(Hiring, {
		through: 'HiringInvoice',
		foreignKey: 'invoiceFk',
		otherKey: 'hiringFk',
		timestamps: false,
	})
	Hiring.belongsToMany(Invoice, {
		through: 'ContractHiring',
		foreignKey: 'hiringFk',
		otherKey: 'invoiceFk',
		timestamps: false,
	})
}
