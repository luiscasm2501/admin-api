const { DataTypes } = require('sequelize')

module.exports = function makeModel(apiDb) {
	const ContractInvoice = apiDb.define(
		'HiringInvoices',
		{},
		{
			paranoid: false,
			timestamps: false,
		}
	)
	return ContractInvoice
}
