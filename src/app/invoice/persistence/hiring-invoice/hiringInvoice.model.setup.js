module.exports = function setupModel(db) {
	const { Invoice, Hiring, HiringInvoices } = db.models

	Invoice.belongsToMany(Hiring, {
		through: HiringInvoices,
		foreignKey: 'invoiceFk',
		otherKey: 'hiringFk',
		timestamps: false,
	})
	Hiring.belongsToMany(Invoice, {
		through: HiringInvoices,
		foreignKey: 'hiringFk',
		otherKey: 'invoiceFk',
		timestamps: false,
	})
	HiringInvoices.belongsTo(Hiring, { foreignKey: 'hiringFk' })
	Hiring.hasMany(HiringInvoices, { foreignKey: 'hiringFk' })
	HiringInvoices.belongsTo(Invoice, { foreignKey: 'invoiceFk' })
	Invoice.hasMany(HiringInvoices, { foreignKey: 'invoiceFk' })
}
