const SequelizeRepo = require('../../../common/persistence/sequilize/sequelizeRepo')

module.exports = class HiringInvoiceRepository extends SequelizeRepo {
	constructor(apiDb) {
		super(apiDb.models.ContractInvoice, apiDb)
	}
}
