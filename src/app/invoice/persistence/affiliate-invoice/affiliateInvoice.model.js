module.exports = function makeModel(apiDb) {
	const AffiliateInvoice = apiDb.define(
		'AffiliateInvoices',
		{},
		{
			paranoid: false,
			timestamps: false,
		}
	)
	return AffiliateInvoice
}
