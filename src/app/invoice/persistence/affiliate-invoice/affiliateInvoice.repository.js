const SequelizeRepo = require('../../../common/persistence/sequilize/sequelizeRepo')

module.exports = class AffiliateInvoiceRepository extends SequelizeRepo {
	constructor(apiDb) {
		super(apiDb.models.AffiliateInvoice, apiDb)
	}
}
