module.exports = function setupModel(db) {
	const { Invoice, ContractAffiliate, AffiliateInvoices } = db.models

	Invoice.belongsToMany(ContractAffiliate, {
		through: AffiliateInvoices,
		foreignKey: 'invoiceFk',
		otherKey: 'contract Fk',
		timestamps: false,
	})
	ContractAffiliate.belongsToMany(Invoice, {
		through: AffiliateInvoices,
		foreignKey: 'contractAffiliateFk',
		otherKey: 'invoiceFk',
		timestamps: false,
	})
	AffiliateInvoices.belongsTo(ContractAffiliate, {
		foreignKey: 'contractAffiliateFk',
	})
	ContractAffiliate.hasMany(AffiliateInvoices, {
		foreignKey: 'contractAffiliateFk',
	})
	AffiliateInvoices.belongsTo(Invoice, { foreignKey: 'invoiceFk' })
	Invoice.hasMany(AffiliateInvoices, { foreignKey: 'invoiceFk' })
}
