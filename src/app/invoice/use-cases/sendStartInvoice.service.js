const { sendEmail } = require('../../common/services/mailer/mailer')
const mapInvoiceInfo = require('../domain/mapInvoiceInfo')

class sendStartInvoiceService {
	constructor(clientProductRepository, setInvoiceTokenService) {
		this.clientProductRepository = clientProductRepository
		this.setInvoiceTokenService = setInvoiceTokenService
	}
	async sendStartInvoice(invoice, overdue = false) {
		let clientProduct = await this.clientProductRepository.getAll({
			clientProductId: invoice.Contract?.clientProductFk,
			include: ['Client', 'Product'],
		})
		clientProduct = clientProduct.rows[0]
		if (clientProduct) {
			const token = this.setInvoiceTokenService.setInvoiceToken(invoice)
			const mappedInfo = mapInvoiceInfo(invoice, clientProduct, token)

			if (overdue) {
				await sendEmail(
					mappedInfo.client.email,
					`Reminder: invoice for ${mappedInfo.product.name} / Recordatorio: factura por ${mappedInfo.product.name}`,
					'overdueInvoice',
					{
						...mappedInfo,
						title: `Overdue ${mappedInfo.product.name} invoice / Factura vencida de ${mappedInfo.product.name}`,
						button: 'Register Payment / Registrar Pago',
					}
				)
			} else {
				await sendEmail(
					mappedInfo.client.email,
					`Invoice for ${mappedInfo.product.name} - ${mappedInfo.startDate} / Factura por ${mappedInfo.product.name} - ${mappedInfo.startDate}`,
					'startInvoice',
					{
						...mappedInfo,
						button: 'Register Payment / Registrar Pago',
						title: `${mappedInfo.product.name} invoice / ${mappedInfo.product.name} factura`,
					}
				)
			}
			return mappedInfo
		}
	}
}
module.exports = sendStartInvoiceService
