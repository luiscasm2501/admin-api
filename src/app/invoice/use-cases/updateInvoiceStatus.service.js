const { DateTime } = require('luxon')
const statuses = require('../../common/persistence/status/statuses')
module.exports = class updateInvoiceStatus {
	constructor(invoiceRepository, getStatusesService, bus) {
		this.invoiceRepository = invoiceRepository
		this.getStatusesService = getStatusesService
		this.bus = bus
	}
	async updateInvoiceStatus() {
		const now = DateTime.now()
		const foundStatuses = await this.getStatusesService.getStatuses()

		const startResult = await this.invoiceRepository.getAll({
			startDate: { lte: now.toSQLDate() },
			statusFk: this.findStatus(foundStatuses, statuses.DRAFT),
			include: ['Contracts', 'Hirings'],
		})
		const startInvoices = startResult.rows
		const expiredResult = await this.invoiceRepository.getAll({
			deadline: { lte: now.toSQLDate() },
			statusFk: {
				notIn: [
					this.findStatus(foundStatuses, statuses.PAID),
					this.findStatus(foundStatuses, statuses.CANCELLED),
				],
			},
			include: ['Contracts', 'Hirings'],
			overdue: { ne: true },
		})
		const expiredInvoices = expiredResult.rows

		const startedNumber = await this.handleStart(startInvoices)
		const expiredNumber = await this.handleExpired(expiredInvoices)
		return [startedNumber, expiredNumber]
	}

	async handleStart(invoices = []) {
		invoices.forEach((invoice) => {
			this.invoiceRepository.updateStatus(invoice.invoiceId, statuses.SENT)
			invoice = invoice.dataValues
			invoice[`${invoice.Contracts.length ? 'Contract' : 'Hiring'}`] =
        invoice.Contracts[0] || invoice.Hirings[0]
			if (invoice.Contract) {
				this.bus.emit('invoiceStarted', invoice)
			} else {
				this.bus.emit('hiringInvoiceStarted', invoice)
			}
		})
		return invoices.length
	}
	async handleExpired(invoices = []) {
		invoices.forEach((invoice) => {
			this.invoiceRepository.update({
				invoiceId: invoice.invoiceId,
				overdue: true,
			})
			invoice[`${invoice.Contracts.length ? 'Contract' : 'Hiring'}`] =
        invoice.Contracts[0] || invoice.Hirings[0]
			if (invoice.Contract) {
				this.bus.emit('invoiceExpired', invoice)
			} else if (invoices.Hiring) {
				this.bus.emit('hiringInvoiceExpired', invoice)
			}
		})
		return invoices.length
	}

	findStatus(statusList = [], statusName = '') {
		return statusList.find((status) => {
			return status[statusName]
		})[statusName]
	}
}
