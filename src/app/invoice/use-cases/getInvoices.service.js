const statuses = require('../../common/persistence/status/statuses')

class getInvoicesService {
	constructor(invoiceRepository) {
		this.invoiceRepository = invoiceRepository
	}
	async getInvoices(proxyInclude, options) {
		const result = await this.invoiceRepository.getAll({
			include: [
				'Status',
				{ required: true, ...proxyInclude },
				['Receipts', ['Status']],
			],
			...options,
		})
		const mappedResult = result.rows.map((invoice) => {
			const mappedInvoice = invoice.dataValues
			let amount = 0
			invoice.Receipts.forEach((receipt) => {
				if (receipt.Status.name == statuses.APPROVED) {
					amount += receipt.amount
				}
			})
			mappedInvoice['currentAmount'] = (amount * 100.0) / 100.0
			delete mappedInvoice.Receipts
			return mappedInvoice
		})
		return {
			rows: mappedResult,
			count: result.count,
		}
	}
	async getInvoice(invoiceId, proxy = 'Contract') {
		const invoice = await this.invoiceRepository.getById(invoiceId, {
			include: ['Status', ['Receipts', ['Status']], [`${proxy}s`]],
		})
		let amount = 0
		invoice.Receipts.forEach((receipt) => {
			if (receipt.Status.name == statuses.APPROVED) {
				amount += receipt.amount
			}
		})
		const resultInvoice = invoice.dataValues
		resultInvoice['currentAmount'] = (amount * 100.0) / 100.0
		resultInvoice[proxy] = resultInvoice[`${proxy}s`][0]
		delete resultInvoice[`${proxy}s`]

		return resultInvoice
	}
}
module.exports = getInvoicesService
