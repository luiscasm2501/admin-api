const { DateTime, Interval } = require('luxon')
const createToken = require('../../common/authentication/use-cases/createToken')

module.exports = class SetInvoiceTokenService {
	constructor() {}

	setInvoiceToken(invoice, tokenTime = undefined) {
		const start = DateTime.fromISO(invoice.startDate)
		const end = DateTime.fromISO(invoice.deadline)
		const interval = Interval.fromDateTimes(start, end)
		const time = tokenTime || interval.length('hours')
		return createToken({ invoice: invoice.invoiceId }, time + 'h')
	}
}
