const statuses = require('../../common/persistence/status/statuses')
const updateInvoicing = require('../domain/updateInvoicing')
const validateInvoice = require('../domain/validateInvoice')

class updateInvoiceService {
	constructor(invoiceRepository, contractRepository, hiringRepository) {
		this.invoiceRepository = invoiceRepository
		this.contractRepository = contractRepository
		this.hiringRepository = hiringRepository
		this.repo = undefined
	}
	async updateInvoice(invoice, contractId, isHiring = false) {
		this.repo = isHiring ? this.hiringRepository : this.contractRepository
		return this.validate(invoice, contractId, isHiring)
	}

	async validate(invoice, contractId, isHiring) {
		const contract = await this.repo.getById(contractId, {
			include: ['ContractTemplate'],
		})

		if (!contract) {
			throw new NotFoundError(`contract ${contractId}`)
		}
		if (contract.automaticInvoice) {
			const include = {
				model: isHiring ? 'HiringInvoices' : 'ContractInvoices',
				conditions: {
					[isHiring ? 'hiringFk' : 'contractFk']: contractId,
				},
			}
			const result = await this.invoiceRepository.getAll({
				include: [
					{ model: 'Status', conditions: { name: statuses.DRAFT } },
					include,
				],
			})
			const invoices = result.rows
			const updatedInvoices = updateInvoicing(
				contract,
				contract.ContractTemplate,
				invoices,
				invoice.deadlineDays
			)
			return await Promise.all(
				updatedInvoices.map(async (inv) => {
					return await this.invoiceRepository.update(inv)
				})
			)
		} else {
			validateInvoice(invoice)
			return await this.invoiceRepository.update({
				invoiceId: invoice.invoiceId,
				startDate: invoice.startDate,
				deadline: invoice.deadline,
				amount: invoice.amount,
			})
		}
	}
}
module.exports = updateInvoiceService
