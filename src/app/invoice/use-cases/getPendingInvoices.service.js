const statuses = require('../../common/persistence/status/statuses')
const { DateTime } = require('luxon')
module.exports = class getPendingInvoicesService {
	constructor(getSettingsService, invoiceRepository) {
		this.getSettingsService = getSettingsService
		this.invoiceRepository = invoiceRepository
		this.repo = undefined
	}

	async getPendingInvoices(options, count = false, kind = 'Hiring') {
		const settings = await this.getSettingsService.getSetting(
			'hiring-notifications'
		)
		const today = DateTime.now()
		const priorDate = today.plus({ days: settings.advance_days })
		const invoiceProxy = kind + 'Invoices'
		const associateProxy = kind == 'Affiliate' ? 'ContractAffiliate' : kind
		const pendingResult = await this.invoiceRepository.getAll({
			startDate: {
				lte: priorDate.toSQLDate(),
				//  gte: today.toSQLDate(),
			},
			sort: 'startDate',
			include: [
				['Receipts', ['Status']],
				{
					model: invoiceProxy,
					required: true,
					include: [[associateProxy, ['Employee']]],
				},
				{
					model: 'Status',
					required: true,
					conditions: {
						name: [statuses.DRAFT, statuses.SENT, statuses.PARTIAL],
					},
				},
			],
			...options,
		})
		const mapped = pendingResult.rows.map((invoice) => {
			const rawInvoice = invoice.dataValues
			delete rawInvoice[invoiceProxy]
			let amount = 0
			invoice.Receipts.forEach((receipt) => {
				if (receipt.Status.name == statuses.APPROVED) {
					amount += receipt.amount
				}
			})
			rawInvoice['currentAmount'] = (amount * 100.0) / 100.0
			delete rawInvoice.Receipts
			const lowerkind =
        associateProxy.charAt(0).toLowerCase() + associateProxy.slice(1)
			return {
				...rawInvoice,
				Employee: invoice[invoiceProxy][0][associateProxy].Employee,
				[`${lowerkind}Id`]: invoice[invoiceProxy][0][`${lowerkind}Fk`],
			}
		})
		if (count) {
			return { count: pendingResult.count }
		}
		return {
			rows: mapped,
			count: pendingResult.count,
		}
	}
}
