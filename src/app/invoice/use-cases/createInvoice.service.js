const NotFoundError = require('../../common/controllers/error-handling/notFoundError')
const ValidationException = require('../../common/domain/validationException')
const validateInvoice = require('../domain/validateInvoice')

class createInvoiceService {
	constructor(invoiceRepository, contractRepository, hiringRepository) {
		this.invoiceRepository = invoiceRepository
		this.contractRepository = contractRepository
		this.hiringRepository = hiringRepository
		this.repo = undefined
	}
	async createInvoice(invoices, contractId, isHiring = false) {
		this.repo = isHiring ? this.hiringRepository : this.contractRepository
		invoices = await this.validate(invoices, contractId, isHiring)
		const result = await this.invoiceRepository.createList(invoices)
		const contractInstance = await this.repo.getById(contractId)
		contractInstance.addInvoices(result)
		return result?.length > 1 ? result : result[0]
	}

	async validate(invoices, contractId, isHiring) {
		const contract = await this.repo.getById(contractId)

		if (!contract) {
			throw new NotFoundError(`contract ${contractId}`)
		}
		if (contract.automaticInvoice) {
			throw new ValidationException(
				'this contract has automatic invoicing enabled'
			)
		}
		/*
    let currentAmount = await this.invoiceRepository.doOperation({
      operation: 'sum',
      attribute: 'amount',
      conditions: { contractFk: contractId },
    })*/
		const proxy = isHiring ? 'Hiring' : 'Contract'
		const lowerCaseProxy = proxy.charAt(0).toLocaleLowerCase() + proxy.slice(1)
		let currentAmount = await this.invoiceRepository.doOperation({
			operation: 'sum',
			attribute: 'amount',
			include: [
				{
					model: `${proxy}Invoices`,
					conditions: {
						[`${lowerCaseProxy}Fk`]: contractId,
					},
					attr: [],
				},
			],
			conditions: {
				[`$${proxy}Invoices.${lowerCaseProxy}Fk$`]: contractId,
			},
		})
		invoices = Array.isArray(invoices) ? invoices : [invoices]

		invoices.forEach((invoice) => {
			validateInvoice(invoice)
			if (invoice.amount + currentAmount > contract.totalPayment) {
				if (currentAmount === contract.totalPayment)
					throw new ValidationException(
						`This contract has enough invoices to cover its total amount of '${currentAmount}''`
					)
				else
					throw new ValidationException(
						`invoice amount '${
							invoice.amount
						}' exceeds contract's total amount of '${
							contract.totalPayment
						}' by '${invoice.amount + currentAmount - contract.totalPayment}'.`
					)
			}
			invoice[`${lowerCaseProxy}Fk`] = contractId
		})
		return invoices
	}
}
module.exports = createInvoiceService
