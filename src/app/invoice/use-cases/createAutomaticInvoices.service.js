const setInvoicing = require('../domain/setInvoicing')

module.exports = class CreateAutomaticInvoicesService {
	constructor(invoiceRepository) {
		this.invoiceRepository = invoiceRepository
	}
	async createAutomaticInvoices(contract, contractTemplate, deadlineDays) {
		const invoices = setInvoicing(contract, contractTemplate, deadlineDays)

		const createdInvoices = await this.invoiceRepository.createList(invoices)
		return await contract.addInvoices(createdInvoices)
	}
}
