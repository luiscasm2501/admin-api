const UnauthorizedError = require('../../common/controllers/error-handling/unauthorizedError')

class validateInvoiceTokenService {
	constructor(invoiceRepository) {
		this.invoiceRepository = invoiceRepository
	}
	async validateInvoiceToken(invoiceId) {
		const existentInvoice = await this.invoiceRepository.getById(invoiceId, {
			include: ['Status'],
		})
		if (!existentInvoice) {
			throw new UnauthorizedError(`invoice #${invoiceId} from payload`)
		}
	}
}
module.exports = validateInvoiceTokenService
