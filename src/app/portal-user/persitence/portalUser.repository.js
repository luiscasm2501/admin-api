const SequelizeRepo = require('../../common/persistence/sequilize/sequelizeRepo')

class portalUserRepository extends SequelizeRepo {
	constructor(apiDb) {
		super(apiDb.models.PortalUser, apiDb)
	}
}

module.exports = portalUserRepository