module.exports = function setupModel(db) {
	const { PortalUser, Employee, UserAffiliate } = db.models

	PortalUser.belongsToMany(Employee, {
		through: UserAffiliate,
		foreignKey: 'portalUserFk',
		otherKey: 'affiliateFk',
		timestamps: false,
	})
	Employee.belongsToMany(PortalUser, {
		through: UserAffiliate,
		foreignKey: 'affiliateFk',
		otherKey: 'portalUserFk',
		timestamps: false,
	})
	
	UserAffiliate.belongsTo(Employee, { foreignKey: 'affiliateFk' })
	Employee.hasOne(UserAffiliate, { foreignKey: 'affiliateFk' })
	UserAffiliate.belongsTo(PortalUser, { foreignKey: 'portalUserFk' })
	PortalUser.hasOne(UserAffiliate, { foreignKey: 'portalUserFk' })	
}
