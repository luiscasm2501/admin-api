module.exports = function makeModel(apiDb) {
	const UserAffiliate = apiDb.define(
		'UserAffiliate',
		{},
		{
			paranoid: false,
			timestamps: false,
		}
	)
	return UserAffiliate
}
