const SequelizeRepo = require('../../../common/persistence/sequilize/sequelizeRepo')

module.exports = class userAffiliateRepository extends SequelizeRepo {
	constructor(apiDb) {
		super(apiDb.models.UserAffiliate, apiDb)
	}
}
