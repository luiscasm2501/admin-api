module.exports = function setupModel(db) {
	const { PortalUser, Client, UserClient } = db.models

	PortalUser.belongsToMany(Client, {
		through: UserClient,
		foreignKey: 'portalUserFk',
		otherKey: 'clientFk',
		timestamps: false,
	})
	Client.belongsToMany(PortalUser, {
		through: UserClient,
		foreignKey: 'clientFk',
		otherKey: 'portalUserFk',
		timestamps: false,
	})
	UserClient.belongsTo(PortalUser, { foreignKey: 'portalUserFk' })
	PortalUser.hasOne(UserClient, { foreignKey: 'portalUserFk' })
	UserClient.belongsTo(Client, { foreignKey: 'clientFk' })
	Client.hasOne(UserClient, { foreignKey: 'clientFk' })
}
