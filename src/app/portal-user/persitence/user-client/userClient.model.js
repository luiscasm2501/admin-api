module.exports = function makeModel(apiDb) {
	const UserClient = apiDb.define(
		'UserClient',
		{},
		{
			paranoid: false,
			timestamps: false,
		}
	)
	return UserClient
}
