const SequelizeRepo = require('../../../common/persistence/sequilize/sequelizeRepo')

module.exports = class userClientRepository extends SequelizeRepo {
	constructor(apiDb) {
		super(apiDb.models.UserClient, apiDb)
	}
}
