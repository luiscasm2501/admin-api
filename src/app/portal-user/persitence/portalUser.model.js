const { DataTypes } = require('sequelize')

module.exports = function makeModel(apiDb) {
	const PortalUser = apiDb.define(
		'PortalUser',
		{   
			username: {
				type:DataTypes.STRING(50),				
				allowNull:false,
			},
			password: {
				type: DataTypes.STRING,
				allowNull: false,			      
			},      
			portalUserId: {
				type: DataTypes.UUID,
				allowNull: false,
				primaryKey: true,
				defaultValue: DataTypes.UUIDV4,
			},
		},
		{ paranoid: true, indexes: [{ unique: true, fields: ['username'] }] }
	)
	return PortalUser
}