const betweenValidator = require('../../common/domain/betweenValidator')
const validateId = require('../../common/domain/idValidator')

module.exports = function portalUserEntity(
	{ username, password, portalUserId },
	update = false
) {	
	(update && !username) ||	
	betweenValidator.stringBetween(username, 2, 50, 'username')
	update && validateId(portalUserId)

	return Object.freeze({
		username,         
		password,
		portalUserId,
	})
}
