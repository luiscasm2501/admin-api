const UnauthorizedError = require('../../common/controllers/error-handling/unauthorizedError')

class verifyPortalUserService {
	constructor(portalUserRepository) {
		this.portalUserRepository = portalUserRepository
	}
	async verifyUser(portalUser) {
		const userRecord = await this.portalUserRepository.getById(portalUser, {
			include: [{ model: 'UserClient', required: false }],
		})
		if (userRecord) {
			return { client: userRecord.UserClient?.clientFk }
		}
		throw new UnauthorizedError('invalid portal user in token')
	}
}
module.exports = verifyPortalUserService
