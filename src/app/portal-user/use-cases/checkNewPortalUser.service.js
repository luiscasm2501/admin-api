class checkNewPortalUserService {
	constructor(portalUserRepository, clientRepository) {
		this.portalUserRepository = portalUserRepository
		this.clientRepository = clientRepository
	}
	async checkNewPortalUser(username) {
		const result = await this.portalUserRepository.getAll({
			username: username,
		})
		return { exists: result.count > 0 }
	}
	async checkNewPortalEmail(email) {
		const result = await this.clientRepository.getAll({
			email: email,
		})
		return { exists: result.count > 0 }
	}
}
module.exports = checkNewPortalUserService
