const NotFoundError = require('../../common/controllers/error-handling/notFoundError')
const {
	encryptPassword,
} = require('../../common/controllers/encryption/encryptor')

module.exports = class registerPortalUserService {
	constructor(
		portalUserRepository,
		userClientRepository,
		userAffiliateRepository,
		clientRepository,
		associateRepository
	) {
		this.portalUserRepository = portalUserRepository
		this.userClientRepository = userClientRepository
		this.userAffiliateRepository = userAffiliateRepository
		this.clientRepository = clientRepository
		this.associateRepository = associateRepository
	}
	/*
	For now i'm finding the client/affiliate by email but it could be changed for id	
    user = {
        email:string,
        password:string,
        user:string,
    }  
    */

	registerByContext(User, context) {
		switch (context) {
		case 'client':
			return this.registerClientUser(User)
		case 'affiliate':
			return this.registerAffiliateUser(User)
		case 'both':
			return this.registerBothUser(User)
		default:
			throw new Error('Context not found')
		}
	}

	async registerClientUser(User) {
		return await this.register(
			User,
			this.clientRepository,
			this.userClientRepository,
			'UserClient',
			'clientId',
			'clientFk',
			'client'
		)
	}

	async registerAffiliateUser(User) {
		return await this.register(
			User,
			this.associateRepository,
			this.userAffiliateRepository,
			'UserAffiliate',
			'employeeId',
			'affiliateFk',
			'affiliate'
		)
	}

	async register(
		User,
		entityRepo,
		userEntityRepo,
		interEntity,
		entityId,
		entityFk,
		notFound
	) {
		const entity = await entityRepo.getById(User.proxyId, {
			include: [interEntity],
		})
		if (!entity) {
			throw new NotFoundError(notFound)
		}
		if (entity[interEntity]) {
			throw new Error('This email already has a user')
		}
		User.password = await encryptPassword(User.password)
		var portalUser = await this.portalUserRepository.create(User)
		try {
			let newUser = { portalUserFk: portalUser.portalUserId }
			newUser[entityFk] = entity[entityId] //sintaxis to add a property key given by variable
			var entityUser = await userEntityRepo.create(newUser)
		} catch (err) {
			await this.portalUserRepository.remove(portalUser.portalUserId, true)
			throw err
		}
		return { portalUser, entityUser }
	}

	async registerBothUser(User) {
		const client = await this.clientRepository.getById(User.proxyId, {
			include: ['UserClient'],
		})
		client = client.count ? client.rows[0] : undefined
		if (!client) {
			throw new NotFoundError('Client')
		}
		if (client.UserClient) {
			throw new Error('This email already has a user')
		}
		var affiliate = await this.associateRepository.getAll({
			email: User.email,
			include: ['UserAffiliate'],
		})
		affiliate = affiliate.count ? affiliate.rows[0] : undefined
		if (!affiliate) {
			throw new NotFoundError('Affiliate')
		}
		if (affiliate.UserAffiliate) {
			throw new Error('This email already has a user')
		}
		User.password = await encryptPassword(User.password)
		var portalUser = await this.portalUserRepository.create(User)
		try {
			var affiliateUser = await this.userAffiliateRepository.create({
				portalUserFk: portalUser.portalUserId,
				affiliateFk: affiliate.employeeId,
			})
			var clientUser = await this.userClientRepository.create({
				portalUserFk: portalUser.portalUserId,
				clientFk: client.clientId,
			})
		} catch (err) {
			await this.portalUserRepository.remove(portalUser.portalUserId, true)
			throw err
		}
		return { portalUser, affiliateUser, clientUser }
	}

	async verifyAndLink(user) {
		let foundClient = await this.clientRepository.getAll({
			email: user.email,
			include: ['UserClient'],
		})
		foundClient = foundClient.count ? foundClient.rows[0] : undefined
		let foundAffiliate = await this.associateRepository.getAll({
			email: user.email,
			include: ['UserAffiliate'],
		})
		foundAffiliate = foundAffiliate.count ? foundAffiliate.rows[0] : undefined
		//if Email is in both tables
		if (foundClient && foundAffiliate) {
			//If is asociated with af and not with client
			if (!foundClient.UserClient && foundAffiliate.UserAffiliate) {
				let clientUser = await this.userClientRepository.create({
					portalUserFk: foundAffiliate.UserAffiliate.portalUserFk,
					clientFk: foundClient.clientId,
				})
				if (clientUser) {
					let foundUser = await this.portalUserRepository.getOne({
						conditions: {
							portalUserId: foundAffiliate.UserAffiliate.portalUserFk,
						},
					})
					if (foundUser) return { client: foundClient, user: foundUser }
					throw new NotFoundError('asociated user')
				} else throw new Error('Problem linking user')
			}
			//If it's associated with client and not af
			else if (foundClient.UserClient && !foundAffiliate.UserAffiliate) {
				let affiliateUser = await this.userAffiliateRepository.create({
					portalUserFk: foundClient.UserClient.portalUserFk,
					affiliateFk: foundAffiliate.employeeId,
				})
				if (affiliateUser) {
					let foundUser = await this.portalUserRepository.getOne({
						conditions: { portalUserId: foundClient.UserClient.portalUserFk },
					})
					if (foundUser) return { affiliate: foundAffiliate, user: foundUser }
					throw new NotFoundError('asociated user')
				} else throw new Error('Problem linking user')
			}
			//If it's not asociated with either them
			else if (!foundClient.UserClient && !foundAffiliate.UserAffiliate) {
				return { client: foundClient, affiliate: foundAffiliate }
			}
			//already both likned
			else {
				throw new Error('Both already linked to a user')
			}
		} else {
			return false
		}
	}
}
