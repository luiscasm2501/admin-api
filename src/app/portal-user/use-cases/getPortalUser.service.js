module.exports = class registerUserService {
	constructor(portalUserRepository) {
		this.portalUserRepository = portalUserRepository
	}

	async getUsers(options) {
		return await this.portalUserRepository.getAll({
			include: ['username', 'portalUserId'],
			exclude: ['password'],
			...options,
		})
	}

	async getUser(id, options) {
		return await this.portalUserRepository.getById(id, {
			include: ['username', 'portalUserId'],
			exclude: ['password'],
			...options,
		})
	}
}