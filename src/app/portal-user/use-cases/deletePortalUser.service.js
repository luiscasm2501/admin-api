class deletePortalUserService {
	constructor(portalUserRepository) {
		this.portalUserRepository = portalUserRepository
	}
	async deleteUser(userId) {
		return await this.portalUserRepository.remove(userId)
	}
	async restoreUser(userId) {
		return await this.portalUserRepository.restore(userId)
	}
}
module.exports = deletePortalUserService