const createToken = require('../../common/authentication/use-cases/createToken')
const createPortalUserPayload = require('../../common/authentication/use-cases/createPortalUserPayload')
const ValidationException = require('../../common/domain/validationException')
const NotFoundError = require('../../common/controllers/error-handling/notFoundError')
const { sendEmail } = require('../../common/services/mailer/mailer')
require('dotenv').config()
module.exports = class recoverPortalUserService {
	constructor(portalUserRepository, clientRepository, associateRepository) {
		this.portalUserRepository = portalUserRepository
		this.clientRepository = clientRepository
		this.associateRepository = associateRepository
		this.contexts = ['client', 'affiliate', 'both']
	}
	async recoverUser(user) {
		var cond = {}
		var context = ''
		var userData = {}
		var foundClient = await this.clientRepository.getAll({
			email: user.email,
			include: ['UserClient'],
		})
		foundClient = foundClient.count ? foundClient.rows[0] : undefined
		var foundAffiliate = await this.associateRepository.getAll({
			email: user.email,
			include: ['UserAffiliate'],
		})
		foundAffiliate = foundAffiliate.count ? foundAffiliate.rows[0] : undefined
		if (foundClient) {
			cond = { portalUserId: foundClient.UserClient.portalUserFk }
			context = this.contexts[0]
			userData = foundClient
		} else if (foundAffiliate) {
			cond = { portalUserId: foundAffiliate.UserAffiliate.portalUserFk }
			context = this.contexts[1]
			userData = foundAffiliate
		} else {
			throw new NotFoundError('email')
		}
		var foundUser = await this.portalUserRepository.getOne({ conditions: cond })
		if (foundUser) {
			const token = createToken(
				createPortalUserPayload(foundUser.portalUserId, context),
				'8h'
			)
			const url = `${process.env.CLIENT_WEB_APP_RECOVER_USER_URL}?token=${token}`
			return await sendEmail(
				user.email,
				'Recover your Cacao\'s user password / Recupera tu contraseña de usuario de Cacao',
				'portal_forgot_password',
				{
					userData,
					url,
					button: 'Change Password / Cambiar contraseña',
					title: 'Recover your password / Recuperar contraseña',
				}
			)
		}
		throw new ValidationException('Couldn\'t find user')
	}
}
