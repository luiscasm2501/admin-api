const createToken = require('../../common/authentication/use-cases/createToken')
const createRegisterPayload = require('../../common/authentication/use-cases/createRegisterPayload')
const ValidationException = require('../../common/domain/validationException')
const { sendEmail } = require('../../common/services/mailer/mailer')
require('dotenv').config()
module.exports = class sendPortalUserEmailService {
	constructor(clientRepository) {
		this.clientRepository = clientRepository
	}
	async sendClientRegistration(client) {
		if (client) {
			if (typeof client === 'string') {
				client = await this.clientRepository.getById(client)
			}
			const token = createToken(
				createRegisterPayload(client.clientId, 'client'),
				'72h'
			)
			const url = `${process.env.CLIENT_AM_WEB_APP_URL}/portal/register/${token}`
			return await sendEmail(
				client.email,
				'Create your Cacao\'s user and password / Crea tu usuario y contraseña de Cacao',
				'portal_register_client',
				{
					client,
					url,
					button: 'Sign Up / Registrarse',
					title: 'Complete your Cacao account / Completa tu cuenta Cacao',
				}
			)
		}
		throw new ValidationException('Couldn\'t find client')
	}

	async sendAffiliateRegistration(affiliate) {
		if (affiliate) {
			const token = createToken(
				createRegisterPayload(affiliate.employeeId, 'affiliate'),
				'72h'
			)
			const url = `${process.env.CLIENT_AM_WEB_APP_URL}/portal/register/${token}`
			return await sendEmail(
				affiliate.email,
				'Create your Cacao\'s user and password / Crea tu usuario y contraseña de Cacao',
				'portal_register_affiliate',
				{
					affiliate,
					url,
					button: 'Sign Up / Registrarse',
					title: 'Complete your Cacao account / Completa tu cuenta Cacao',
				}
			)
		}
		throw new ValidationException('Couldn\'t find affiliate')
	}

	async sendRegisterBoth(userData) {
		if (userData) {
			let affiliate = userData.affiliate
			const token = createToken(
				createRegisterPayload(affiliate.employeeId, 'both'),
				'72h'
			)
			const url = `${process.env.CLIENT_AM_WEB_APP_URL}/portal/register/${token}`
			return await sendEmail(
				affiliate.email,
				'Create your Cacao\'s user and password / Crea tu usuario y contraseña de Cacao',
				'portal_register_both',
				{
					affiliate,
					url,
					button: 'Sign Up / Registrarse',
					title: 'Complete your Cacao account / Completa tu cuenta Cacao',
				}
			)
		}
		throw new ValidationException('Couldn\'t find user data')
	}
	async sendUserLinked(userData) {
		if (userData) {
			const url = `${process.env.CLIENT_AM_WEB_APP_URL}/portal/login`
			return await sendEmail(
				userData.client ? userData.client.email : userData.affiliate.email,
				'Login to Cacao\'s site with your existent user and password / Inicie sesión en el sitio de Cacao con su usuario y contraseña existentes',
				'portal_linked_user',
				{
					userData,
					url,
					button: 'Access Account / Acceder a la cuenta',
					title: 'Login into your account / Inicie sesión en su cuenta',
				}
			)
		}
		throw new ValidationException('Couldn\'t find user data')
	}
}
