const {
	encryptPassword,
} = require('../../common/controllers/encryption/encryptor')
const portalUserEntity = require('../domain/portalUserEntity')

module.exports = class registerPortalUserService {
	constructor(portalUserRepository) {
		this.portalUserRepository = portalUserRepository
	}

	async updateUser(user) {
		portalUserEntity(user, true)
		if (user.password) {
			user.password = await encryptPassword(user.password)			
		}
		return await this.portalUserRepository.update(user)
	}
}
