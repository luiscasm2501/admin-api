const NotFoundError = require('../../common/controllers/error-handling/notFoundError')

module.exports = class getAuthPortalUserService {
	constructor(portalUserRepository) {
		this.portalUserRepository = portalUserRepository
		this.contexts = ['client','affiliate','both']		
	}	

	async getAuthUser(id) {
		var foundUser = await this.portalUserRepository.getAll({
			portalUserId: id,
			include: [
				{model:'UserAffiliate', include:['Employee']},
				{model:'UserClient', include: ['Client']}],
		})
		foundUser = foundUser.count ? foundUser.rows[0] : undefined
		if (foundUser) {
			return {
				portalUserId:foundUser.portalUserId,
				context: this.findUserContext(foundUser),
				firstName: this.findUserFirstName(foundUser),					
				lastName: this.findUserLastName(foundUser),	
				username: foundUser.username,
				email: this.findUserEmail(foundUser),
				clientId: foundUser.UserClient ? foundUser.UserClient.Client.clientId : undefined,
				affiliateId: foundUser.UserAffiliate ? foundUser.UserAffiliate.Employee.employeeId: undefined
			}								
		}
		throw new NotFoundError('user')
	}

	findUserContext(foundPortUser){
		if(foundPortUser.UserClient && foundPortUser.UserAffiliate) 
			return this.contexts[2] 
		else if (foundPortUser.UserClient)
			return this.contexts[0]
		else if (foundPortUser.UserAffiliate)
			return this.contexts[1]
		else
			throw new NotFoundError('relation with client or affiliate') 
	}

	findUserFirstName(user){
		return user.UserAffiliate 
			? user.UserAffiliate.Employee.firstName
			: user.UserClient.Client.name.split(' ')[0] 
	}

	findUserLastName(user){
		return user.UserAffiliate 
			? user.UserAffiliate.Employee.LastName
			: user.UserClient.Client.name.split(' ')[1] 
	}
    
	findUserEmail(user){
		return user.UserAffiliate 
			? user.UserAffiliate.Employee.email
			: user.UserClient.Client.email
	}

}


