const createToken = require('../../common/authentication/use-cases/createToken')
const NotFoundError = require('../../common/controllers/error-handling/notFoundError')
const createPortalUserPayload = require('../../common/authentication/use-cases/createPortalUserPayload')
const {
	checkPassword,
} = require('../../common/controllers/encryption/encryptor')
const UnauthorizedError = require('../../common/controllers/error-handling/unauthorizedError')
class loginPortalUserService {
	constructor(
		portalUserRepository, 
		userClientRepository, 
		userAffiliateRepository, 
		clientRepository, 
		associateRepository )
	{		
		this.portalUserRepository = portalUserRepository
		this.userClientRepository = userClientRepository
		this.userAffiliateRepository = userAffiliateRepository
		this.clientRepository = clientRepository
		this.associateRepository = associateRepository
		this.contexts = ['client','affiliate','both']
	}

	async loginUser(user, time = null) {		
		if (user) {
			var cond = {}
			var context = ''
			var foundUser = {}
			var firstName = ''
			var lastName = ''
			var clientId = undefined
			var affiliateId = undefined			
			const email = user.email.indexOf('@')			
			//if it found an @ then it is an email
			if(email != -1){				
				let foundClient = await this.clientRepository.getAll({
					email: user.email,
					include: ['UserClient'],
				})
				foundClient = foundClient.count ? foundClient.rows[0] : undefined
				let foundAffiliate = await this.associateRepository.getAll({
					email: user.email,
					include: ['UserAffiliate'],
				})
				foundAffiliate = foundAffiliate.count ? foundAffiliate.rows[0] : undefined
				if(foundClient && foundAffiliate){																			
					cond = foundClient.UserClient ?
						{portalUserId:foundClient.UserClient.portalUserFk} :
						{portalUserId:foundAffiliate.UserAffiliate.portalUserFk}
					context = this.contexts[2]
					firstName = foundAffiliate.firstName
					lastName = foundAffiliate.lastName
					clientId = foundClient.clientId
					affiliateId = foundAffiliate.employeeId					
				}																		
				else if(foundClient){
					cond = {portalUserId:foundClient.UserClient.portalUserFk}
					context = this.contexts[0]
					firstName = foundClient.name.split(' ')[0]
					lastName = foundClient.name.split(' ')[1]
					clientId = foundClient.clientId
				}
				else if (foundAffiliate){
					cond = {portalUserId:foundAffiliate.UserAffiliate.portalUserFk}
					context = this.contexts[1]
					firstName = foundAffiliate.firstName
					lastName = foundAffiliate.lastName
					affiliateId = foundAffiliate.employeeId	
				}
				else{
					throw new NotFoundError('email') 
				}												 																																													
				foundUser = await this.portalUserRepository.getOne({conditions: cond})												
			}
			else{
				foundUser = await this.portalUserRepository.getAll({
					username: user.email,
					include: [
						{model:'UserAffiliate', include:['Employee']},
						{model:'UserClient', include: ['Client']}],
				})
				foundUser = foundUser.count ? foundUser.rows[0] : undefined				 				
				if (foundUser) {
					context = this.findUserContext(foundUser)
					firstName = this.findUserFirstName(foundUser)					
					lastName = this.findUserLastName(foundUser)
					clientId = foundUser.UserClient ? foundUser.UserClient.Client.clientId : undefined
					affiliateId = foundUser.UserAffiliate ? foundUser.UserAffiliate.Employee.employeeId: undefined								
				}				
			}
			if (foundUser) {						
				const verified = await checkPassword(user.password, foundUser.password)
				if (verified) {																																			
					const token = createToken(createPortalUserPayload(foundUser.portalUserId,context), time)
					return {
						token,
						context: context,
						user:{
							portalUserId: foundUser.portalUserId,
							firstName: firstName,
							lastName: lastName,
							username: foundUser.username,
							email: user.email,
							clientId: clientId,
							affiliateId: affiliateId,
						}						
						
					}
					
				}
				throw new UnauthorizedError('password and email don\'t match')
			}else{
				throw new NotFoundError('user')
			}			
		}
		throw new UnauthorizedError()		
	}
	
	findUserContext(foundPortUser){
		if(foundPortUser.UserClient && foundPortUser.UserAffiliate) 
			return this.contexts[2] 
		else if (foundPortUser.UserClient)
			return this.contexts[0]
		else if (foundPortUser.UserAffiliate)
			return this.contexts[1]
		else
			throw new NotFoundError('relation with client or affiliate') 
	}

	findUserFirstName(user){
		return user.UserAffiliate 
			? user.UserAffiliate.Employee.firstName
			: user.UserClient.Client.name.split(' ')[0] 
	}

	findUserLastName(user){
		return user.UserAffiliate 
			? user.UserAffiliate.Employee.LastName
			: user.UserClient.Client.name.split(' ')[1] 
	}	
}
module.exports = loginPortalUserService
