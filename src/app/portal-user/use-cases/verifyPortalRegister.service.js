const UnauthorizedError = require('../../common/controllers/error-handling/unauthorizedError')

class verifyPortalRegisterService {
	constructor(clientRepository, associateRepository) {
		this.clientRepository = clientRepository
		this.associateRepository = associateRepository
	}
	async verifyRegister(toRegister, context) {
		let userRecord = {}
		if (context == 'client') {
			userRecord = await this.clientRepository.getById(toRegister)
		} else if (context == 'affiliate') {
			userRecord = await this.associateRepository.getById(toRegister)
		} else {
			throw new UnauthorizedError('invalid client/affiliate in token')
		}
		if (userRecord) {
			return true
		}
		throw new UnauthorizedError('invalid client/affiliate in token')
	}
}
module.exports = verifyPortalRegisterService
