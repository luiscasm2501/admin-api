const { createController } = require('awilix-router-core')
const NotFoundError = require('../../common/controllers/error-handling/notFoundError')
const UpdateError = require('../../common/controllers/error-handling/updateError')

const alterPortalUserControllers = (updatePortalUserService, deletePortalUserService) => ({	
	updateUser: async (req, res, next) => {
		try {
			const paramId = req.params.id
			const bodyId = req.body.portalUserId
			if (paramId === bodyId) {
				const result = await updatePortalUserService.updateUser(req.body)
				res.send(result)
			} else {
				next(new UpdateError(paramId, bodyId))
			}
		} catch (error) {
			next(error)
		}
	},
	removeUser: async (req, res, next) => {
		try {
			const id = req.params.id
			const result = await deletePortalUserService.deleteUser(id)
			if (result) {
				res.send(`User ${id} succesfully marked as deleted`)
			} else {
				next(new NotFoundError(`User ${id}`))
			}
		} catch (error) {
			next(error)
		}
	},
	restoreUser: async (req, res, next) => {
		try {
			const id = req.params.id
			const result = await deletePortalUserService.restoreUser(id)
			if (result) {
				res.send(`User ${id} succesfully restored`)
			} else {
				next(new NotFoundError(`User ${id}`))
			}
		} catch (error) {
			next(error)
		}
	},
})
module.exports = createController(alterPortalUserControllers)
	.prefix('/portal-user/:id')
	.put('', 'updateUser')
	.delete('', 'removeUser')
	.patch('', 'restoreUser')    

/**
   * @swagger
   * /portal-user/{userId}:
   *  put:
   *    summary: Update an user's information
   *    tags: [Portal Users]
   *    parameters:
   *      - $ref: '#/components/parameters/userId'
   *    requestBody:
   *      required: true
   *      content:
   *        application/json:
   *          schema:
   *            $ref: '#/components/schemas/PortalUser'
   *    responses:
   *      200:
   *        description: updated user
   *        content:
   *          application/json:
   *            schema:
   *              $ref: '#/components/schemas/PortalUser'
   *      404:
   *        $ref: '#/components/responses/notFound'
   * 
   *  delete:
   *   summary: Marks user record as "deleted". If used on a marked user, deletes it completely.
   *   tags: [Portal Users]
   *   parameters:
   *    - $ref: '#/components/parameters/userId'
   *   responses:
   *    200:
   *      description: user succesfully deleted
   *    404:
   *      $ref: '#/components/responses/notFound'
   *  patch:
   *   summary: Restores an user record marked as "deleted" to its original state.
   *   tags: [Portal Users]
   *   parameters:
   *    - $ref: '#/components/parameters/userId'
   *   responses:
   *    200:
   *      description: user succesfully restored
   *    404:
   *      $ref: '#/components/responses/notFound'
   */