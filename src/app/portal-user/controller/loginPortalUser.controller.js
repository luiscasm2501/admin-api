const { createController } = require('awilix-router-core')

const portalLoginController = (
	loginPortalUserService,
	logoutPortalUserService,
	updatePortalUserService,
	getAuthPortalUserService
) => ({
	loginUser: async (req, res, next) => {
		try {
			const result = await loginPortalUserService.loginUser(req.body)
			res.set('Authorization', 'Bearer ' + result.token)
			res.send(result)
		} catch (error) {
			next(error)
		}
	},
	updateUser: async (req, res, next) => {
		try {
			const result = await updatePortalUserService.updateUser({
				...req.body,
				portalUserId: req._portalUser,
			})
			res.send(result)
		} catch (error) {
			next(error)
		}
	},
	getUser: async (req, res, next) => {
		try {
			const userId = req._portalUser
			const result = await getAuthPortalUserService.getAuthUser(userId)
			res.send(result)
		} catch (error) {
			next(error)
		}
	},
	logoutUser: async (req, res, next) => {
		try {
			if (req.headers.authorization) {
				const token = req.headers.authorization.split(' ')[1]
				const result = await logoutPortalUserService.logoutUser(token)
				res.send('ok')
			} else {
				res.status(403).end()
			}
		} catch (error) {
			next(error)
		}
	},
})

module.exports = createController(portalLoginController)
	.prefix('/portal-auth')
	.all('/access', 'loginUser')
	.all('/logout', 'logoutUser')
	.get('/me', 'getUser')
	.put('/me', 'updateUser')

/**
 * @swagger
 * /portal-auth/access:
 *  put:
 *    summary: get an access token by providing valid credentials
 *    tags: [Portal Auth]
 *    requestBody:
 *      description: user's credentials
 *      required: true
 *      content:
 *        application/json:
 *          schema:
 *            type: object
 *            properties:
 *              email:
 *                type: string
 *              password:
 *                type: string
 *            example:
 *              email: user@email.com
 *              password: aBc12*3X
 *  responses:
 *      200:
 *        description: submitted and created clients's user information
 *        content:
 *          application/json:
 *            schema:
 *             type: object
 *            properties:
 *              token:
 *                type: string
 *              userId:
 *                type: string
 *            example:
 *              token: secret_token
 *              userId: "3fa85f64-5717-4562-b3fc-2c963f66afa6"
 *              context: "client"
 *
 * /portal-auth/logout:
 *  get:
 *    summary: stops current token from being used further
 *    tags: [Portal Auth]
 *    parameters:
 *      - $ref: '#/components/headers/authorization'
 *
 * /portal-auth/me:
 *  get:
 *    tags: [Portal Auth]
 *    summary: get current user's information inside token
 *    parameters:
 *      - $ref: '#/components/headers/authorization'
 *    responses:
 *      200:
 *        description: current user's record
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/PortalUser'
 *  put:
 *    tags: [Portal Auth]
 *    summary: update current user from token
 *    parameters:
 *      - $ref: '#/components/headers/authorization'
 *    requestBody:
 *      required: true
 *      content:
 *        application/json:
 *          schema:
 *            $ref: '#/components/schemas/PortalUser'
 *    responses:
 *      200:
 *        description: updated current user
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/PortalUser'
 */
