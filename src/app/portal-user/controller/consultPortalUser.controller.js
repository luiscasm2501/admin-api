const { createController } = require('awilix-router-core')
const NotFoundError = require('../../common/controllers/error-handling/notFoundError')

const consultPortalUserControllers = (
	getPortalUserService,
	checkNewPortalUserService
) => ({
	getUsers: async (req, res, next) => {
		try {
			const result = await getPortalUserService.getUsers(req.query)
			next(result)
		} catch (error) {
			next(error)
		}
	},
	getUser: async (req, res, next) => {
		try {
			const id = req.params.id
			const result = await getPortalUserService.getUser(id)
			if (result) res.send(result)
			else next(NotFoundError(`user ${id}`))
		} catch (error) {
			next(error)
		}
	},
	checkNewUser: async (req, res, next) => {
		try {
			const result = await checkNewPortalUserService.checkNewPortalUser(
				req.params.username
			)
			res.send(result)
		} catch (error) {
			next(error)
		}
	},
	checkNewUserEmail: async (req, res, next) => {
		try {
			const result = await checkNewPortalUserService.checkNewPortalEmail(
				req.params.email
			)
			res.send(result)
		} catch (error) {
			next(error)
		}
	},
})

module.exports = createController(consultPortalUserControllers)
	.prefix('/portal-user')
	.get('', 'getUsers')
	.get('/:id', 'getUser')
	.get('/email/:email', 'checkNewUserEmail')
	.get('/username/:username', 'checkNewUser')

/**
 * @swagger
 *  /portal-user:
 *    get:
 *      summary: Returns a list of user records
 *      tags: [Portal Users]
 *      parameters:
 *        - $ref: '#/components/parameters/page'
 *        - $ref: '#/components/parameters/size'
 *        - $ref: '#/components/parameters/sort'
 *      responses:
 *        200:
 *          description: list of users
 *          content:
 *            application/json:
 *              schema:
 *                type: array
 *                $ref: '#/components/schemas/PortalUser'
 * /portal-user/{userId}:
 *  get:
 *    summary: get an user's information by id
 *    tags: [Portal Users]
 *    parameters:
 *      - $ref: '#/components/parameters/userId'
 *    responses:
 *      200:
 *        description: user record (password isn't retrieved)
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/PortalUser'
 *      404:
 *        $ref: '#/components/responses/notFound'
 */
