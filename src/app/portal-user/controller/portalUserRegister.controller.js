const { createController } = require('awilix-router-core')

const registerControllers = (
	registerPortalUserService,
	recoverPortalUserService,
	sendPortalUserEmailService
) => ({
	registerUser: async (req, res, next) => {
		try {
			const result = await registerPortalUserService.registerByContext(
				{ ...req.body, proxyId: req._toRegisterId },
				req._context
			)
			res.send(result)
		} catch (error) {
			next(error)
		}
	},
	registerClientUser: async (req, res, next) => {
		try {
			const result = await registerPortalUserService.registerClientUser(
				req.body
			)
			res.send(result)
		} catch (error) {
			next(error)
		}
	},
	registerAffiliateUser: async (req, res, next) => {
		try {
			const result = await registerPortalUserService.registerAffiliateUser(
				req.body
			)
			res.send(result)
		} catch (error) {
			next(error)
		}
	},
	registerBothUser: async (req, res, next) => {
		try {
			const result = await registerPortalUserService.registerBothUser(req.body)
			res.send(result)
		} catch (error) {
			next(error)
		}
	},
	recoverUser: async (req, res, next) => {
		try {
			await recoverPortalUserService.recoverUser(req.body)
			res.send('ok')
		} catch (error) {
			next(error)
		}
	},
	//Still missing method to resend registration email
	resendUser: async (req, res, next) => {
		try {
			await sendPortalUserEmailService.sendClientRegistration(
				req.params.clientId
			)
			res.send('ok')
		} catch (error) {
			next(error)
		}
	},
})

module.exports = createController(registerControllers)
	.prefix('/portal-user')
	.post('/clients', 'registerClientUser')
	.post('/affiliates', 'registerAffiliateUser')
	.post('/clientaffiliate', 'registerBothUser')
	.post('/recover', 'recoverUser')
	.post('/register', 'registerUser')
	.put('/clients/:clientId/register-email', 'resendUser')
/**
 * @swagger
 * /portal-user/clients:
 *  post:
 *    tags: [Portal Users]
 *    summary: create a new portal-user for a Client
 *    requestBody:
 *      required: true
 *      content:
 *        application/json:
 *          schema:
 *            $ref: '#/components/schemas/PortalUserReqBody'
 *    responses:
 *      200:
 *        description: submitted and created clients's user information
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/PortalUser'
 *
 *
 * /portal-user/affiliates:
 *  post:
 *    tags: [Portal Users]
 *    summary: create a new portal-user for an affiliate
 *    requestBody:
 *      required: true
 *      content:
 *        application/json:
 *          schema:
 *            $ref: '#/components/schemas/PortalUserReqBody'
 *    responses:
 *      200:
 *        description: submitted and created affiliates's user information
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/PortalUser'
 *
 * /portal-user/clientaffiliate:
 *  post:
 *    tags: [Portal Users]
 *    summary: create a new portal-user for an affiliate and a client with the same email
 *    requestBody:
 *      required: true
 *      content:
 *        application/json:
 *          schema:
 *            $ref: '#/components/schemas/PortalUserReqBody'
 *    responses:
 *      200:
 *        description: submitted and created affiliates's and client's user information
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/PortalUser'
 *
 * /portal-user/recover:
 *  post:
 *    summary: Recover an user's password
 *    tags: [Portal Users]
 *    requestBody:
 *      required: true
 *      content:
 *        application/json:
 *          schema:
 *            type: object
 *            parameters:
 *              email:
 *                type: string
 *            example:
 *                email: user@email.com
 *    description: sends an email for the user to change their password.
 */
