const { createController } = require('awilix-express')
const upload = require('../../files/config/files.config')
const { VERIFY_FILE_FIELDS } = require('../constants')

const verificationController = (createClientVerificationService) => ({
	createVerification: async (req, res, next) => {
		try {
			const clientId = req._client
			const result =
        await createClientVerificationService.createClientVerification(
        	{
        		...req.body,
        		clientFk: clientId,
        	},
        	req.files
        )
			if (result.exists) {
				res.send(result.item)
			} else {
				// Only register with storage middleware on creation
				next(result.item)
			}
		} catch (error) {
			next(error)
		}
	},
})

module.exports = createController(verificationController)
	.prefix('/client-verification')
	.post('', 'createVerification')
	.before(
		upload.fields([
			{ name: VERIFY_FILE_FIELDS.address, maxCount: 1 },
			{ name: VERIFY_FILE_FIELDS.identity, maxCount: 1 },
			{ name: VERIFY_FILE_FIELDS.signature, maxCount: 1 },
		])
	)
