const { createController } = require('awilix-express')
const ForbiddenError = require('../../common/controllers/error-handling/forbiddenError')

const verificationController = (
	getClientVerificationService,
	reviewClientVerificationService
) => ({
	getVerifications: async (req, res, next) => {
		try {
			const clientId = req._client
			if (clientId) {
				const result = await getClientVerificationService.getClientVerification(
					clientId
				)
				res.send(result)
			} else if (req._user) {
				const result =
          await getClientVerificationService.getClientVerifications(req.query)
				next(result)
			} else {
				throw new ForbiddenError(
					'Only admins and soliciting client can access this resource'
				)
			}
		} catch (error) {
			next(error)
		}
	},
	getSingleVerification: async (req, res, next) => {
		try {
			if (!req._user) {
				throw new ForbiddenError('Only admins can access this resource')
			}
			const result = await getClientVerificationService.getClientVerification(
				req.params.clientId
			)
			res.send(result)
		} catch (error) {
			next(error)
		}
	},
	reviewVerification: async (req, res, next) => {
		try {
			if (!req._user) {
				throw new ForbiddenError('Only admins can access this resource')
			}
			const result =
        await reviewClientVerificationService.reviewClientVerification({
        	clientId: req.params.clientId,
        	...req.body,
        })

			res.send(result)
		} catch (error) {
			next(error)
		}
	},
})

module.exports = createController(verificationController)
	.get('/client/:clientId/client-verification', 'getSingleVerification')
	.get('/client-verification', 'getVerifications')
	.put('/client/:clientId/client-verification', 'reviewVerification')
