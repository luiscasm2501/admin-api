const ValidationException = require('../../common/domain/validationException')
const statuses = require('../../common/persistence/status/statuses')
const { IMGS } = require('../../common/services/mailer/constant')
const { sendEmail } = require('../../common/services/mailer/mailer')

module.exports = class ReviewClientVerificationService {
	constructor(clientVerificationRepository) {
		this.verificationRepository = clientVerificationRepository
	}

	async reviewClientVerification({ clientId, isApproved, comment }) {
		const clientVerification = await this.verificationRepository.getOne({
			conditions: { clientFk: clientId },
			include: ['Status', 'Client'],
		})
		if (!clientVerification) {
			throw new ValidationException(
				`No verification request found for client [${clientId}]`
			)
		}
		if (!isApproved && !comment) {
			throw new ValidationException(
				'Please provide a reason why this verification request wasn\'t approved'
			)
		}
		const clientVerificationId = clientVerification.clientVerificationId
		const newStatus = isApproved ? statuses.APPROVED : statuses.REJECTED
		if (newStatus === clientVerification.Status.name) {
			throw new ValidationException(`This verification is already ${newStatus}`)
		}
		try {
			if (comment) {
				const commentResult = await this.verificationRepository.update({
					clientVerificationId,
					feedbackComment: comment,
				})
				if (isApproved === undefined) {
					return commentResult
				}
			}
			const result = await this.verificationRepository.updateStatus(
				clientVerificationId,
				newStatus
			)
			const emailInfo = {
				email: clientVerification.Client.email,
				name: clientVerification.Client.name,
				comment: comment,
				url: process.env.CLIENT_AM_WEB_APP_URL,
			}
			if (isApproved) {
				await sendEmail(
					clientVerification.Client.email,
					'Your verify request has been approved / Su solicitud de verificación ha sido aprobada',
					'verification_approved',
					{ ...emailInfo, title: 'Verify Approved / Verificación aprobada', img: IMGS.SUCCESS }
				)
			} else {
				await sendEmail(
					clientVerification.Client.email,
					'Your verify request has been rejected / Su solicitud de verificación ha sido rechazada',
					'verification_rejected',
					{
						...emailInfo,
						title: 'Verify Rejected / Verificación rechazada',
						img: IMGS.ERROR,
						button: 'Go to site / Ir al sitio',
					}
				)
			}
			return result
		} catch (error) {
			await this.verificationRepository.updateStatus(
				clientVerificationId,
				statuses.PENDING_REVIEW
			)
			throw error
		}
	}
}
