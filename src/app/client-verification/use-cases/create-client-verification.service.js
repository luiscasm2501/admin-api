const ValidationException = require('../../common/domain/validationException')
const { rm } = require('fs')
const { GET_VERIFY_FOLDER, VERIFY_FILE_FIELDS } = require('../constants')
const statuses = require('../../common/persistence/status/statuses')
class CreateClientVerificationService {
	constructor(
		addressRepository,
		clientVerificationRepository,
		clientRepository,
		createVerificationFilesService,
		sendNotificationService
	) {
		this.verificationRepository = clientVerificationRepository
		this.addressRepository = addressRepository
		this.clientRepository = clientRepository
		this.createVerificationFilesService = createVerificationFilesService
		this.sendNotificationService = sendNotificationService
	}

	async createClientVerification(verification, files) {
		const exists = await this.verificationRepository.getOne({
			conditions: { clientFk: verification.clientFk },
		})
		if (!exists) {
			Object.values(VERIFY_FILE_FIELDS).forEach((field) => {
				if (!files[field])
					throw new ValidationException(
						`Request is missing required files [${field}]`
					)
			})
		}
		const client = await this.clientRepository.getById(verification.clientFk)
		try {
			if (verification.residencyStatus === 'Non-Resident') {
				if (!verification.permanentAddress && !exists?.permanentAddress)
					throw new ValidationException(
						'Missing permanent address for non-resident status'
					)
				await this.addressRepository.update({
					addressId: client?.addressFk,
					permanentAddress: verification.permanentAddress,
				})
			}
			let item = undefined
			if (exists) {
				item = await this.verificationRepository.update({
					clientVerificationId: exists.clientVerificationId,
					...verification,
				})
				await this.verificationRepository.updateStatus(
					exists.clientVerificationId,
					statuses.PENDING_REVIEW
				)
			} else {
				item = await this.verificationRepository.create({
					...verification,
				})
			}
			await this.sendNotificationService.sendNotification({
				title: `${client.name} has ${
					exists ? 'updated' : 'submitted'
				} a verification request`,
				content: {
					description:
            'Please review this request under "Clients" section and approve it or reject it accordingly',
					clientId: client.clientId,
				},
			})
			await this.createVerificationFilesService.createVerificationFiles({
				files: (Array.isArray(files) ? files : Object.values(files)).map(
					(file) => {
						file = Array.isArray(file) ? file[0] : file
						return {
							...file,
							type: verification[`${file.fieldname}Type`],
						}
					}
				),
				clientVerificationId: item.clientVerificationId,
			})
			return { item, exists: !!exists }
		} catch (error) {
			if (!exists) {
				rm(GET_VERIFY_FOLDER(verification.clientFk), () => {})
				await this.verificationRepository.remove(
					verification.clientFk,
					true,
					'clientFk'
				)
			}
			throw error
		}
	}
}
module.exports = CreateClientVerificationService
