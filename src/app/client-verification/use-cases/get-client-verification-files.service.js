module.exports = class GetClientVerificationFilesService {
	constructor(verificationFileRepository, verificationFileMetaRepository) {
		this.verificationFileRepository = verificationFileRepository
		this.verificationFileMetaRepository = verificationFileMetaRepository
	}

	async getClientVerificationFiles(clientVerificationId) {
		const files = (
			await this.verificationFileRepository.getAll({
				clientVerificationFk: clientVerificationId,
			})
		).rows
		const parsedFiles = []
		for (let file of files) {
			const fileMeta = await this.verificationFileMetaRepository.getById(
				file.verificationFileId
			)
			parsedFiles.push({
				...file.dataValues,
				url: fileMeta.url,
				ext: fileMeta.type,
				filename: fileMeta.name,
			})
		}
		return parsedFiles
	}
}
