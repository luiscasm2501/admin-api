const { VERIFY_FILE_FIELDS, VERIFY_FILE_CONTEXT } = require('../constants')

module.exports = class CreateVerificationFilesService {
	constructor(createFileMetaService, verificationFileRepository) {
		this.createFileMetaService = createFileMetaService
		this.verificationFileRepository = verificationFileRepository
	}
	async createVerificationFiles(
		{ files, clientVerificationId } = { files: {} }
	) {
		const previousFiles = (
			await this.verificationFileRepository.getAll({
				clientVerificationFk: clientVerificationId,
			})
		).rows
		for (let file of Array.isArray(files) ? files : Object.values(files)) {
			if (Array.isArray(file)) {
				file = file[0]
			}
			const fileMatch = previousFiles.find(
				(prevFile) =>
					file.fieldname &&
          VERIFY_FILE_FIELDS[prevFile.context] === file.fieldname
			)
			let createdFile
			if (fileMatch) {
				createdFile = await this.verificationFileRepository.update({
					...fileMatch.dataValues,
					...file,
				})
			} else {
				createdFile = await this.verificationFileRepository.create({
					...file,
					context: Object.values(VERIFY_FILE_CONTEXT).find((context) =>
						file.fieldname.includes(context)
					),
					clientVerificationFk: clientVerificationId,
				})
			}
			await this.createFileMetaService.createFileMeta({
				...file,
				verificationFileId: createdFile.verificationFileId,
			})
		}
	}
}
