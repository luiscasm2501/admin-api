module.exports = class getClientVerificationService {
	constructor(clientVerificationRepository, getClientVerificationFilesService) {
		this.verificationRepository = clientVerificationRepository
		this.getClientVerificationFilesService = getClientVerificationFilesService
	}
	async getClientVerification(clientId) {
		const clientVerification = await this.verificationRepository.getOne({
			include: ['Status', 'Client'],
			conditions: { clientFk: clientId },
		})
		if (clientVerification) {
			return {
				...clientVerification.dataValues,
				VerificationFiles:
          await this.getClientVerificationFilesService.getClientVerificationFiles(
          	clientVerification.clientVerificationId
          ),
			}
		}
		return clientVerification
	}
	async getClientVerifications(options = {}) {
		return await this.verificationRepository.getAll({
			...options,
			include: ['Status', 'Client'],
		})
	}
}
