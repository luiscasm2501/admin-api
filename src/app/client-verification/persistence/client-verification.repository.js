const SequelizeRepo = require('../../common/persistence/sequilize/sequelizeRepo')
const statuses = require('../../common/persistence/status/statuses')

class ClientVerificationRepository extends SequelizeRepo {
	constructor(apiDb) {
		super(apiDb.models.ClientVerification, apiDb, statuses.PENDING_REVIEW)
	}
}

module.exports = ClientVerificationRepository
