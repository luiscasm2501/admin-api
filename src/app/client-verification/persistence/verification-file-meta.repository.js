const MongoRepo = require('../../common/persistence/mongo/mongoRepo')

module.exports = class verificationFileMetaRepository extends MongoRepo {
	constructor(nosqlDb) {
		super(nosqlDb, 'verification_files')
	}
}
