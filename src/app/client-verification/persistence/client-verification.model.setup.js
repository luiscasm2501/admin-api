module.exports = function setupModel(db) {
	const { ClientVerification, VerificationFile, Client, Status } = db.models
	Client.hasOne(ClientVerification, {
		foreignKey: 'clientFk',
		onDelete: 'CASCADE',
	})

	ClientVerification.belongsTo(Client, {
		foreignKey: { name: 'clientFk', allowNull: false },
	})

	Status.hasMany(ClientVerification, {
		foreignKey: 'statusFk',
	})

	ClientVerification.belongsTo(Status, {
		foreignKey: { name: 'statusFk', allowNull: false },
	})

	ClientVerification.hasMany(VerificationFile, {
		foreignKey: 'clientVerificationFk',
		onDelete: 'CASCADE',
	})

	VerificationFile.belongsTo(ClientVerification, {
		foreignKey: { name: 'clientVerificationFk', allowNull: false },
	})
}
