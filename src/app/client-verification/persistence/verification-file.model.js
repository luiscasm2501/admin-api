const { DataTypes } = require('sequelize')
const { VERIFY_FILE_CONTEXT } = require('../constants')

module.exports = function makeModel(apiDb) {
	const VerificationFile = apiDb.define(
		'VerificationFile',
		{
			verificationFileId: {
				type: DataTypes.UUID,
				allowNull: false,
				primaryKey: true,
				defaultValue: DataTypes.UUIDV4,
			},
			context: {
				type: DataTypes.STRING(20),
				allowNull: false,
				validate: {
					isIn: [Object.values(VERIFY_FILE_CONTEXT)],
				},
			},
			type: {
				type: DataTypes.STRING(20),
				allowNull: true,
			},
		},
		{ paranoid: false }
	)
	return VerificationFile
}
