const SequelizeRepo = require('../../common/persistence/sequilize/sequelizeRepo')

class VerificationFileRepository extends SequelizeRepo {
	constructor(apiDb) {
		super(apiDb.models.VerificationFile, apiDb)
	}
}

module.exports = VerificationFileRepository
