const { DataTypes } = require('sequelize')

module.exports = function makeModel(apiDb) {
	const ClientVerification = apiDb.define(
		'ClientVerification',
		{
			clientVerificationId: {
				type: DataTypes.UUID,
				allowNull: false,
				primaryKey: true,
				defaultValue: DataTypes.UUIDV4,
			},
			firstName: {
				type: DataTypes.STRING(50),
				allowNull: false,
			},
			secondName: {
				type: DataTypes.STRING(50),
				allowNull: true,
			},
			surname: {
				type: DataTypes.STRING(50),
				allowNull: false,
			},
			secondSurname: {
				type: DataTypes.STRING(50),
				allowNull: true,
			},
			gender: {
				type: DataTypes.STRING(20),
				allowNull: false,
				validate: {
					isIn: [['F', 'M', 'O']],
				},
			},
			civilStatus: {
				type: DataTypes.STRING(20),
				allowNull: false,
				validate: {
					isIn: [['Single', 'Married', 'Other']],
				},
			},
			referencePerson: {
				type: DataTypes.STRING(100),
				allowNull: false,
			},
			birthDate: {
				allowNull: false,
				type: DataTypes.DATEONLY,
			},
			nationality: {
				type: DataTypes.STRING(100),
				allowNull: false,
			},
			residencyStatus: {
				type: DataTypes.STRING(20),
				allowNull: false,
				validate: {
					isIn: [['Citizen', 'Resident', 'Non-Resident']],
				},
			},
			companyName: {
				type: DataTypes.STRING(100),
				allowNull: true,
			},
			companyIdentifier: {
				type: DataTypes.STRING(100),
				allowNull: true,
			},
			companyCountry: {
				type: DataTypes.STRING(100),
				allowNull: true,
			},
			feedbackComment: {
				type: DataTypes.STRING(500),
				allowNull: true,
			},
		},
		{ paranoid: false }
	)
	return ClientVerification
}
