require('dotenv').config()
const { existsSync, mkdirSync } = require('fs')
const path = require('path')
const { FOLDERS } = require('../files/config/folders')
const VERIFY_FILE_CONTEXT = {
	address: 'address',
	identity: 'identity',
	signature: 'signature',
}

const VERIFY_FILE_FIELDS = {
	address: VERIFY_FILE_CONTEXT.address + 'File',
	identity: VERIFY_FILE_CONTEXT.identity + 'File',
	signature: VERIFY_FILE_CONTEXT.signature + 'File',
}

const GET_VERIFY_FOLDER = (clientId) => {
	const folder = path.join(
		process.env.FILE_SRC,
		FOLDERS.CLIENT_VERIFY,
		clientId
	)
	!existsSync(folder) && mkdirSync(folder, { recursive: true })
	return folder
}
const EVENTS = {
	verification_created:'verificationCreated'
}
module.exports = { VERIFY_FILE_FIELDS, GET_VERIFY_FOLDER, VERIFY_FILE_CONTEXT, EVENTS }
