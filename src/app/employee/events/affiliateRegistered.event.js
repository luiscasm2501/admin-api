const trycatchevent = require('../../common/events/trycatchevent')

function affiliateRegisteredEvent(bus, container) {
	const event = 'affiliateRegistered'
	const ctx = 'sendPortalUserEmailService'
	const email = container.resolve(ctx)
	const register = container.resolve('registerPortalUserService')
	bus.register(event, (employee) => {
		trycatchevent(async () => {
			let verification = await register.verifyAndLink(employee)				
			if(verification.affiliate && verification.client){
				email.sendRegisterBoth(verification)
			}		
			else if(verification.affiliate){
				email.sendUserLinked(verification)
			}
			else if(!verification){
				email.sendAffiliateRegistration(employee)	
			}		
		})
	})
}
module.exports = affiliateRegisteredEvent