const { createController } = require('awilix-router-core')
const NotFoundError = require('../../common/controllers/error-handling/notFoundError')
const UpdateError = require('../../common/controllers/error-handling/updateError')

const alterControllers = (updateEmployeeService, removeEmployeeService) => ({
	updateEmployee: async (req, res, next) => {
		try {
			const paramId = req.params.id
			const bodyId = req.body.employeeId
			if (paramId === bodyId) {
				const result = await updateEmployeeService.updateEmployee(req.body)
				res.send(result)
			} else {
				next(new UpdateError(paramId, bodyId))
			}
		} catch (error) {
			next(error)
		}
	},
	removeEmployee: async (req, res, next) => {
		try {
			const id = req.params.id
			const result = await removeEmployeeService.removeEmployee(id)
			if (result) {
				res.send(`Employee ${id} succesfully marked as deleted`)
			} else {
				next(new NotFoundError(`Employee ${id}`))
			}
		} catch (error) {
			next(error)
		}
	},
	restoreEmployee: async (req, res, next) => {
		try {
			const id = req.params.id
			const result = await removeEmployeeService.restoreEmployee(id)
			if (result) {
				res.send(`Employee ${id} succesfully restored`)
			} else {
				next(new NotFoundError(`Employee ${id}`))
			}
		} catch (error) {
			next(error)
		}
	},
})

const employeePrefix = '/employees/:id'
const affiliatePrefix = '/affiliates/:id'
module.exports = createController(alterControllers)
	.put(employeePrefix, 'updateEmployee')
	.delete(employeePrefix, 'removeEmployee')
	.patch(employeePrefix, 'restoreEmployee')
	.put(affiliatePrefix, 'updateEmployee')
	.delete(affiliatePrefix, 'removeEmployee')
	.patch(affiliatePrefix, 'restoreEmployee')

/**
 * @swagger
 * /employees/{employeeId}:
 *  put:
 *    tags: [Employees]
 *    summary: update an employee's information by id
 *    parameters:
 *      - $ref: '#/components/parameters/employeeId'
 *    requestBody:
 *      required: true
 *      content:
 *        application/json:
 *          schema:
 *            $ref: '#/components/schemas/Employee'
 *    responses:
 *      200:
 *        description: update employee information
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/Employee'
 *  delete:
 *    tags: [Employees]
 *    summary: delete an employee record by id
 *    description: on the first go, it marks it a deleted, if used again, deletes it permanently
 *    parameters:
 *      - $ref: '#/components/parameters/employeeId'
 *    responses:
 *      200:
 *        description: employee successfully deleted / marked as "deleted"
 *  patch:
 *    tags: [Employees]
 *    summary: restores an employee record marked as "deleted"
 *    parameters:
 *      - $ref: '#/components/parameters/employeeId'
 *    responses:
 *      200:
 *        description: employee successfully restored
 */
