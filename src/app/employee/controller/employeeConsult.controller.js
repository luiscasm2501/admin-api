const { createController } = require('awilix-router-core')
const NotFoundError = require('../../common/controllers/error-handling/notFoundError')

const consultControllers = (getEmployeesService) => ({
	getEmployees: async (req, res, next) => {
		try {
			const result = await getEmployeesService.getEmployees(req.query)
			next(result)
		} catch (error) {
			next(error)
		}
	},
	getAffiliates: async (req, res, next) => {
		try {
			const result = await getEmployeesService.getEmployees(req.query, true)
			next(result)
		} catch (error) {
			next(error)
		}
	},	
	getEmployee: async (req, res, next) => {
		try {
			const result = await getEmployeesService.getEmployee(req.params.id)
			if (result) res.send(result)
			else next(new NotFoundError(`employee ${req.params.id}`))
		} catch (error) {
			next(error)
		}
	},	
})

module.exports = createController(consultControllers)
	.get('/affiliates', 'getAffiliates')
	.get('/employees', 'getEmployees')
	.get('/employees/:id', 'getEmployee')
	.get('/affiliates/:id', 'getEmployee')		

/**
 * @swagger
 * /employees:
 *  get:
 *    tags: [Employees]
 *    summary: retrieve a list of employee records
 *    parameters:
 *      - $ref: '#/components/parameters/page'
 *      - $ref: '#/components/parameters/sort'
 *    responses:
 *      200:
 *        description: list of employees
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/Employee'
 *              type: array
 * /employees/{employeeId}:
 *  get:
 *    tags: [Employees]
 *    summary: get an employee record by id
 *    parameters:
 *      - $ref: '#/components/parameters/employeeId'
 *    responses:
 *      200:
 *        description: employee record
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/Employee'
 *  
 */
