const { createController } = require('awilix-router-core')

const registerControllers = (registerEmployeeService) => ({
	registerEmployee: async (req, res, next) => {
		try {
			const result = await registerEmployeeService.registerEmployee({
				...req.body,
				kind: 'employee',
			})
			res.send(result)
		} catch (error) {
			next(error)
		}
	},
	registerAffiliate: async (req, res, next) => {
		try {
			const result = await registerEmployeeService.registerEmployee({
				...req.body,
				kind: 'affiliate',
			})
			res.send(result)
		} catch (error) {
			next(error)
		}
	},
})

module.exports = createController(registerControllers)
	.post('/affiliates', 'registerAffiliate')
	.post('/employees', 'registerEmployee')

/**
 * @swagger
 * /employees:
 *  post:
 *    tags: [Employees]
 *    summary: create a new employee record
 *    requestBody:
 *      required: true
 *      content:
 *        application/json:
 *          schema:
 *            $ref: '#/components/schemas/Employee'
 *    responses:
 *      200:
 *        description: submitted and created employee information
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/Employee'
 * /affiliates:
 *  post:
 *    tags: [Employees]
 *    summary: create a new affiliate record
 *    requestBody:
 *      required: true
 *      content:
 *        application/json:
 *          schema:
 *            $ref: '#/components/schemas/Employee'
 *    responses:
 *      200:
 *        description: submitted and created affiliate information
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/Employee'
 *
 */
