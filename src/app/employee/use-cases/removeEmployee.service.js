module.exports = class RemoveEmployeeService {
	constructor(associateRepository) {
		this.associateRepository = associateRepository
	}

	async removeEmployee(id) {
		if (id) return await this.associateRepository.remove(id)
	}
	async restoreEmployee(id) {
		if (id) return await this.associateRepository.restore(id)
	}
}
