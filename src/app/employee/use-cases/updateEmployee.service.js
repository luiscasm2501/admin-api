const addressEntity = require('../../address/domain/addressEntity')
const employeeEntity = require('../domain/employeeEntity')

module.exports = class updateEmployeeService {
	constructor(associateRepository, addAffiliateTemplateService) {
		this.associateRepository = associateRepository
		this.addAffiliateTemplateService = addAffiliateTemplateService
	}

	async updateEmployee(employee) {
		employeeEntity(employee, true)
		if (employee.Address) addressEntity(employee.Address, true)
		if (employee.templateId) {
			await this.addAffiliateTemplateService.addAffiliateTemplate(
				employee.employeeId,
				employee.templateId
			)
		}
		return await this.associateRepository.update(employee)
	}
}
