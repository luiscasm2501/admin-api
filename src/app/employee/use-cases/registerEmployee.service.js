const addressEntity = require('../../address/domain/addressEntity')
const employeeEntity = require('../domain/employeeEntity')

module.exports = class registerEmployeeService {
	constructor(
		associateRepository,
		addAffiliateTemplateService,
		kindRepository,
		bus
	) {
		this.associateRepository = associateRepository
		this.addAffiliateTemplateService = addAffiliateTemplateService
		this.kindRepository = kindRepository
		this.bus = bus
	}

	async registerEmployee(employee) {
		employeeEntity(employee)
		if (employee.Address) addressEntity(employee.Address)
		const kind = await this.kindRepository.getOne({
			conditions: { name: employee.kind },
		})
		employee['kindFk'] = kind.kindId
		const createdEmployee = await this.associateRepository.create(employee)
		if (employee.kind == 'affiliate') {
			await this.addAffiliateTemplateService.addAffiliateTemplate(
				createdEmployee.employeeId,
				employee.templateId
			)
			this.bus.emit('affiliateRegistered', {
				...employee,
				employeeId: createdEmployee.employeeId,
			})
		}
		return createdEmployee
	}
}
