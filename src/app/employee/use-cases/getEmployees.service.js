const NotFoundError = require('../../common/controllers/error-handling/notFoundError')
module.exports = class getEmployeesService {
	constructor(associateRepository,kindRepository) {
		this.associateRepository = associateRepository
		this.kindRepository = kindRepository		
	}

	async getEmployees(options, isAffiliate = false) {
		const includes = ['Address', 'Status']
		const kindname = isAffiliate ? 'affiliate' : 'employee' 
		const kind = await this.kindRepository.getOne({conditions:{name:kindname},})				
		const employees = await this.associateRepository.getAll({
			...options,
			kindFk: kind.kindId,
			include: isAffiliate ? includes.concat(['FileTemplates']) : includes,
		})
		return {
			...employees,
			rows: employees.rows.map((employee) => {
				const emp = employee.dataValues
				if (isAffiliate) {
					emp.templateId = employee.FileTemplates?.length
						? employee.FileTemplates[0].fileTemplateId
						: undefined
					delete emp.FileTemplates
				}
				return emp
			}),
		}
	}
	async getEmployee(id) {
		return await this.associateRepository.getById(id, {
			include: ['Address', 'Status'],
		})
	}	
}
