const { createController } = require('awilix-router-core')
const NotFoundError = require('../../common/controllers/error-handling/notFoundError')
const registerControllers = (paymentMethodService) => ({
	createPaymentMethod: async (req, res, next) => {
		try {
			const result = await paymentMethodService.create(req.body)
			res.send(result)
		} catch (error) {
			next(error)
		}
	},
	getPaymentMethods: async (req, res, next) => {
		try {
			const result = await paymentMethodService.get(req.query)
			next(result)
		} catch (error) {
			next(error)
		}
	},
	updatePaymentMethod: async (req, res, next) => {
		try {
			const paramId = req.params.id
			delete req.body.paymentMethodId
			const result = await paymentMethodService.update({
				...req.body,
				paymentMethodId: paramId,
			})
			res.send(result)
		} catch (error) {
			next(error)
		}
	},
	removePaymentMethod: async (req, res, next) => {
		try {
			const id = req.params.id
			const result = await paymentMethodService.delete(id)
			if (result) {
				res.send(`Payment Method ${id} succesfully deleted`)
			} else {
				throw new NotFoundError(`Payment Method [${id}]`)
			}
		} catch (error) {
			next(error)
		}
	},
})

module.exports = createController(registerControllers)
	.prefix('/payment-methods')
	.post('', 'createPaymentMethod')
	.get('', 'getPaymentMethods')
	.put('/:id', 'updatePaymentMethod')
	.delete('/:id', 'removePaymentMethod')
