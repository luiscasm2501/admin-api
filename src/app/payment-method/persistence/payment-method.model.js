const { DataTypes } = require('sequelize')

module.exports = function makeModel(apiDb) {
	const PaymentMethod = apiDb.define(
		'PaymentMethod',
		{
			name: {
				type: DataTypes.STRING(100),
				allowNull: false,
			},
			description: {
				type: DataTypes.STRING(500),
			},
			reference: {
				type: DataTypes.JSON,
				allowNull: false,
			},
			paymentMethodId: {
				type: DataTypes.INTEGER,
				primaryKey: true,
				autoIncrement: true,
			},
		},
		{ indexes: [{ unique: true, fields: ['name'] }] }
	)
	return PaymentMethod
}
