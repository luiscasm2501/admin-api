const Repository = require('../../common/persistence/sequilize/sequelizeRepo')
const statuses = require('../../common/persistence/status/statuses')
class PaymentMethodRepository extends Repository {
	constructor(apiDb) {
		super(apiDb.models.PaymentMethod, apiDb, statuses.ACTIVE)
	}
}

module.exports = PaymentMethodRepository
