class PaymentMethodService {
	constructor(paymentMethodRepository) {
		this.repo = paymentMethodRepository
	}
	filterReference(paymentMethod) {
		let filteredReference = undefined
		if (paymentMethod.reference) {
			filteredReference = {}
			Object.entries(paymentMethod.reference).forEach(([key, value]) => {
				if (key.length && value.length) {
					filteredReference[key] = value
				}
			})
		}
		return {
			paymentMethodId: paymentMethod.paymentMethodId,
			name: paymentMethod.name,
			description: paymentMethod.description,
			statusFk: paymentMethod.statusFk,
			...(filteredReference && { reference: filteredReference }),
		}
	}
	async create(paymentMethod) {
		return await this.repo.create(this.filterReference(paymentMethod))
	}

	async get(options = {}) {
		return await this.repo.getAll({ ...options, include: ['Status'] })
	}

	async update(paymentMethod) {
		return await this.repo.update(this.filterReference(paymentMethod))
	}

	async delete(id) {
		return await this.repo.remove(id)
	}
}
module.exports = PaymentMethodService
