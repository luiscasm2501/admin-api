const { DateTime, Interval } = require('luxon')
const parsePaymentSchedule = require('../../templates/contract-template/domain/parsePaymentSchedule')
module.exports = function mapContractDownload(hiring) {
	if (!hiring) {
		return {}
	}

	const employee = hiring.Employee
	const startDate = DateTime.fromISO(hiring.startDate)
	const expirationDate = DateTime.fromISO(hiring.expirationDate)
	const interval = Interval.fromDateTimes(startDate, expirationDate)
	const currentDate = DateTime.now()
	const duration =
    ((interval.length(
    	parsePaymentSchedule(hiring.ContractTemplate.scheduleType)
    ) /
      hiring.ContractTemplate.paymentSchedule) *
      100.0) /
    100.0
	return {
		...hiring.ContractTemplate.dataValues,
		name: `${employee.firstName} ${employee.secondName || ''} ${
			employee.lastName
		} ${employee.middleName || ''}`,
		contractName: hiring.ContractTemplate.name,
		legalIdentifier: employee.legalIdentifier,
		startDate: startDate.toLocaleString(DateTime.DATE_SHORT),
		expirationDate: expirationDate.toLocaleString(DateTime.DATE_SHORT),
		totalPayment: hiring.totalPayment,
		partialPayment: hiring.totalPayment / duration,
		position: hiring.position,
		mode: parseMode(hiring.mode),
		duration,
		workingDay: parseHiringDay(hiring.workingDay),
		rawDuration: duration * hiring.ContractTemplate.paymentSchedule,
		scheduleType: parsePaymentSchedule(
			hiring.ContractTemplate.scheduleType,
			'ES'
		),
		...currentDate.toObject({}),
		month: currentDate.monthLong,
		currentDate: currentDate.toLocaleString(DateTime.DATE_SHORT),
	}
}

function parseMode(mode) {
	switch (mode.toLowerCase()) {
	case 'presencial':
		return 'presencial, en las sedes de Cacao'
	case 'semi-presencial':
		return 'semi-presencial, dependiendo del contexto y la situación del CONTRATADO'
	default:
		return 'remota, exclusivamente a través de internet'
	}
}

function parseHiringDay(hD) {
	switch (hD.toLowerCase()) {
	case 'FULL-TIME':
		return 'tiempo completo'
	case 'MIXED':
		return 'tiempo a convenir'
	default:
		return 'medio tiempo'
	}
}
