const statuses = require('../../common/persistence/status/statuses')
const hiringEntity = require('../domain/hiringEntity')

module.exports = class UpdateHiringService {
	constructor(
		hiringRepository,
		templateRepository,
		createAutomaticInvoicesService
	) {
		this.hiringRepository = hiringRepository
		this.createAutomaticInvoicesService = createAutomaticInvoicesService
		this.templateRepository = templateRepository
	}

	async updateHiring(hiring) {
		hiringEntity(hiring, true)
		const statusName = hiring.statusName?.toUpperCase()
		if (statusName && statuses[statusName]) {
			await this.hiringRepository.updateStatus(hiring.hiringId, statusName)
		}

		const updatedHiring = await this.hiringRepository.update(hiring)
		if (statusName == statuses.APPROVED && updatedHiring.automaticInvoice) {
			const template = await this.templateRepository.getById(
				updatedHiring.contractTemplateFk
			)
			await this.createAutomaticInvoicesService.createAutomaticInvoices(
				updatedHiring,
				template,
				hiring.deadlineDays
			)
		}
		return updatedHiring
	}
}
