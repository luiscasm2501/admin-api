const { DateTime } = require('luxon')
const { sendEmail } = require('../../common/services/mailer/mailer')
const parseScheduleType = require('../../templates/contract-template/domain/parsePaymentSchedule')
const mapHiringDownload = require('../domain/mapHiringDownload')
const path = require('path')
const getImg = require('../../common/services/store/getImg')
module.exports = class sendHiringReceiptService {
	constructor(
		getHiringsService,
		generateFileService,
		getFileReceiptMetaService
	) {
		this.getHiringsService = getHiringsService
		this.generateFileService = generateFileService
		this.getFileReceiptMetaService = getFileReceiptMetaService
	}
	async sendHiringReceipt(receipt) {
		const hiring = await this.getHiringsService.getHiring(
			receipt.HiringInvoices.hiringFk
		)
		const startDate = DateTime.fromSQL(receipt.Invoice.startDate)
		const receiptMeta = await this.getFileReceiptMetaService.getFileReceiptMeta(
			receipt.receiptId
		)
		const receiptUrl = await getImg(receiptMeta.url, receipt.token)
		const paymentInfo = {
			...mapHiringDownload(hiring),
			amount: receipt.amount,
			concept: receipt.concept,
			paymentMethod: receipt.paymentMethod,
			paymentDate: receipt.paymentDate,
			url: receiptUrl,
			employee: hiring.Employee,
			startDate: startDate.toLocaleString(DateTime.DATE_SHORT),
			endDate: startDate
				.plus({
					[`${parseScheduleType(hiring.ContractTemplate.scheduleType)}`]:
            hiring.ContractTemplate.paymentSchedule,
				})
				.toLocaleString(DateTime.DATE_SHORT),
		}
		const filename = `voucher_${hiring.Employee.firstName}_${hiring.Employee.lastName}_${paymentInfo.startDate}`
		const templatePath = path.join(__dirname, '../static/voucher.handlebars')
		const voucherFile = await this.generateFileService.generateFile(
			templatePath,
			paymentInfo,
			filename
		)
		sendEmail(
			hiring.Employee.email,
			`Salary from Cacao for ${paymentInfo.startDate} / Salario de Cacao por ${paymentInfo.startDate}`,
			'hiring_receipt',
			paymentInfo,
			[{ filename: filename + '.pdf', path: voucherFile.pdf.url }]
		)
	}
}
