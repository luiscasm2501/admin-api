module.exports = class getHiringsService {
	constructor(hiringRepository) {
		this.hiringRepository = hiringRepository
	}

	async getHirings(options) {
		return await this.hiringRepository.getAll({
			include: ['Status', 'Employee', 'ContractTemplate'],
			...options,
		})
	}
	async getHiring(hiringId, options = {}) {
		return await this.hiringRepository.getById(hiringId, {
			include: ['Status', 'Employee', 'ContractTemplate'],
			...options,
		})
	}
	async getHiringsByEmployee(employeeId, options) {
		return await this.hiringRepository.getAll({
			employeeFk: employeeId,
			include: ['Status', 'Employee', 'ContractTemplate'],
			...options,
		})
	}
}
