const ValidationException = require('../../common/domain/validationException')
const statuses = require('../../common/persistence/status/statuses')

class deleteHiringService {
	constructor(hiringRepository) {
		this.hiringRepository = hiringRepository
	}

	async deleteHiring(hiringId) {
		await this.checkStatus(hiringId)

		return await this.hiringRepository.remove(hiringId)
	}

	async checkStatus(hiringId) {
		const status = await this.hiringRepository.getStatus(hiringId)
		if (
			![statuses.DRAFT, statuses.PENDING_REVIEW, statuses.DELETED].includes(
				status.name
			)
		) {
			throw new ValidationException(
				`${status.name} status. Only DRAFT contracts can be deleted`
			)
		}
		return status
	}
	async restoreHiring(id) {
		if (id) return await this.hiringRepository.restore(id)
	}
}
module.exports = deleteHiringService
