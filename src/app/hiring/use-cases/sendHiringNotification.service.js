const { DateTime } = require('luxon')
const statuses = require('../../common/persistence/status/statuses')
const { sendEmail } = require('../../common/services/mailer/mailer')
module.exports = class sendHiringNotificationService {
	constructor(
		getSettingsService,
		roleRepository,
		userRepository,
		invoiceRepository
	) {
		this.getSettingsService = getSettingsService
		this.roleRepository = roleRepository
		this.userRepository = userRepository
		this.invoiceRepository = invoiceRepository
		this.repo = undefined
	}

	async sendHiringNotification() {
		const settings = await this.getSettingsService.getSetting(
			'hiring-notifications'
		)
		const today = DateTime.now()
		const priorDate = today.plus({ days: settings.advance_days })
		let users = {}
		const pendingResult = await this.invoiceRepository.getAll({
			startDate: {
				lte: priorDate.toSQLDate(),
				gte: today.toSQLDate(),
			},
			sort: 'startDate',
			include: [
				{
					model: 'HiringInvoices',
					required: true,
					include: [['Hiring', ['Employee']]],
				},
				{
					model: 'Status',
					required: true,
					conditions: { name: [statuses.DRAFT, statuses.SENT] },
				},
			],
		})
		if (pendingResult.count) {
			const paymentInfo = pendingResult.rows.map((invoice) => {
				const date = DateTime.fromSQL(invoice.startDate)
				invoice = invoice.dataValues
				const parsedInfo = {
					amount: invoice.amount.toFixed(2),
					date: `${date.day}/${date.month}`,
					overdue: today > date,
					employee: invoice.HiringInvoices[0].Hiring.Employee,
				}
				return parsedInfo
			})
			users = await this.getUsers(settings)
			if (users) {
				const userMails = users.map((user) => {
					return user.email
				})
				sendEmail(
					userMails,
					`Cacao's incoming payrolls of ${today.day}/${today.month} / Nóminas entrantes de Cacao de ${today.day}/${today.month}`,
					'payrolls',
					{
						payrolls: paymentInfo,
						...settings,
						title: 'Incoming payrolls / Nóminas entrantes',
						button: 'Check Hirings / Consultar Contrataciones',
						today: today.toFormat(DateTime.DATE_SHORT),
					}
				)
			}
		}
		return { users: users.length || 0, pending: pendingResult.count }
	}

	async getUsers(settings) {
		const type = settings.type
		this.repo = type == 'role' ? this.roleRepository : this.userRepository
		const status = {
			model: 'Status',
			required: true,
			conditions: { name: statuses.ACTIVE },
		}
		const request =
      type == 'role'
      	? {
      		roleId: settings.entities,
      		include: [
      			{
      				model: 'Users',
      				required: true,
      				include: [status],
      			},
      		],
      	}
      	: {
      		userId: settings.entities,
      		include: [status],
      	}
		const result = await this.repo.getAll(request)
		let users = result.rows
		if (type == 'role') {
			users = users
				.filter((role) => {
					return role.Users
				})
				.map((role) => {
					return role.Users
				})
				.flat(2)
		}
		return users
	}
}
