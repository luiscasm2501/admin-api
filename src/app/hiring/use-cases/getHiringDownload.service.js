const path = require('path')

const NotFoundError = require('../../common/controllers/error-handling/notFoundError')
const getImg = require('../../common/services/store/getImg')
const mapHiringDownload = require('../domain/mapHiringDownload')
module.exports = class getContractDownloadService {
	constructor(getHiringsService, getFileTemplatesService, signatureRepository) {
		this.getHiringsService = getHiringsService
		this.getFileTemplatesService = getFileTemplatesService
		this.signatureRepository = signatureRepository
	}
	async getHiringDownload(hiringId, authorization) {
		const hiring = await this.getHiringsService.getHiring(hiringId)
		const template = await this.getFileTemplatesService.getFileTemplate(
			hiring.ContractTemplate.fileTemplateFk
		)
		const signatures = await this.signatureRepository.getAll({
			conditions: { status: 'ACTIVE' },
		})
		if (!hiring) {
			throw new NotFoundError('Hiring')
		}
		if (!template) {
			throw new NotFoundError('Hiring template')
		}
		for (let [index, signature] of signatures.entries()) {
			signature.url = await getImg(signature.url, authorization)
			signatures[index] = signature
		}
		const mappedHiring = mapHiringDownload(hiring)
		const url = template.name
		const file_path = path.join(process.env.FILE_SRC + '/template', url)
		const result = { ...mappedHiring, template: file_path, signatures }
		return result
	}
}
