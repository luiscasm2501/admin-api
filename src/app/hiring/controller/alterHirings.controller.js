const { createController } = require('awilix-router-core')
const NotFoundError = require('../../common/controllers/error-handling/notFoundError')
const UpdateError = require('../../common/controllers/error-handling/updateError')

const registerControllers = (updateHiringService, deleteHiringService) => ({
	updateHiring: async (req, res, next) => {
		try {
			const paramId = req.params.hiringId
			const bodyId = req.body.hiringId
			if (paramId === bodyId) {
				const result = await updateHiringService.updateHiring(req.body)
				res.send(result)
			} else {
				next(new UpdateError(paramId, bodyId))
			}
		} catch (error) {
			next(error)
		}
	},
	removeHiring: async (req, res, next) => {
		try {
			const id = req.params.hiringId
			const result = await deleteHiringService.deleteHiring(id)
			if (result) {
				res.send(`Hiring ${id} succesfully marked as deleted`)
			} else {
				next(new NotFoundError(`Hiring ${id}`))
			}
		} catch (error) {
			next(error)
		}
	},
	restoreHiring: async (req, res, next) => {
		try {
			const id = req.params.hiringId
			const result = await deleteHiringService.restoreHiring(id)
			if (result) {
				res.send(`Hiring ${id} succesfully restored`)
			} else {
				next(new NotFoundError(`Hiring ${id}`))
			}
		} catch (error) {
			next(error)
		}
	},
})

module.exports = createController(registerControllers)
	.prefix('/hirings/:hiringId')
	.put('', 'updateHiring')
	.delete('', 'removeHiring')
	.patch('', 'restoreHiring')
