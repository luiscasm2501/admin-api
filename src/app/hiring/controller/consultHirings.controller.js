const { createController } = require('awilix-router-core')
const { DateTime } = require('luxon')
const path = require('path')
require('dotenv').config()

const consultControllers = (
	getHiringsService,
	getHiringDownloadService,
	generateThenZipService
) => ({
	getHirings: async (req, res, next) => {
		try {
			const result = await getHiringsService.getHirings(req.query)
			next(result)
		} catch (error) {
			next(error)
		}
	},
	getHiringById: async (req, res, next) => {
		try {
			const result = await getHiringsService.getHiring(req.params.hiringId)
			res.send(result)
		} catch (error) {
			next(error)
		}
	},
	downloadHiring: async (req, res, next) => {
		try {
			const { hiringId } = req.params
			const fileInfo = await getHiringDownloadService.getHiringDownload(
				hiringId,
				req.headers.authorization
			)
			const fileZip = await generateThenZipService.generateThenZip(
				fileInfo.template,
				fileInfo,
				`CONTRACT-${fileInfo.name}_${DateTime.local().toMillis()}`
			)

			res.send(fileZip)
		} catch (error) {
			next(error)
		}
	},
})

module.exports = createController(consultControllers)
	.prefix('/hirings')
	.get('', 'getHirings')
	.get('/:hiringId', 'getHiringById')
	.all('/:hiringId/download', 'downloadHiring')

/**
 * @swagger
 * /hirings:
 *  get:
 *    summary: get a list of hiring records
 *    tags: [Employees]
 *    parameters:
 *      - $ref: '#/components/parameters/page'
 *      - $ref: '#/components/parameters/size'
 *      - $ref: '#/components/parameters/sort'
 *    responses:
 *      200:
 *        description: list of hirings
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/Hiring'
 *
 * /hirings/{hiringId}/download:
 *  get:
 *    tags: [Employees]
 *    summary: download the hiring document with all data set
 *    parameters:
 *      - $ref: '#/components/parameters/hiringId'
 *    responses:
 *      200:
 *        description: hiring contract generated zip
 *        content:
 *          application/octet-stream:
 *            schema:
 *              type: string
 *              format: binary
 */
