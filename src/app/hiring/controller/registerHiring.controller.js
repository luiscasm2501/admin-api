const { createController } = require('awilix-router-core')

const registerControllers = (
	createHiringService,
	getHiringsService,
	updateHiringService,
	deleteHiringService
) => ({
	createHiring: async (req, res, next) => {
		try {
			const result = await createHiringService.createHiring(
				req.body,
				req.params.id
			)
			res.send(result)
		} catch (error) {
			next(error)
		}
	},
	getHirings: async (req, res, next) => {
		try {
			const result = await getHiringsService.getHiringsByEmployee(
				req.params.id,
				req.query
			)
			next(result)
		} catch (error) {
			next(error)
		}
	},
	updateHiring: async (req, res, next) => {
		try {
			const paramId = req.params.hiringId
			const bodyId = req.body.hiringId
			if (paramId === bodyId) {
				const result = await updateHiringService.updateHiring(req.body)
				res.send(result)
			} else {
				next(new UpdateError(paramId, bodyId))
			}
		} catch (error) {
			next(error)
		}
	},
	removeHiring: async (req, res, next) => {
		try {
			const id = req.params.hiringId
			const result = await deleteHiringService.deleteHiring(id)
			if (result) {
				res.send(`Hiring ${id} succesfully marked as deleted`)
			} else {
				next(new NotFoundError(`Hiring ${id}`))
			}
		} catch (error) {
			next(error)
		}
	},
	restoreHiring: async (req, res, next) => {
		try {
			const id = req.params.hiringId
			const result = await deleteHiringService.restoreHiring(id)
			if (result) {
				res.send(`Hiring ${id} succesfully restored`)
			} else {
				next(new NotFoundError(`Hiring ${id}`))
			}
		} catch (error) {
			next(error)
		}
	},
})

module.exports = createController(registerControllers)
	.prefix('/employees/:id/hirings')
	.post('', 'createHiring')
	.get('', 'getHirings')
	.put('/:hiringId', 'updateHiring')
	.delete('/:hiringId', 'removeHiring')
	.patch('/:hiringId', 'restoreHiring')

/**
 * @swagger
 * /employees/{employeeId}/hirings:
 *  post:
 *    summary: create a new hiring for an employee
 *    tags: [Employees]
 *    parameters:
 *      - $ref: '#/components/parameters/employeeId'
 *    requestBody:
 *      required: true
 *      content:
 *        application/json:
 *          schema:
 *            $ref: '#/components/schemas/Hiring'
 *    responses:
 *      200:
 *        description: created hiring record
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/Hiring'
 *  get:
 *    summary: get hirings by employee
 *    tags: [Employees]
 *    parameters:
 *      - $ref: '#/components/parameters/employeeId'
 *      - $ref: '#/components/parameters/page'
 *      - $ref: '#/components/parameters/sort'
 *    responses:
 *      200:
 *        description: hiring list of an employee
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/Hiring'
 *
 * /employees/{employeeId}/hirings{hiringId}:
 *  put:
 *    summary: update a hiring by id
 *    tags: [Employees]
 *    parameters:
 *      - $ref: '#/components/parameters/employeeId'
 *    requestBody:
 *      required: true
 *      content:
 *        application/json:
 *          schema:
 *            $ref: '#/components/schemas/Hiring'
 *    responses:
 *      200:
 *        description: updated hiring record
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/Hiring'
 */
