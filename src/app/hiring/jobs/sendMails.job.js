const cron = require('node-cron')
const logger = require('../../common/controllers/logger/logger')

function job(sendHiringNotificationService, expression) {
	expression = '0 0 * * 1,3'
	cron.schedule(expression, () => {
		try {
			sendHiringNotificationService.sendHiringNotification().then((result) => {
				logger.info(
					`sent pending job payment email [users:${result.users}, payments: ${result.pending}]`
				)
			})
		} catch (error) {
			logger.error(`notification service error [${error.message}]`)
		}
	})
}
const service = 'sendHiringNotificationService'
module.exports = { job, service }
