const dotenv = require('dotenv')
const getAllowedOrigins = require('./common/authentication/use-cases/getAllowedOrigins')

dotenv.config()
const ALLOWED_ORIGINS = getAllowedOrigins()
const freeRoutes = [
	'auth/access',
	'perms',
	'statuses',
	'api-status',
	'setup-user',
	'storage',
	'kinds',
	{ methods: ['GET'], route: 'categories' },
	'users/recover',
	'hcaptcha/validate',
	'portal-auth/access',
	'portal-user/recover',
	{
		regex: /^(.*?)(?:\/portal-user\/username\b)(?:\/[\w\-\_]+){1}$/,
		origins: ALLOWED_ORIGINS,
	},
	{
		regex: /^(.*?)(?:\/portal-user\/email\b)(?:\/[\w\-\@\.]+){1}$/,
		origins: ALLOWED_ORIGINS,
	},
	{ route: 'clients', methods: ['POST'], origins: ALLOWED_ORIGINS },
	{
		route: 'notifications',
		methods: ['POST'],
		origins: process.env.DEPLOY_API_URL,
	},
]

class AppConfig {
	constructor() {
		this.environment = process.env.NODE_ENV
		this.port = process.env.PORT
		this.storage = process.env.FILE_SRC
		this.prefix = process.env.API_PREFIX
		this.webApp = process.env.WEB_APP_URL
		const syncMode = process.env.POSTGRES_DB_SYNC
		this.syncMode = syncMode ? { [syncMode.toLocaleLowerCase()]: true } : {}
		this.freeRoutes = freeRoutes.map((route) => {
			return {
				route: `${this.prefix}/${route.route || route}`,
				...(route.methods && { methods: route.methods }),
				...(route.regex && { regex: route.regex }),
			}
		})
	}
}

module.exports = AppConfig
