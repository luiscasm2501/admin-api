const { createController } = require('awilix-router-core')
const NotFoundError = require('../../common/controllers/error-handling/notFoundError')

const consultControllers = (getTicketService, getMessageService) => ({
	getTickets: async (req, res, next) => {
		try {
			const result = await getTicketService.getTickets(req.query)
			next(result)
		} catch (error) {
			next(error)
		}
	},
	getTicket: async (req, res, next) => {
		try {
			const id = req.params.id
			const result = await getTicketService.getTicket(id)
			if (result) res.send(result)
			else next(new NotFoundError(`ticket ${id}`))
		} catch (error) {
			next(error)
		}
	},
	getTicketMessages: async (req, res, next) => {
		try {
			const id = req.params.id
			const result = await getMessageService.getTicketMessages(id, req.query)
			if (result) next(result)
			else next(new NotFoundError(`ticket ${id}`))
		} catch (error) {
			next(error)
		}
	},
	getPendingTicketMessages: async (req, res, next) => {
		try {
			const id = req.params.id
			const result = await getMessageService.getPendingCount({
				ticketId: id,
				kind: req._client ? 'support' : 'client',
			})
			res.send(result)
		} catch (error) {
			next(error)
		}
	},
})

module.exports = createController(consultControllers)
	.prefix('/support-tickets')
	.get('', 'getTickets')
	.get('/:id', 'getTicket')
	.get('/:id/messages', 'getTicketMessages')
	.get('/:id/pending-messages', 'getPendingTicketMessages')

/**
 * @swagger
 *  /support-tickets:
 *    get:
 *      summary: Returns a list of ticket records
 *      tags: [Support Tickets]
 *      parameters:
 *        - $ref: '#/components/parameters/page'
 *        - $ref: '#/components/parameters/size'
 *        - $ref: '#/components/parameters/sort'
 *      responses:
 *        200:
 *          description: list of tickets
 *          content:
 *            application/json:
 *              schema:
 *                type: array
 *                $ref: '#/components/schemas/Ticket'
 * /support-tickets/{ticketId}:
 *  get:
 *    summary: get a ticket's information by id
 *    tags: [Support Tickets]
 *    parameters:
 *      - $ref: '#/components/parameters/ticketId'
 *    responses:
 *      200:
 *        description: ticket record
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/Ticket'
 *      404:
 *        $ref: '#/components/responses/notFound'
 * /support-tickets/{ticketId}/messages:
 *  get:
 *    summary: get a ticket related messages
 *    tags: [Support Tickets]
 *    parameters:
 *      - $ref: '#/components/parameters/ticketId'
 *    responses:
 *      200:
 *        description: ticket messages
 *        content:
 *          application/json:
 *            schema:
 *                type: array
 *                $ref: '#/components/schemas/Message'
 *      404:
 *        $ref: '#/components/responses/notFound'
 *
 */
