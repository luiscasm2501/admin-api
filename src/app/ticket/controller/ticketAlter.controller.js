const { createController } = require('awilix-router-core')
const NotFoundError = require('../../common/controllers/error-handling/notFoundError')
const UpdateError = require('../../common/controllers/error-handling/updateError')

const alterControllers = (updateTicketService, removeTicketService) => ({
	updateTicket: async (req, res, next) => {
		try {
			const paramId = req.params.id
			const bodyId = req.body.ticketId
			if (paramId === bodyId) {
				const result = await updateTicketService.updateTicket(req.body)
				res.send(result)
			} else {
				next(new UpdateError(paramId, bodyId))
			}
		} catch (error) {
			next(error)
		}
	},
	removeTicket: async (req, res, next) => {
		try {
			const id = req.params.id
			const result = await removeTicketService.removeTicket(id)
			if (result) {
				res.send(`Ticket ${id} succesfully marked as deleted`)
			} else {
				next(new NotFoundError(`Ticket ${id} not found`))
			}
		} catch (error) {
			next(error)
		}
	},
	restoreTicket: async (req, res, next) => {
		try {
			const id = req.params.id
			const result = await removeTicketService.restoreTicket(id)
			if (result) {
				res.send(`Ticket ${id} succesfully restored`)
			} else {
				next(new NotFoundError(`Ticket ${id} not found`))
			}
		} catch (error) {
			next(error)
		}
	},
})

module.exports = createController(alterControllers)
	.prefix('/support-tickets/:id')
	.put('', 'updateTicket')
	.delete('', 'removeTicket')
	.patch('', 'restoreTicket')

/**
 * @swagger
 * /support-tickets/{ticketId}:
 *  put:
 *    tags: [Support Tickets]
 *    summary: update a ticket's information by id
 *    parameters:
 *      - $ref: '#/components/parameters/ticketId'
 *    requestBody:
 *      required: true
 *      content:
 *        application/json:
 *          schema:
 *            $ref: '#/components/schemas/Ticket'
 *    responses:
 *      200:
 *        description: updated ticket information
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/Ticket'
 *  delete:
 *    tags: [Support Tickets]
 *    summary: delete a ticket record by id
 *    description: on the first go, it marks it a deleted, if used again, deletes it permanently
 *    parameters:
 *      - $ref: '#/components/parameters/ticketId'
 *    responses:
 *      200:
 *        description: ticket successfully deleted / marked as "deleted"
 *  patch:
 *    tags: [Support Tickets]
 *    summary: restores a ticket record marked as "deleted"
 *    parameters:
 *      - $ref: '#/components/parameters/ticketId'
 *    responses:
 *      200:
 *        description: ticket successfully restored
 */