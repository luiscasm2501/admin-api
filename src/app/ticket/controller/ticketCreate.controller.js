const { createController } = require('awilix-router-core')
const upload = require('../../files/config/files.config')

const registerControllers = (createTicketService) => ({
	createTicket: async (req, res, next) => {
		try {
			const result = await createTicketService.createTicket(
				{ ...req.body, ...(req._client && { clientFk: req._client }) },
				req.files
			)
			next(result)
		} catch (error) {
			next(error)
		}
	},
})

module.exports = createController(registerControllers)
	.prefix('/support-tickets')
	.post('', 'createTicket')
	.before(upload.array('messages'))

/**
 * @swagger
 * /support-tickets:
 *  post:
 *    tags: [Support Tickets]
 *    summary: create a new ticket record
 *    requestBody:
 *      required: true
 *      content:
 *        application/json:
 *          schema:
 *            $ref: '#/components/schemas/CreateTicket'
 *    responses:
 *      200:
 *        description: created ticket record
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/Ticket'
 */
