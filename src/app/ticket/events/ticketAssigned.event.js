const trycatchevent = require('../../common/events/trycatchevent')
const NotFoundError = require('../../common/controllers/error-handling/notFoundError')
function ticketAssignedEvent(bus, container) {
	const event = 'ticketAssigned'
	const ctx = 'sendTicketEmailService'
	const email = container.resolve(ctx)
	const userRepo = container.resolve('userRepository')	
	bus.register(event, (ticket) => {
		trycatchevent(async () => {
			const user = await 	userRepo.getById(ticket.supportFk)
			if(user){
				email.sendTicketAssigned(user)					
			}
			else{
				throw new NotFoundError('user')
			}				
			
		})
	})
}
module.exports = ticketAssignedEvent