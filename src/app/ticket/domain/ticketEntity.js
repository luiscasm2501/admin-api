const betweenValidator = require('../../common/domain/betweenValidator')
const validateId = require('../../common/domain/idValidator')

module.exports = function userEntity(
	{
		subject,
		description,
		priority,
		ticketId,
		statusFk,
		supportFk,
		clientFk,
		categoryFk,
	},
	update = false
) {
	(update && !subject) ||
    betweenValidator.stringBetween(subject, 2, 100, 'subject')
	;(update && !description) ||
    betweenValidator.stringBetween(description, 2, 1000, 'description')
	update && validateId(ticketId)
	;(update && !clientFk) || validateId(clientFk, 'client ID')

	return Object.freeze({
		subject,
		description,
		priority,
		ticketId,
		statusFk,
		supportFk,
		clientFk,
		categoryFk,
	})
}
