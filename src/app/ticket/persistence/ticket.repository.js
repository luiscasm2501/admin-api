const SequelizeRepo = require('../../common/persistence/sequilize/sequelizeRepo')
const statuses = require('../../common/persistence/status/statuses')

class ticketRepository extends SequelizeRepo {
	constructor(apiDb) {
		super(apiDb.models.Ticket, apiDb, statuses.OPEN)
	}
}

module.exports = ticketRepository