const { DataTypes } = require('sequelize')

module.exports = function makeModel(apiDb) {
	const Ticket = apiDb.define(
		'Ticket',
		{
			subject: {
				type: DataTypes.STRING(100),
				allowNull: false,
			},
			description: {
				type: DataTypes.STRING(1000),
				allowNull: false,
			},
			priority: {
				type: DataTypes.STRING(50),
				allowNull: true,
				validate: {
					isIn: [['High', 'Medium', 'Low']],
				},
			},
			ticketId: {
				type: DataTypes.UUID,
				allowNull: false,
				primaryKey: true,
				defaultValue: DataTypes.UUIDV4,
			},
		},
		{ paranoid: true }
	)
	return Ticket
}
