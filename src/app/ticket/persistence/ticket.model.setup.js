module.exports = function setupModel(db) {
	const { User, Client, Category, Status, Ticket } = db.models

	Status.hasMany(Ticket, {
		foreignKey: 'statusFk',
	})

	Ticket.belongsTo(Status, {
		foreignKey: 'statusFk',
	})

	User.hasMany(Ticket, {
		foreignKey: 'supportFk',
	})

	Ticket.belongsTo(User, {
		foreignKey: 'supportFk',
	})

	Client.hasMany(Ticket, {
		foreignKey: 'clientFk',
	})

	Ticket.belongsTo(Client, {
		foreignKey: 'clientFk',
	})

	Category.hasMany(Ticket, {
		foreignKey: 'categoryFk',
	})

	Ticket.belongsTo(Category, {
		foreignKey: 'categoryFk',
	})
}
