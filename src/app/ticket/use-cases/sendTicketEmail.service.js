const createToken = require('../../common/authentication/use-cases/createToken')
const createUserPayload = require('../../common/authentication/use-cases/createUserPayload')
const ValidationException = require('../../common/domain/validationException')
const { sendEmail } = require('../../common/services/mailer/mailer')
require('dotenv').config()
module.exports = class sendTicketEmailService {
	async sendTicketAssigned(user) {
		if (user) {
			const url = `${process.env.WEB_APP_URL}/support`
			return await sendEmail(
				user.email,
				'New support ticket / Nuevo ticket de soporte',
				'ticket_assigned',
				{
					user,
					url,
					button: 'Go to support console / Ir a la consola de soporte',
					title: 'New ticket assigned / Nuevo ticket asignado',
				}
			)
		}
		throw new ValidationException('Couldn\'t find user')
	}
}
