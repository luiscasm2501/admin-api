const ticketEntity = require('../domain/ticketEntity')

class createTicketService {
	constructor(ticketRepository, createMessageService, bus, contractRepository) {
		this.ticketRepository = ticketRepository
		this.createMessageService = createMessageService
		this.contractRepository = contractRepository
		this.bus = bus
	}
	async createTicket(ticket, files) {
		ticketEntity(ticket)
		let registeredTicket = await this.ticketRepository.create(ticket)
		if (files && files.length > 0) {
			let message = {
				sent_time: new Date(),
				ticket_id: registeredTicket.ticketId,
				sender_id: registeredTicket.clientFk,
				type: 'client',
			}
			var sentMessage = await this.createMessageService.createMessage(
				files,
				message
			)
			if (sentMessage)
				return { ...registeredTicket.dataValues, Files_message: sentMessage }
		}
		/*if (registeredTicket) {			
			this.bus.emit('ticketRegistered', ticket)
		}*/
		return registeredTicket
	}
}

module.exports = createTicketService
