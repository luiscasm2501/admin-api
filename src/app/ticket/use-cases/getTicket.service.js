module.exports = class getTicketService {
	constructor(ticketRepository, messageRepository) {
		this.ticketRepository = ticketRepository
		this.messageRepository = messageRepository
	}

	async getTickets(options) {
		return await this.ticketRepository.getAll({
			include: [
				'Category',
				'Status',
				'User',

				{
					model: 'Client',
					include: [
						{
							model: 'UserClient',
							include: [
								{
									model: 'PortalUser',
									include: ['username', 'portalUserId'],
									exclude: ['password'],
								},
							],
						},
					],
				},
			],
			...options,
		})
	}

	async getTicket(id, options) {
		return await this.ticketRepository.getById(id, {
			include: [
				'Category',
				'Status',
				'User',

				{
					model: 'Client',
					include: [
						{
							model: 'UserClient',
							include: [
								{
									model: 'PortalUser',
									include: ['username', 'portalUserId'],
									exclude: ['password'],
								},
							],
						},
					],
				},
			],
			...options,
		})
	}

	async getClientTickets(clientId, options) {
		try {
			const result = await this.ticketRepository.getAll({
				include: [
					'Category',
					'Status',
					'User',
					{
						model: 'Client',
						required: true,
						conditions: { clientId: clientId },
					},
				],
				...options,
			})
			const tickets = result.rows
			for (let i in tickets) {
				let res = await this.messageRepository.getAll({
					conditions: {
						ticket_id: tickets[i].ticketId,
						read: false,
						type: 'support',
					},
				})
				tickets[i].dataValues['supportUnread'] = res.length
			}
			result.rows = tickets
			return result
		} catch (err) {
			console.log(err)
		}
	}
}
