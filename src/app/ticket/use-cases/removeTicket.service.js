module.exports = class RemoveTicketService {
	constructor(ticketRepository) {
		this.ticketRepository = ticketRepository
	}

	async removeTicket(id) {
		if (id) return await this.ticketRepository.remove(id)
	}
	async restoreTicket(id) {
		if (id) return await this.ticketRepository.restore(id)
	}
}
