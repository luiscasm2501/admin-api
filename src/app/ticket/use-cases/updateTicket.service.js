const ticketEntity = require('../domain/ticketEntity')
const statuses = require('../../common/persistence/status/statuses')
const { sendEmail } = require('../../common/services/mailer/mailer')
const { MAIL_TEMPLATES } = require('../../common/services/mailer/constant')

class updateTicketService {
	constructor(ticketRepository, statusRepository, bus) {
		this.ticketRepository = ticketRepository
		this.statusRepository = statusRepository
		this.bus = bus
	}

	async notifyTicketClosed(ticketId) {
		try {
			const ticket = await this.ticketRepository.getById(ticketId, {
				include: ['Client'],
			})

			const emailData = {
				title: 'Ticket solved / Ticket resuelto',
				name: ticket.Client.name,
				ticket: ticketId,
				subject: ticket.subject,
				img: 'success',
				button: 'Manage Tickets / Administrar tickets',
			}

			const template = MAIL_TEMPLATES.TICKET_SOLVED
			await sendEmail(
				ticket.Client.email,
				template.subject(ticketId),
				template.template,
				emailData
			)
		} catch (error) {
			await this.ticketRepository.updateStatus(ticketId, statuses.IN_REVIEW)
			throw error
		}
	}

	async updateTicket(ticket) {
		ticketEntity(ticket, true)
		if (ticket.supportFk) {
			const newStatus = await this.statusRepository.getOne({
				conditions: { name: statuses.IN_REVIEW },
			})
			ticket['statusFk'] = newStatus.statusId
		}
		const updatedTicket = await this.ticketRepository.update(ticket)
		if (updatedTicket && ticket.supportFk) {
			this.bus.emit('ticketAssigned', ticket)
		}
		const closedStatus = await this.statusRepository.getOne({
			conditions: { name: statuses.CLOSED },
		})
		if (updatedTicket && closedStatus.statusId === ticket.statusFk) {
			await this.notifyTicketClosed(updatedTicket.ticketId)
		}
		return updatedTicket
	}
}

module.exports = updateTicketService
