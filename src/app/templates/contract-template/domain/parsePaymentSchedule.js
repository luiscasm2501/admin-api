function parseScheduleType(scheduleType, lang = 'EN') {
	switch (lang) {
	case 'ES':
		switch (scheduleType) {
		case 'D':
			return 'días'
		case 'M':
			return 'meses'
		case 'W':
			return 'semanas'
		case 'Y':
			return 'años'
		default:
			return 'meses'
		}
	default:
		switch (scheduleType) {
		case 'D':
			return 'days'
		case 'M':
			return 'months'
		case 'W':
			return 'weeks'
		case 'Y':
			return 'years'
		default:
			return 'months'
		}
	}
}
module.exports = parseScheduleType
