const { createController } = require('awilix-router-core')

const registerControllers = (createTemplateService) => ({
	createTemplate: async (req, res, next) => {
		try {
			const result = await createTemplateService.createTemplate(req.body)
			res.send(result)
		} catch (error) {
			next(error)
		}
	},
})

module.exports = createController(registerControllers)
	.prefix('/contract-templates')
	.post('', 'createTemplate')

/**
 * @swagger
 * /contract-templates:
 *  post:
 *    summary: create a new contract template
 *    tags: [ContractTemplates]
 *    requestBody:
 *      required: true
 *      content:
 *        application/json:
 *          schema:
 *            $ref: '#/components/schemas/ContractTemplate'
 *    responses:
 *      200:
 *        description: created contract template
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/ContractTemplate'
 */
