const { createController } = require('awilix-router-core')
const NotFoundError = require('../../../common/controllers/error-handling/notFoundError')
const UpdateError = require('../../../common/controllers/error-handling/updateError')

const alterControllers = (updateTemplateService, removeTemplateService) => ({
	updateTemplate: async (req, res, next) => {
		try {
			const paramId = req.params.id
			const bodyId = req.body.templateId
			if (paramId === bodyId) {
				const result = await updateTemplateService.updateTemplate(req.body)
				res.send(result)
			} else {
				next(new UpdateError(paramId, bodyId))
			}
		} catch (error) {
			next(error)
		}
	},
	removeTemplate: async (req, res, next) => {
		try {
			const id = req.params.id
			const result = await removeTemplateService.removeTemplate(id)
			if (result) {
				res.send(`Template ${id} succesfully deleted`)
			} else {
				next(new NotFoundError(`Contract template ${id}`))
			}
		} catch (error) {
			next(error)
		}
	},
})

module.exports = createController(alterControllers)
	.prefix('/contract-templates/:id')
	.put('', 'updateTemplate')
	.delete('', 'removeTemplate')

/**
 * @swagger:
 * /contract-templates/{id}:
 *  put:
 *    tags: [ContractTemplates]
 *    summary: update a contract template by identifier
 *    requestBody:
 *      required: true
 *      content:
 *        application/json:
 *          schema:
 *            $ref: '#/components/schemas/ContractTemplate'
 *    responses:
 *      200:
 *        description: updated contract template
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/ContractTemplate'
 *  delete:
 *    tags: [ContractTemplates]
 *    summary: remove a contract template by identifier
 *    parameters:
 *      - $ref: '#/components/parameters/id'
 *    responses:
 *      200:
 *        description: contract template succesfully deleted
 */
