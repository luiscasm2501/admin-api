const { createController } = require('awilix-router-core')
const NotFoundError = require('../../../common/controllers/error-handling/notFoundError')

const consultControllers = (getTemplatesService) => ({
	getTemplates: async (req, res, next) => {
		try {
			const result = await getTemplatesService.getTemplates(req.query)
			next(result)
		} catch (error) {
			next(error)
		}
	},
	getTemplate: async (req, res, next) => {
		try {
			const id = req.params.id
			const result = await getTemplatesService.getTemplate(id)
			if (result) res.send(result)
			else next(NotFoundError(`contrac template ${id}`))
		} catch (error) {
			next(error)
		}
	},
})

module.exports = createController(consultControllers)
	.prefix('/contract-templates')
	.get('', 'getTemplates')

/**
 * @swagger
 * /contract-templates:
 *  get:
 *    tags: [ContractTemplates]
 *    summary: retrieve a list of contract templates
 *    parameters:
 *      - $ref: '#/components/parameters/page'
 *      - $ref: '#/components/parameters/size'
 *      - $ref: '#/components/parameters/sort'
 *    responses:
 *      200:
 *        description: list of contract templates
 *        content:
 *          application/json:
 *            type: array
 *            schema:
 *              $ref: '#/components/schemas/ContractTemplate'
 */
