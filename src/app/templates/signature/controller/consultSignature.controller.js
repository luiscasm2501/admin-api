const { createController } = require('awilix-router-core')

const consultControllers = (getSignaturesService) => ({
	getSignatures: async (req, res, next) => {
		try {
			const result = await getSignaturesService.getSignatures(req.query)
			next(result)
		} catch (error) {
			next(error)
		}
	},
})

module.exports = createController(consultControllers)
	.prefix('/signatures')
	.get('', 'getSignatures')

/**
 * @swagger
 * /signatures:
 *  get:
 *    summary: get a list of signatures
 *    tags: [Templates]
 *    parameters:
 *      - $ref: '#/components/parameters/page'
 *      - $ref: '#/components/parameters/size'
 *      - $ref: '#/components/parameters/sort'
 *    responses:
 *      200:
 *        description: list of signatures
 *        content:
 *          application/json:
 *            type: array
 *            schema:
 *              $ref: '#/components/schemas/Signature'
 */
