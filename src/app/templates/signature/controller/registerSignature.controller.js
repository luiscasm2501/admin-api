const { createController } = require('awilix-router-core')
const ValidationException = require('../../../common/domain/validationException')
const upload = require('../../../files/config/files.config')

const registerControllers = (registerSignatureService) => ({
	registerSignature: async (req, res, next) => {
		try {
			if (req.file) {
				const result = await registerSignatureService.registerSignature({
					...req.body,
					...req.file,
				})
				next(result)
			} else {
				throw new ValidationException('Signature image is required')
			}
		} catch (error) {
			next(error)
		}
	},
})

module.exports = createController(registerControllers)
	.prefix('/signatures')
	.post('', 'registerSignature')
	.before(upload.single('signature'))

/**
 * @swagger
 * /signatures:
 *  post:
 *    tags: [Templates]
 *    summary: register a new signature
 *    requestBody:
 *      required: true
 *      content:
 *        multipart/form-data:
 *          schema:
 *            $ref: '#/components/schemas/Signature'
 *    responses:
 *      200:
 *        description: created signature
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/Signature'
 */
