const { createController } = require('awilix-router-core')

const upload = require('../../../files/config/files.config')
const NotFoundError = require('../../../common/controllers/error-handling/notFoundError')
const alterControllers = (updateSignatureService, deleteSignatureService) => ({
	updateSignature: async (req, res, next) => {
		try {
			const result = await updateSignatureService.updateSignature({
				...req.body,
				...req.file,
			})
			res.send(result)
		} catch (error) {
			next(error)
		}
	},
	deleteSignature: async (req, res, next) => {
		try {
			const result = await deleteSignatureService.deleteSignature(req.params.id)
			if (result) {
				res.send('Signature deleted')
			} else {
				throw new NotFoundError('Signature')
			}
		} catch (error) {
			next(error)
		}
	},
})

module.exports = createController(alterControllers)
	.prefix('/signatures/:id')
	.delete('', 'deleteSignature')
	.put('', 'updateSignature')
	.before(upload.single('signature'))

/**
 * @swagger
 * /signatures/{signatureId}:
 *  put:
 *    tags: [Templates]
 *    summary: updates a signature's information
 *    requestBody:
 *      required: true
 *      content:
 *        multipart/form-data:
 *          schema:
 *            $ref: '#/components/schemas/Signature'
 *    parameters:
 *      - $ref: '#/components/parameters/signatureId'
 *    responses:
 *      200:
 *        description: updated signature
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/Signature'
 *
 *  delete:
 *    summary: removes a signature permanently
 *    tags: [Templates]
 *    parameters:
 *      - $ref: '#/components/parameters/signatureId'
 *    responses:
 *      200:
 *        description: signature removed succesfully
 */
