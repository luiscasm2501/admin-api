const mapFileData = require('../../../files/domain/mapFileData')

module.exports = class registerSignatureService {
	constructor(signatureRepository) {
		this.repo = signatureRepository
	}
	async registerSignature(file) {
		const parsedFile = mapFileData(file)
		return await this.repo.create({
			name: file.name,
			title: file.title,
			url: parsedFile.url,
		})
	}
}
