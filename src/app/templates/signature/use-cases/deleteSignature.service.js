module.exports = class deleteSignatureService {
	constructor(signatureRepository) {
		this.repo = signatureRepository
	}
	async deleteSignature(signatureId) {
		return await this.repo.remove(signatureId)
	}
}
