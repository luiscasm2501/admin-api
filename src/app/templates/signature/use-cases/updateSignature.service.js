const mapFileData = require('../../../files/domain/mapFileData')
const { unlink } = require('fs')
const path = require('path')
const logger = require('../../../common/controllers/logger/logger')
module.exports = class updateSignatureService {
	constructor(signatureRepository) {
		this.repo = signatureRepository
	}
	async updateSignature(file) {
		const fileMeta = await this.repo.getById(file.id)
		if (fileMeta) {
			const url = fileMeta.url.split('/storage/')[1]
			const file_path = path.join(process.env.FILE_SRC, url)
			const filename = url.split('signature/')[1]
			if (file.filename && file.filename !== filename) {
				unlink(file_path, (err) => {
					if (err) {
						logger.error(`Couldn't remove file [${err.message}]`)
					}
				})
			}
		}
		const parsedFile = file.filename ? mapFileData(file) : fileMeta
		return await this.repo.update({
			...file,
			...(parsedFile && { url: parsedFile.url }),
		})
	}
}
