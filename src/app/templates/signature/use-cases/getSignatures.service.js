module.exports = class getSignaturesService {
	constructor(signatureRepository) {
		this.repo = signatureRepository
	}
	async getSignatures(options = {}) {
		return await this.repo.getAll({
			settings: options,
			count: true,
		})
	}
}
