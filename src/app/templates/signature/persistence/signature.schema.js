function makeSchema(Schema) {
	const signatureSchema = new Schema({
		title: {
			type: String,
			required: true,
		},
		name: {
			type: String,
			required: true,
		},
		status: { type: String, default: 'ACTIVE' },
		url: {
			type: String,
			required: true,
		},
	})
	return {
		name: 'signatures',
		schema: signatureSchema,
	}
}
module.exports = makeSchema
