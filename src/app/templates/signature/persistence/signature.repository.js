const mongoRepo = require('../../../common/persistence/mongo/mongoRepo')

module.exports = class signatureRepository extends mongoRepo {
	constructor(nosqlDb) {
		super(nosqlDb, 'signatures')
	}
}
