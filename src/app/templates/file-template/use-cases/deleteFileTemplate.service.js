module.exports = class uploadTemplateService {
	constructor(fileTemplateRepository, bus) {
		this.fileTemplateRepository = fileTemplateRepository
		this.bus = bus
	}
	async deleteFileTemplate(templateId) {
		const preCount = await this.fileTemplateRepository.getById(templateId)
		const deleted = await this.fileTemplateRepository.remove(templateId)
		if (deleted && !preCount) {
			this.bus.emit('fileTemplateDeleted', templateId)
		}
		return deleted
	}
	async restoreFileTemplate(templateId) {
		return await this.fileTemplateRepository.restore(templateId)
	}
}
