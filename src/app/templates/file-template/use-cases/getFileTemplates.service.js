module.exports = class getFileTemplatesService {
	constructor(fileTemplateRepository, fileTemplateMetaRepository) {
		this.repo = fileTemplateRepository
		this.metaRepo = fileTemplateMetaRepository
	}
	async getFileTemplates(options) {
		const filetemplates = await this.repo.getAll({
			include: ['Status', 'Category'],
			...options,
		})
		const fileMeta = await this.metaRepo.getAll()

		const mappedFiles = filetemplates.rows.map((fileValue) => {
			const fileData =
        fileMeta.find((file) => file._id == fileValue.fileTemplateId) ||
        'unknown'
			return {
				...fileValue.dataValues,
				file: fileData.name,
				fileurl: fileData.url,
				filetype: fileData.type,
			}
		})
		return { rows: mappedFiles, count: filetemplates.count }
	}

	async getFileTemplate(id) {
		return await this.metaRepo.getById(id, {
			attr: ['url', 'name'],
		})
	}
}
