module.exports = class uploadTemplateService {
	constructor(fileTemplateRepository, bus) {
		this.fileTemplateRepository = fileTemplateRepository
		this.bus = bus
	}
	async updateFileTemplate(fileTemplate, updateFile) {
		const result = await this.fileTemplateRepository.update(fileTemplate)
		if (result && updateFile) {
			this.bus.emit('fileTemplateUpdated', {
				...fileTemplate,
				id: fileTemplate.fileTemplateId,
			})
		}
		return result
	}
}
