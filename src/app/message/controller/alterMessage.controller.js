const { createController } = require('awilix-router-core')
const NotFoundError = require('../../common/controllers/error-handling/notFoundError')

const alterPortalUserControllers = (updateMessagesService) => ({	
	markAsRead: async (req, res, next) => {		
		try {			
			const ticketId = req.params.ticketId
			const type = req.params.type			
			const result = await updateMessagesService.markAsRead(ticketId, type)			
			res.send(result)
		} catch (error) {
			next(error)
		}
	},
	
})
module.exports = createController(alterPortalUserControllers)
	.prefix('/messages')
	.put('/mark-as-read/:ticketId/:type', 'markAsRead')    	
/**
 * @swagger
 * /messages/mark-as-read/{ticketId}/{type}:
 *  put:
 *    tags: [Messages]
 *    summary: Marks a ticket's messages as read
 *    parameters:
 *      - $ref: '#/components/parameters/ticketId' 
 *      - $ref: '#/components/parameters/type' 
 *    responses:
 *      200:
 *        description: list of messages
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/Message'
 *              type: array
 *      404:
 *        $ref: '#/components/responses/notFound'
 * 
 */