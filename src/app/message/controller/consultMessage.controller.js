const { createController } = require('awilix-router-core')
const NotFoundError = require('../../common/controllers/error-handling/notFoundError')

const consultControllers = (getMessageService) => ({
	getAllMessages: async (req, res, next) => {
		try {
			const result = await getMessageService.getMessages(req.query)			
			next(result)
		} catch (error) {
			next(error)
		}
	},
	getMessage: async (req, res, next) => {
		try {
			const id = req.params.id
			const result = await getMessageService.getMessage(id)
			if (result) res.send(result)
			else next(new NotFoundError(`Message ${id}`))
		} catch (error) {
			next(error)
		}
	},
})

module.exports = createController(consultControllers)
	.prefix('/messages')
	.get('', 'getAllMessages')
	.get('/:id', 'getMessage')

/**
 * @swagger
 * /messages:
 *  get:
 *    tags: [Messages]
 *    summary: get a list of message records
 *    parameters:
 *      - $ref: '#/components/parameters/page'
 *      - $ref: '#/components/parameters/sort'
 *    responses:
 *      200:
 *        description: list of messages
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/Message'
 *              type: array
 * /messages/{messageId}:
 *  get:
 *    tags: [Messages]
 *    summary: get a message record by id
 *    parameters:
 *      - $ref: '#/components/parameters/messageId'
 *    responses:
 *      200:
 *        description: message record
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/Message'
 */
