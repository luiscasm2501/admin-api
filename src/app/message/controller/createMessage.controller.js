const { createController } = require('awilix-router-core')
const upload = require('../../files/config/files.config')

const registerControllers = (createMessageService) => ({
	createMessage: async (req, res, next) => {
		try {			
			const result = await createMessageService.createMessage(req.files,req.body)
			res.send(result)						
		} catch (error) {
			next(error)
		}
	},
})

module.exports = createController(registerControllers)
	.prefix('/messages')
	.post('', 'createMessage')
	.before(upload.array('messages',10))

/**
 * @swagger
 * /messages:
 *  post:
 *    tags: [Messages]
 *    summary: create a new message record
 *    requestBody:
 *      required: true
 *      content:
 *        application/json:
 *          schema:
 *            $ref: '#/components/schemas/createMessage'
 *    responses:
 *      200:
 *        description: created message record
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/Message'
 */
