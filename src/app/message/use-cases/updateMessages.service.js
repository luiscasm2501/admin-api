module.exports = class updateMessageService {
	constructor(messageRepository) {
		this.messageRepository = messageRepository
	}

	/*It need the ticket id and the 'type' of message to mark.
    For example a client reads the support messages so it passes 'support'
    to the endpoint*/
	async markAsRead(ticketId, type) {						
		const result = await this.messageRepository.getAll({						
			conditions:{
				ticket_id:ticketId, 
				read: false,
				type: type //this can be done easily but it was done this way to be more readable
			},			 
		})		        
		if(result){
			let messages = result
			for( let i in messages){ 
				messages[i].read = true
				await this.messageRepository.update(messages[i])
			}
			return `Marked ${messages.length} as read`
		}		
	}
}