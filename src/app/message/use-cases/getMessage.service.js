module.exports = class getMessageService {
	constructor(messageRepository) {
		this.messageRepository = messageRepository
	}

	async getTicketMessages(ticketId, options) {
		const messages = await this.messageRepository.getAll({
			conditions: { ticket_id: ticketId },
			settings: options,
			count: true,
		})
		messages.rows = messages.rows.reverse()
		return messages
	}

	async getMessages(options) {
		return await this.messageRepository.getAll({
			settings: options,
			count: true,
		})
	}

	async getMessage(id, options) {
		return await this.messageRepository.getById(id, {
			settings: options,
		})
	}

	async getPendingCount({ ticketId, kind }) {
		const messages = await this.messageRepository.getAll({
			conditions: { ticket_id: ticketId, read: false, type: kind },
			count: true,
		})

		return { count: messages.count }
	}
}
