const mapForFile = require('../../files/domain/mapForFile')

class createMessageService {
	constructor(messageRepository) {
		this.messageRepository = messageRepository		
	}
	async createMessage(files, message) {								
		message['read'] = false
		if(files && files.length > 0){
			message['files'] = []
			for(let j in files){			
				let mappedFile = mapForFile(files[j])
				message.files.push(mappedFile)
			}
		}  		 						
		return await this.messageRepository.create(message)
	}
}
module.exports = createMessageService