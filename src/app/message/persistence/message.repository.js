const mongoRepo = require('../../common/persistence/mongo/mongoRepo')

module.exports = class messageRepository extends mongoRepo {
	constructor(nosqlDb) {
		super(nosqlDb, 'messages')
	}
}