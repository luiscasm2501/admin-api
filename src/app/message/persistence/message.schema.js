function makeSchema(Schema) {
	const messageSchema = new Schema({
		body: {
			type: String,
			required: false,
		},	
		sent_time: {
			type: Date,
			required: true,
		},
		ticket_id:{
			type:String,
			required:true
		},
		sender_id:{
			type:String,
			required:true
		},
		type:{
			type:String,
			required:true,
			enum:['client', 'support'],
		},
		read:{
			type:Boolean,
			required:true
		},
		files:{
			type:[],
			required:false,			
		}
	})
	return {
		name: 'messages',
		schema: messageSchema,
	}
}
module.exports = makeSchema

