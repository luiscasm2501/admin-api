const { createController } = require('awilix-router-core')

const registerControllers = (createSupportTipService) => ({
	createSupportTip: async (req, res, next) => {
		try {			
			const result = await createSupportTipService.createSupportTip(req.body)
			res.send(result)						
		} catch (error) {
			next(error)
		}
	},
})

module.exports = createController(registerControllers)
	.prefix('/support-tips')
	.post('', 'createSupportTip')

/**
 * @swagger
 * /support-tips:
 *  post:
 *    tags: [Support Tips]
 *    summary: create a new support tip record
 *    requestBody:
 *      required: true
 *      content:
 *        application/json:
 *          schema:
 *            $ref: '#/components/schemas/createSupportTip'
 *    responses:
 *      200:
 *        description: created supportTip record
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/SupportTip'
 */
