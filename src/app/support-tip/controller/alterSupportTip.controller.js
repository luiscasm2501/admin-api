const { createController } = require('awilix-router-core')
const NotFoundError = require('../../common/controllers/error-handling/notFoundError')
const UpdateError = require('../../common/controllers/error-handling/updateError')

const alterControllers = (updateSupportTipService, removeSupportTipService) => ({
	updateSupportTip: async (req, res, next) => {
		try {
			const paramId = req.params.id
			const bodyId = req.body.supportTipId			
			if (paramId === bodyId) {
				const result = await updateSupportTipService.updateSupportTip(req.body)
				res.send(result)
			} else {
				next(new UpdateError(paramId, bodyId))
			}
		} catch (error) {
			next(error)
		}
	},
	removeSupportTip: async (req, res, next) => {
		try {
			const id = req.params.id
			const result = await removeSupportTipService.removeSupportTip(id)
			if (result) {
				res.send(`SupportTip ${id} succesfully marked as deleted`)
			} else {
				next(new NotFoundError(`SupportTip ${id} not found`))
			}
		} catch (error) {
			next(error)
		}
	},
	restoreSupportTip: async (req, res, next) => {
		try {
			const id = req.params.id
			const result = await removeSupportTipService.restoreSupportTip(id)
			if (result) {
				res.send(`Support tip ${id} succesfully restored`)
			} else {
				next(new NotFoundError(`Support tip ${id} not found`))
			}
		} catch (error) {
			next(error)
		}
	},
})

module.exports = createController(alterControllers)
	.prefix('/support-tips/:id')
	.put('', 'updateSupportTip')
	.delete('', 'removeSupportTip')
	.patch('', 'restoreSupportTip')

/**
 * @swagger
 * /support-tips/{supportTipId}:
 *  put:
 *    tags: [Support Tips]
 *    summary: update a support tip's information by id
 *    parameters:
 *      - $ref: '#/components/parameters/supportTipId'
 *    requestBody:
 *      required: true
 *      content:
 *        application/json:
 *          schema:
 *            $ref: '#/components/schemas/SupportTip'
 *    responses:
 *      200:
 *        description: updated support tip's information
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/SupportTip'
 *  delete:
 *    tags: [Support Tips]
 *    summary: delete a supportTip record by id
 *    description: on the first go, it marks it a deleted, if used again, deletes it permanently
 *    parameters:
 *      - $ref: '#/components/parameters/supportTipId'
 *    responses:
 *      200:
 *        description: Support tip successfully deleted / marked as "deleted"
 *  patch:
 *    tags: [Support Tips]
 *    summary: restores a support tip record marked as "deleted"
 *    parameters:
 *      - $ref: '#/components/parameters/supportTipId'
 *    responses:
 *      200:
 *        description: Support tip successfully restored
 */