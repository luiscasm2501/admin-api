const { createController } = require('awilix-router-core')
const NotFoundError = require('../../common/controllers/error-handling/notFoundError')

const consultControllers = (getSupportTipService) => ({
	getAllSupportTips: async (req, res, next) => {
		try {
			const result = await getSupportTipService.getSupportTips(req.query)			
			next(result)
		} catch (error) {
			next(error)
		}
	},
	getSupportTip: async (req, res, next) => {
		try {
			const id = req.params.id
			const result = await getSupportTipService.getSupportTip(id)
			if (result) res.send(result)
			else next(new NotFoundError(`SupportTip ${id}`))
		} catch (error) {
			next(error)
		}
	},
})

module.exports = createController(consultControllers)
	.prefix('/support-tips')
	.get('', 'getAllSupportTips')
	.get('/:id', 'getSupportTip')

/**
 * @swagger
 * /support-tips:
 *  get:
 *    tags: [Support Tips]
 *    summary: get a list of support tip records
 *    parameters:
 *      - $ref: '#/components/parameters/page'
 *      - $ref: '#/components/parameters/sort'
 *    responses:
 *      200:
 *        description: list of support tips
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/SupportTip'
 *              type: array
 * /support-tips/{supportTipId}:
 *  get:
 *    tags: [Support Tips]
 *    summary: get a support tip record by id
 *    parameters:
 *      - $ref: '#/components/parameters/supportTipId'
 *    responses:
 *      200:
 *        description: support tip record
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/SupportTip'
 */
