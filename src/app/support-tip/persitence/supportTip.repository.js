const SequelizeRepo = require('../../common/persistence/sequilize/sequelizeRepo')

module.exports = class supportTipRepository extends SequelizeRepo {
	constructor(apiDb) {
		super(apiDb.models.SupportTip, apiDb)
	}
}
