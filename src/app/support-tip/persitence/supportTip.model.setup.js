module.exports = function setupModel(db) {
	const { Category ,  SupportTip } = db.models  
	
	Category.hasMany(SupportTip,{
		foreignKey: 'categoryFk',
	})

	SupportTip.belongsTo(Category,{
		foreignKey: 'categoryFk',
	})
}
