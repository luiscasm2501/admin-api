const { DataTypes } = require('sequelize')

function makeModel(apiDb) {
	const SupportTip = apiDb.define(
		'SupportTip',
		{
			title: {
				type: DataTypes.STRING(100),
				allowNull: false,				
			},
			description: {
				type: DataTypes.STRING(1000),
				allowNull: false,
			},
			registerDate: {
				type: DataTypes.DATE(),
				allowNull: false,
			},
			supportTipId: {
				type: DataTypes.UUID,
				allowNull: false,
				primaryKey: true,
				defaultValue: DataTypes.UUIDV4,
			},
		},
		{ paranoid: true, createdAt: false }		
	)	
	return SupportTip
}

module.exports = makeModel
