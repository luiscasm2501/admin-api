class createSupportTipService {
	constructor(supportTipRepository) {
		this.supportTipRepository = supportTipRepository		
	}
	async createSupportTip(supportTip ) {							
		return await this.supportTipRepository.create(supportTip)
	}
}
module.exports = createSupportTipService