module.exports = class getSupportTipService {
	constructor(supportTipRepository) {
		this.supportTipRepository = supportTipRepository
	}

	async getSupportTips(options) {		
		return await this.supportTipRepository.getAll({
			include: ['Category'],						
			...options
		})
	}

	async getSupportTip(id, options) {
		return await this.supportTipRepository.getById(id, {
			include: ['Category'],						
			...options,
		})
	}
}