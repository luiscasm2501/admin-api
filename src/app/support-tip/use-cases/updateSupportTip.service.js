class updateSupportTipService {
	constructor(supportTipRepository) {
		this.supportTipRepository = supportTipRepository
	}
	async updateSupportTip(supportTip) {		
		return await this.supportTipRepository.update(supportTip)
	}
}

module.exports = updateSupportTipService
