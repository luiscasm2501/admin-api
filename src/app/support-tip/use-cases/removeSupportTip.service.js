module.exports = class RemoveSupportTipService {
	constructor(supportTipRepository) {
		this.supportTipRepository = supportTipRepository
	}

	async removeSupportTip(id) {
		if (id) return await this.supportTipRepository.remove(id)
	}
	async restoreSupportTip(id) {
		if (id) return await this.supportTipRepository.restore(id)
	}
}
