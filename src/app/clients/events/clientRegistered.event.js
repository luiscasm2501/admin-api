const trycatchevent = require('../../common/events/trycatchevent')

function clientRegisteredEvent(bus, container) {
	const event = 'clientRegistered'
	const ctx = 'sendPortalUserEmailService'
	const email = container.resolve(ctx)
	const register = container.resolve('registerPortalUserService')
	bus.register(event, (client) => {
		trycatchevent( async () => {			
			let verification = await register.verifyAndLink(client)				
			if(verification.affiliate && verification.client){								
				email.sendRegisterBoth(verification)
			}	
			else if(verification.client){				
				email.sendUserLinked(verification)
			}
			else if(!verification){				
				email.sendClientRegistration(client)	
			}						
		})
	})
}
module.exports = clientRegisteredEvent