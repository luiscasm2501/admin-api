const addressEntity = require('../../address/domain/addressEntity')
const clientEntity = require('../domain/clientEntity')
const createToken = require('../../common/authentication/use-cases/createToken')
const createRegisterPayload = require('../../common/authentication/use-cases/createRegisterPayload')
class registerClientService {
	constructor(clientRepository, bus) {
		this.clientRepository = clientRepository
		this.bus = bus
	}
	async registerClient(client) {
		clientEntity(client)
		if (client.Address) addressEntity(client.Address)
		let registeredClient = await this.clientRepository.create(client)
		if (registeredClient) {
			this.bus.emit('clientRegistered', registeredClient.dataValues)
		}
		const token = createToken(
			createRegisterPayload(registeredClient.clientId, 'client'),
			'48h'
		)
		return { ...registeredClient.dataValues, token }
	}
}

module.exports = registerClientService
