const { createController } = require('awilix-router-core')
const NotFoundError = require('../../common/controllers/error-handling/notFoundError')
const UpdateError = require('../../common/controllers/error-handling/updateError')

const alterControllers = (updateClientService, removeClientService) => ({
	updateClient: async (req, res, next) => {
		try {
			const paramId = req.params.id
			const bodyId = req.body.clientId
			if (paramId === bodyId) {
				const result = await updateClientService.updateClient(req.body)
				res.send(result)
			} else {
				next(new UpdateError(paramId, bodyId))
			}
		} catch (error) {
			next(error)
		}
	},
	removeClient: async (req, res, next) => {
		try {
			const id = req.params.id
			const result = await removeClientService.removeClient(id)
			if (result) {
				res.send(`Client ${id} succesfully marked as deleted`)
			} else {
				next(new NotFoundError(`Client ${id} not found`))
			}
		} catch (error) {
			next(error)
		}
	},
	restoreClient: async (req, res, next) => {
		try {
			const id = req.params.id
			const result = await removeClientService.restoreClient(id)
			if (result) {
				res.send(`Client ${id} succesfully restored`)
			} else {
				next(new NotFoundError(`Client ${id} not found`))
			}
		} catch (error) {
			next(error)
		}
	},
})

module.exports = createController(alterControllers)
	.prefix('/clients/:id')
	.put('', 'updateClient')
	.delete('', 'removeClient')
	.patch('', 'restoreClient')

/**
 * @swagger
 * /clients/{clientId}:
 *  put:
 *    tags: [Clients]
 *    summary: update a client's information by id
 *    parameters:
 *      - $ref: '#/components/parameters/clientId'
 *    requestBody:
 *      required: true
 *      content:
 *        application/json:
 *          schema:
 *            $ref: '#/components/schemas/Client'
 *    responses:
 *      200:
 *        description: updated client information
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/Client'
 *  delete:
 *    tags: [Clients]
 *    summary: delete a client record by id
 *    description: on the first go, it marks it a deleted, if used again, deletes it permanently
 *    parameters:
 *      - $ref: '#/components/parameters/clientId'
 *    responses:
 *      200:
 *        description: client successfully deleted / marked as "deleted"
 *  patch:
 *    tags: [Clients]
 *    summary: restores a client record marked as "deleted"
 *    parameters:
 *      - $ref: '#/components/parameters/clientId'
 *    responses:
 *      200:
 *        description: client successfully restored
 */