const { createController } = require('awilix-router-core')
const NotFoundError = require('../../common/controllers/error-handling/notFoundError')

const consultControllers = (getClientsService, getClientService, getTicketService) => ({
	getAllClients: async (req, res, next) => {
		try {
			const result = await getClientsService.getClients(req.query)
			next(result)
		} catch (error) {
			next(error)
		}
	},
	getClient: async (req, res, next) => {
		try {
			const id = req.params.id
			const result = await getClientService.getClient(id)
			if (result) res.send(result)
			else next(new NotFoundError(`Client ${id}`))
		} catch (error) {
			next(error)
		}
	},
	getClientTickets: async (req, res, next) => {
		try {
			const id = req.params.id
			const result = await getTicketService.getClientTickets(id,req.query)
			if (result) next(result)
			else next(NotFoundError(`tickets of client ${id}`))
		} catch (error) {
			next(error)
		}
	},
})

module.exports = createController(consultControllers)
	.prefix('/clients')
	.get('', 'getAllClients')
	.get('/:id', 'getClient')
	.get('/:id/support-tickets', 'getClientTickets')

/**
 * @swagger
 * /clients:
 *  get:
 *    tags: [Clients]
 *    summary: get a list of client records
 *    parameters:
 *      - $ref: '#/components/parameters/page'
 *      - $ref: '#/components/parameters/sort'
 *    responses:
 *      200:
 *        description: list of clients
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/Client'
 *              type: array
 * /clients/{clientId}:
 *  get:
 *    tags: [Clients]
 *    summary: get a client record by id
 *    parameters:
 *      - $ref: '#/components/parameters/clientId'
 *    responses:
 *      200:
 *        description: client record
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/Client'
 * 
 * /clients/{clientId}/support-tickets:
 *  get:
 *    summary: get all tickets from a client Id
 *    tags: [Clients]
 *    parameters:
 *      - $ref: '#/components/parameters/clientId'
 *    responses:
 *      200:
 *        description: ticket record
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/Ticket'
 *      404:
 *        $ref: '#/components/responses/notFound'
 */
