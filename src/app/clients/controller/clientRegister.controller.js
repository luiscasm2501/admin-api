const { createController } = require('awilix-router-core')

const registerControllers = (registerClientService) => ({
	registerClient: async (req, res, next) => {
		try {
			const result = await registerClientService.registerClient(req.body)

			res.send(result)
		} catch (error) {
			next(error)
		}
	},
})

module.exports = createController(registerControllers)
	.prefix('/clients')
	.post('', 'registerClient')

/**
 * @swagger
 * /clients:
 *  post:
 *    tags: [Clients]
 *    summary: create a new client record
 *    requestBody:
 *      required: true
 *      content:
 *        application/json:
 *          schema:
 *            $ref: '#/components/schemas/Client'
 *    responses:
 *      200:
 *        description: created client record
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/Client'
 */
