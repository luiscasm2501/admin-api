/**
 * @swagger
 * components:
 *  parameters:
 *    fileTemplateId:
 *      in: path
 *      required: true
 *      name: fileTemplateId
 *      schema:
 *        type: string
 *        format: uuid
 *  schemas:
 *    FileTemplate:
 *      type: object
 *      properties:
 *        name:
 *          type: string
 *          description: identifying title for the template
 *        description:
 *          type: string
 *          description: optional additional details about the template
 *          required: false
 *        template:
 *          type: string
 *          format: binary
 *          description: file of the template used to render contracts and hirings
 *        fileTemplateId:
 *          type: string
 *          format: uuid
 *          description: unique identifier
 *        categoryFk:
 *          type: string
 *          format: uuid
 *          description: identifier of a category record
 */
