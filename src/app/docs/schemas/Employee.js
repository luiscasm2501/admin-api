/**
 * @swagger
 * components:
 *  schemas:
 *    Address:
 *      type: object
 *      properties:
 *        primaryLine:
 *          type: string
 *        secondaryLine:
 *          type: string
 *          required: false
 *        city:
 *          type: string
 *        state:
 *          type: string
 *        country:
 *          type: string
 *        zipCode:
 *          type: string
 *      example:
 *        primaryLine: 711-2880 Nulla St.
 *        secondaryLine: Mankato Mississippi 96522
 *        city: Dallas
 *        state: Texas
 *        country: USA
 *        zipCode: 10200
 *
 *    Employee:
 *      type: object
 *      properties:
 *        legalIdentifier:
 *          type: string
 *        firstName:
 *          type: string
 *        secondName:
 *          type: string
 *          required: false
 *        lastName:
 *          type: string
 *        middleName:
 *          type: string
 *          required: false
 *        email:
 *          type: string
 *          format: email
 *        phone:
 *          type: string
 *          format: phone        
 *        employeeId:
 *          type: string
 *          format: uuid
 *        Address:
 *          $ref: '#/components/schemas/Address'
 */
