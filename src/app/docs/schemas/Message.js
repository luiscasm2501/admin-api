/**
 * @swagger
 * components:
 *  parameters:
 *    messageId:
 *      in: path
 *      required: true
 *      name: messageId
 *      schema:
 *        type: string
 *        format: uuid
 *    type:
 *      in: path
 *      required: true
 *      name: type
 *      schema:
 *        type: string 
 *  schemas:
 *    Message:
 *      type: object
 *      properties:
 *        body:
 *          type: string
 *        sent_time:
 *          type: string
 *          format: date-time          
 *        ticket_id:
 *          type: string
 *        sender_id:
 *          type: string
 *        type:
 *          type: object 
 *          description: clasification of message (CLIENT|SUPPORT)        
 *        read:
 *          type: boolean      
 *        files:
 *          type: array
 *          items:
 *            type: string
 *          required: false              
 *        _id:
 *          type: string
 *          format: uuid
 *          description: auto-generated id
 *    
 *    createMessage:
 *      type: object
 *      properties:
 *        body:
 *          type: string
 *        sent_time:
 *          type: string
 *          format: date-time          
 *        ticket_id:
 *          type: string
 *        sender_id:
 *          type: string
 *        type:
 *          type: string 
 *          description: clasification of message (CLIENT|SUPPORT)
 *        read:
 *          type: boolean        
 *        files:
 *          type: array
 *          items:
 *            type: string
 *          required: false            
 */