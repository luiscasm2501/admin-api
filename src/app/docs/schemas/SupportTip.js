/**
 * @swagger
 * components:
 *  parameters:
 *    supportTipId:
 *      in: path
 *      required: true
 *      name: supportTipId
 *      schema:
 *        type: string
 *        format: uuid
 *  schemas:
 *    SupportTip:
 *      type: object
 *      properties:
 *        title:
 *          type: string
 *        registerDate:
 *          type: string
 *          format: date-time          
 *        description:
 *          type: string
 *        categoryFk:
 *          type: string
 *          format: uuid
 *          description: auto-generated id      
 *        supportTipId:
 *          type: string
 *          format: uuid
 *          description: auto-generated id
 *    
 *    createSupportTip:
 *      type: object
 *      properties:
 *        title:
 *          type: string
 *        registerDate:
 *          type: string
 *          format: date-time          
 *        description:
 *          type: string
 *        categoryFk:
 *          type: string
 *          format: uuid
 *          description: auto-generated id              
 */