/**
 * @swagger
 *  
 * components:
 *  parameters:
 *    bulletinId:
 *      in: path
 *      required: true
 *      name: bulletinId
 *      schema:
 *        type: string
 *        format: uuid
 *  schemas:
 *    Bulletin:
 *      type: object
 *      properties:
 *        title:
 *          type: string
 *        description:
 *          type: string          
 *        date:
 *          type: string
 *          format: date-time                       
 *        kindFk:
 *          type: string
 *          format: uuid
 *          description: auto-generated id       
 */
