/**
 * @swagger
 * tags:
 *  - name: Products
 *    description: products and services that can be acquired by clients via contracts
 * components:
 *  parameters:
 *    productId:
 *      name: productId
 *      in: path
 *      required: true
 *      description: unique identifier of a product
 *  schemas:
 *    Product:
 *      type: object
 *      properties:
 *        name:
 *          type: string
 *          description: unique title to describe a product
 *        productId:
 *          type: string
 *          format: uuid
 */
