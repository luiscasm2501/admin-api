/**
 * @swagger
 * tags:
 *  - name: Employees
 *    description: internal staff hired by Cacao
 * components:
 *  parameters:
 *    hiringId:
 *      name: hiringId
 *      in: path
 *      required: true
 *      schema:
 *        type: string
 *        format: uuid
 *    employeeId:
 *      name: employeeId
 *      in: path
 *      required: true
 *      schema:
 *        type: string
 *        format: uuid
 *  schemas:
 *    Hiring:
 *      type: object
 *      properties:
 *        totalPayment:
 *          type: number
 *          description: total amount to be paid for the hiring
 *        position:
 *          type: string
 *          description: job position (CEO, CTO...)
 *        mode:
 *          type: string
 *          description: geographical type of the hiring (remote, presencial...)
 *        workingDay:
 *          type: string
 *          description: time characteristic of the hiring (full-time, half-time)
 *        startDate:
 *          type: string
 *          format: date
 *        expirationDate:
 *          type: string
 *          format: date
 *        hiringId:
 *          type: string
 *          format: uuid
 *      example:
 *        totalPayment: 480.99
 *        position: 'Software Engineer'
 *        mode: REMOTE
 *        workingDay: HALF-TIME
 *        startDate: 15-11-2021
 *        expirationDate: 15-02-2022
 *        hiringId: <uuid>
 *
 */
