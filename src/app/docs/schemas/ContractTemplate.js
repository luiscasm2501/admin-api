/**
 * @swagger
 * tags:
 *  - name: ContractTemplates
 *    description: allows to define reusable placeholders for client and employee's contracts
 * components:
 *  schemas:
 *    ContractTemplate:
 *      type: object
 *      properties:
 *        templateId:
 *          type: string
 *        name:
 *          type: string
 *        scheduleType:
 *          type: string
 *          description: kind of time period between payments (Y=years, M=months, W=weeks)
 *        paymentSchedule:
 *          type: integer
 *          description: how many periods are between payments (1 month, 12 weeks, etc)
 *        description:
 *          type: string
 *          required: false
 *        fileTemplateFk:
 *          type: string
 *          description: identifier to file template record
 *
 *      example:
 *         name: 2-month contracts
 *         description: payments every two months
 *         scheduleType: M
 *         paymentSchedule: 2
 *         templatedId: <uuid>
 *         fileTemplateFk: <uuid>
 */
