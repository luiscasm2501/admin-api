/**
 * @swagger
 * components:
 *  securitySchemes:
 *    BearerAuth:
 *      type: http
 *      description: authorization token obtained by login in the API
 *      in: header
 *      name: authorization
 *      scheme: bearer
 *  parameters:
 *    id:
 *      name: id
 *      in: path
 *      description: unique identifier of record
 *      schema:
 *        type: string
 *
 *    page:
 *      name: page
 *      description: sets current position to retrieve records from
 *      in: query
 *      schema:
 *        type: integer
 *    size:
 *      name: size
 *      in: query
 *      description: number of records to retrieve. If no page and size are provided, returns all records at once
 *      schema:
 *        type: integer
 *    sort:
 *      name: sort
 *      description: "specifies the order and the attributes used to sort the result. Accepts multiple attributes, delimited by ','"
 *      in: query
 *      schema:
 *        type: string
 *      examples:
 *        ascending:
 *          value: "attribute1[,attribute2,...]:a"
 *        descending:
 *          value: "attribute:d"
 *  responses:
 *    notFound:
 *      description: resource not found
 */
