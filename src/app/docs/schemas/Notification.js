/**
 * @swagger
 * components:
 *  schemas:
 *    Notification:
 *      type: object
 *      properties:
 *        title:
 *          type: string
 *        kind:
 *          type: string
 *          description: clasification of notification (INFO|WARNING|ERROR|REMINDER)
 *        created_time:
 *          type: string
 *          format: date-time
 *        content:
 *          type: object
 *          required: false
 *          description: any additional details of the notification
 *        _id:
 *          type: string
 *          format: uuid
 *          description: auto-generated id
 */
