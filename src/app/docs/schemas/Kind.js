/**
 * @swagger
 * components:
 *  parameters:
 *    kindId:
 *      in: path
 *      required: true
 *      name: kindId
 *      schema:
 *        type: string
 *        format: uuid
 *  schemas:
 *    Kind:
 *      type: object
 *      properties:
 *        name:
 *          type: string                    
 *        kindId:
 *          type: string
 *          format: uuid
 *          description: auto-generated id               
 */