/**
 * @swagger
 * tags:
 *  - name: Users
 *    description: Users of the API and Web Application
 *  - name: Auth
 *    description: Endpoints that are derived from having an user token
 * components:
 *  parameters:
 *    userId:
 *      name: userId
 *      in: path
 *      description: uuid of a certain user
 *      required: true
 *      schema:
 *        type: string
 *  headers:
 *    authorization:
 *      required: true
 *      name: authorization
 *      in: header
 *      description: header containing a valid JWT authorization token
 *      schema:
 *        type: string
 *        example: "Bearer <token>"
 *  schemas:
 *    User:
 *      type: object
 *      properties:
 *        userId:
 *          type: string
 *          description: auto-generated uuid of an user
 *        firstName:
 *          type: string
 *        lastName:
 *          type: string
 *        email:
 *          type: string
 *        password:
 *          type: string
 *      example:
 *        userId: (auto-generated uuid)
 *        firstName: John
 *        lastName: Johnson
 *        email: user@email.com
 *        password: Asd*xsXAS
 *
 */
