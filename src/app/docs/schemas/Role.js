/**
 * @swagger
 * tags:
 *  - name: Roles
 *    description: related to permissions and actions allowed to users
 * components:
 *  parameters:
 *    roleId:
 *      name: roleId
 *      in: path
 *      description: unique uuid of role
 *  schemas:
 *    Permission:
 *      type: object
 *      properties:
 *        name:
 *          type: string
 *          description: what entity or privilege does this permission represent
 *        type:
 *          type: string
 *          description: what action does this permission grant (C,R,U,D)
 *        permissionId:
 *          type:string
 *      example:
 *        name: Client
 *        type: U
 *        description: this permission allows to update a client
 *        permissionId: <uuid>
 *    Role:
 *      type: object
 *      properties:
 *        name:
 *          type: string
 *        description:
 *          type: string
 *        perms:
 *          type: array
 *          description: list of permissions related to this role
 *      example:
 *        name: reader role
 *        description: this role grants reading of many things
 *        perms: [<array of permission ids>]
 *
 */
