/**
 * @swagger
 *  
 * components:
 *  parameters:
 *    ticketId:
 *      in: path
 *      required: true
 *      name: ticketId
 *      schema:
 *        type: string
 *        format: uuid
 *  schemas:
 *    CreateTicket:
 *      type: object
 *      properties:
 *        subject:
 *          type: string
 *        description:
 *          type: string          
 *        registerDate:
 *          type: string
 *          format: date-time
 *        priority:
 *          type: string
 *          description: must be 'High','Medium' or 'Low'                       
 *        contractFk:
 *          type: string
 *          format: uuid
 *          description: auto-generated id
 *        categoryFk:
 *          type: string
 *          format: uuid
 *          description: auto-generated id  
 * 
 *    Ticket:
 *      type: object
 *      properties:
 *        ticketId:
 *          type: string
 *          format: uuid
 *          description: auto-generated id
 *        subject:
 *          type: string
 *        description:
 *          type: string          
 *        registerDate:
 *          type: string
 *          format: date-time
 *        priority:
 *          type: string
 *          description: must be 'High','Medium' or 'Low'                       
 *        contractFk:
 *          type: string
 *          format: uuid
 *          description: auto-generated id
 *        categoryFk:
 *          type: string
 *          format: uuid
 *          description: auto-generated id 
 *        supportFk:
 *          type: string
 *          format: uuid
 *          description: auto-generated id
 *        statusFk:
 *          type: string
 *          format: uuid
 *          description: auto-generated id     
 */
