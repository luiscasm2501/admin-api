/**
 * @swagger
 * components:
 *  schemas:
 *    PortalUserReqBody:
 *      type: object
 *      properties:
 *        email:
 *          type: string   
 *        username:
 *          type: string
 *        password:
 *          type: string                  
 */