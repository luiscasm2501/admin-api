/**
 * @swagger
 * tags:
 *  - name: Templates
 *    description: Endpoints to manage all use-cases regarding the files of hirings and contracts
 * components:
 *  parameters:
 *    signatureId:
 *      in: header
 *      name: signatureId
 *      schema:
 *        type: string
 *        example: <uuid>
 *  schemas:
 *    Signature:
 *      type: object
 *      properties:
 *        name:
 *          type: string
 *        title:
 *          type: string
 *        signature:
 *          type: string
 *          format: byte
 *        signatureId:
 *          type: string
 *      example:
 *        name: John Johnson
 *        title: CEO
 *        signature: signature_img.jpg
 *        signatureId: <auto-generated uuid>
 */
