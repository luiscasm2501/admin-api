/**
 * @swagger
 * tags:
 *  - name: Invoices
 *    description: represents pending payments from client contracts
 * components:
 *  parameters:
 *    invoiceId:
 *      name: invoiceId
 *      description: unique identifier of invoice
 *      required: true
 *      in: path
 *      schema:
 *        type: string
 *    receiptId:
 *      name: receiptId
 *      description: unique identifier of receipt
 *      required: true
 *      in: path
 *      schema:
 *        type: string
 *  schemas:
 *    Invoice:
 *      type: object
 *      properties:
 *        amount:
 *          type: number
 *          description: monetary quantity charged by this invoice
 *        startDate:
 *          type: string
 *          format: date
 *          description: when this invoice starts
 *        deadline:
 *          type: string
 *          format: date
 *          description: last date available to pay this invoice
 *        invoiceId:
 *          type: string
 *          format: uuid
 *          description: unique identifier of invoice
 *      example:
 *        amount: 400.99
 *        startDate: 12/08/2021
 *        deadline: 12/09/2021
 *        invoiceId: <invoice uuid>
 *    Receipt:
 *      type: object
 *      properties:
 *        amount:
 *          type: number
 *          description: monetary quantity charged by this invoice
 *        paymentDate:
 *          type: string
 *          format: date
 *          description: whenever the payment was made
 *        paymentMethod:
 *          type: string
 *          description: however this payment was made
 *        receipt:
 *          description: image file of receipt
 *          type: string
 *          format: binary
 *        concept:
 *          required: false
 *          type: string
 *          description: any additional details about this payment
 *        receiptId:
 *          type: string
 *          format: uuid
 *          description: unique identifier of receipt
 *      example:
 *        amount: 400.99
 *        paymentDate: 31/08/2021
 *        paymentMethod: online payment
 *        concept: payment for an invoice, confirmation number xXxxXXXxX
 *        receipt: receipt_image_file.jpg
 *        receiptId: <receipt uuid>
 */
