/**
 * @swagger
 * tags:
 *  - name: Clients
 *    description: clients are entities that acquire  products through contracts, get invoices sent, and register payments
 * components:
 *  parameters:
 *    clientId:
 *      in: path
 *      required: true
 *      name: clientId
 *      schema:
 *        type: string
 *        format: uuid
 *  schemas:
 *    Client:
 *      type: object
 *      properties:
 *        legalIdentifier:
 *          type: string
 *        name:
 *          type: string
 *        email:
 *          type: string
 *          format: email
 *        phone:
 *          type: string
 *          format: phone
 *        clientId:
 *          type: string
 *          format: uuid
 *        Address:
 *          $ref: '#/components/schemas/Address'
 */
