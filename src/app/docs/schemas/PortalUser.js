/**
 * @swagger
 * components:
 *  schemas:    
 *    PortalUser:
 *      type: object
 *      properties:
 *        username:
 *          type: string                            
 *        portalUserId:
 *          type: string
 *          format: uuid
 *          description: auto-generated id
 *        password:
 *          type: string                
 */
 