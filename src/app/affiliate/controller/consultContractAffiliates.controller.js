const { createController } = require('awilix-router-core')
const NotFoundError = require('../../common/controllers/error-handling/notFoundError')

const consultControllers = (
	getContractAffiliatesService,
	sendTermsService
) => ({
	getContractAffiliates: async (req, res, next) => {
		try {
			const result = await getContractAffiliatesService.getContractAffiliates(
				req.query
			)
			next(result)
		} catch (error) {
			next(error)
		}
	},
	getContractAffiliate: async (req, res, next) => {
		try {
			const id = req.params.id
			const result = await getContractAffiliatesService.getContractAffiliate(id)
			if (result) res.send(result)
			else next(new NotFoundError(`Affiliate contract ${id}`))
		} catch (error) {
			next(error)
		}
	},
	sendTerms: async (req, res, next) => {
		try {
			const id = req.params.id
			const result = await sendTermsService.sendTerms(id)
			if (result) res.send(result)
			else next(new NotFoundError(`Affiliate ${id}`))
		} catch (error) {
			next(error)
		}
	},
})

module.exports = createController(consultControllers)
	.prefix('/contract-affiliates')
	.get('', 'getContractAffiliates')
	.get('/:id', 'getContractAffiliate')
	.get('/:id/terms', 'sendTerms')
