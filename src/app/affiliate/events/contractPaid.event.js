const trycatchevent = require('../../common/events/trycatchevent')

function registerEvent(bus, container) {
	const event = 'receiptApproved'
	const ctx = 'checkAffiliatePaymentService'
	const context = container.resolve(ctx)
	bus.register(event, (receipt) => {
		trycatchevent(() => {
			context.checkAffiliatePayment(receipt)
		})
	})
}
module.exports = registerEvent
