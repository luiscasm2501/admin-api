const { DateTime } = require('luxon')
const { sendEmail } = require('../../common/services/mailer/mailer')
const path = require('path')
const getImg = require('../../common/services/store/getImg')
module.exports = class sendAffiliateReceiptService {
	constructor(
		getContractAffiliatesService,
		generateFileService,
		getFileReceiptMetaService
	) {
		this.getContractAffiliatesService = getContractAffiliatesService
		this.generateFileService = generateFileService
		this.getFileReceiptMetaService = getFileReceiptMetaService
	}
	async sendAffiliateReceipt(receipt) {
		const contractAffiliate =
      await this.getContractAffiliatesService.getContractAffiliate(
      	receipt.AffiliateInvoices.contractAffiliateFk
      )
		const startDate = DateTime.fromSQL(receipt.Invoice.startDate)
		const receiptMeta = await this.getFileReceiptMetaService.getFileReceiptMeta(
			receipt.receiptId
		)
		const receiptUrl = await getImg(receiptMeta.url, receipt.token)
		const paymentInfo = {
			amount: receipt.amount,
			concept: receipt.concept,
			name:
        contractAffiliate.Affiliate.firstName +
        ' ' +
        contractAffiliate.Affiliate.lastName,
			legalIdentifier: contractAffiliate.Affiliate.legalIdentifier,
			client: contractAffiliate.Contract.ClientProduct.Client.name,
			product: contractAffiliate.Contract.ClientProduct.Product.name,
			paymentMethod: receipt.paymentMethod,
			paymentDate: receipt.paymentDate,
			url: receiptUrl,
			employee: contractAffiliate.Affiliate,
			startDate: startDate.toLocaleString(DateTime.DATE_SHORT),
		}
		const filename = `voucher_${contractAffiliate.Affiliate.firstName}_${contractAffiliate.Affiliate.lastName}_${paymentInfo.startDate}`
		const templatePath = path.join(
			__dirname,
			'../static/voucher_aff.handlebars'
		)
		const voucherFile = await this.generateFileService.generateFile(
			templatePath,
			paymentInfo,
			filename
		)
		sendEmail(
			contractAffiliate.Affiliate.email,
			`Commission from Cacao for ${paymentInfo.startDate} / Comisión de Cacao por ${paymentInfo.startDate}`,
			'affiliate_receipt',
			paymentInfo,
			[{ filename: filename + '.pdf', path: voucherFile.pdf.url }]
		)
	}
}
