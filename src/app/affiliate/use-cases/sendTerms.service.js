const { sendEmail } = require('../../common/services/mailer/mailer')
require('dotenv').config()
module.exports = class sendTermsService {
	constructor(
		associateRepository,
		affiliateTemplateRepository,
		getFileTemplatesService,
		generateFileService
	) {
		this.associateRepository = associateRepository
		this.getFileTemplatesService = getFileTemplatesService
		this.affiliateTemplateRepository = affiliateTemplateRepository
		this.generateFileService = generateFileService
	}
	async sendTerms(affiliateId) {
		const affiliate = await this.associateRepository.getById(affiliateId)
		const affiliateTemplate = await this.affiliateTemplateRepository.getOne({
			conditions: { affiliateFk: affiliateId },
		})
		const template = await this.getFileTemplatesService.getFileTemplate(
			affiliateTemplate.fileTemplateFk
		)
		const filename = 'terms_conditions_' + affiliate.firstName
		const templateFile = await this.generateFileService.generateFile(
			process.env.FILE_SRC + '/template/' + template.name,
			affiliate,
			filename
		)
		if (affiliate && template) {
			await sendEmail(
				affiliate.email,
				'Cacao\'s Terms and conditions / Términos y condiciones de Cacao',
				'affiliate_terms',
				{ ...affiliate.dataValues },
				[{ filename: templateFile.pdf.filename, path: templateFile.pdf.url }]
			)
			return 'OK'
		}
	}
}
