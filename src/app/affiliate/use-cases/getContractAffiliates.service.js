module.exports = class getContractAffiliatesService {
	constructor(contractAffiliateRepository) {
		this.contractAffiliateRepository = contractAffiliateRepository
	}
	async getContractAffiliates(options) {
		const res = await this.contractAffiliateRepository.getAll({
			...options,
			include: [
				'Employee',
				'Status',
				{
					model: 'Contract',
					include: [{ model: 'ClientProduct', include: ['Client', 'Product'] }],
				},
			],
		})
		return {rows : this.mapAffiliate(res.rows), count : res.count} 
	}
	async getContractAffiliate(contractAffiliateId, options = {}) {
		const contractAffiliate = await this.contractAffiliateRepository.getById(
			contractAffiliateId,
			{
				include: [
					'Employee',
					'Status',
					{
						model: 'Contract',
						include: [
							{ model: 'ClientProduct', include: ['Client', 'Product'] },
						],
					},
				],
				...options,
			}
		)
		return this.mapAffiliate(contractAffiliate)
	}
	map(contractAffiliate) {
		if (contractAffiliate) {
			const mapped = contractAffiliate.dataValues
			mapped.Affiliate = mapped.Employee
			delete mapped.Employee
			return mapped
		}
	}
	mapAffiliate(contracts) {
		if (Array.isArray(contracts)) {
			return contracts.map(this.map) || [] 
		}
		return this.map(contracts)
	}
}
