module.exports = class AssignAffiliateService {
	constructor(contractRepository, contractAffiliateRepository) {
		this.contractRepository = contractRepository
		this.contractAffiliateRepository = contractAffiliateRepository
	}
	async assignAffiliate(contractId, contractAffiliate) {
		await this.contractAffiliateRepository.create({
			contractFk: contractId,
			...contractAffiliate,
		})
	}
	async updateAffiliate(contractAffiliate) {
		return await this.contractAffiliateRepository.update(contractAffiliate)
	}
}
