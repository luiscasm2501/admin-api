const { DateTime } = require('luxon')
const logger = require('../../common/controllers/logger/logger')
const statuses = require('../../common/persistence/status/statuses')

module.exports = class checkAffiliatePaymentService {
	constructor(
		invoiceRepository,
		contractAffiliateRepository,
		sendNotificationService
	) {
		this.invoiceRepository = invoiceRepository
		this.contractAffiliateRepository = contractAffiliateRepository
		this.sendNotificationService = sendNotificationService
	}
	async checkAffiliatePayment(receipt, attemps = 0) {
		const invoice = await this.invoiceRepository.getById(receipt.invoiceFk, {
			include: [
				{
					model: 'ContractInvoices',
					required: true,
				},
			],
		})
		const contractInvoice = invoice?.ContractInvoices
			? invoice.ContractInvoices[0]
			: undefined
		if (contractInvoice) {
			const contractAffiliates = await this.contractAffiliateRepository.getAll({
				contractFk: contractInvoice.contractFk,
				include: ['Employee'],
			})
			const today = DateTime.now()
			const sentStatus = await this.invoiceRepository.findStatus('sent')
			contractAffiliates.rows.forEach((contractAff) => {
				const invoiceAmount = parseFloat(
					(contractAff.gain * receipt.amount).toFixed(2)
				)
				contractAff
					.createInvoice({
						amount: invoiceAmount,
						startDate: today.toSQLDate(),
						deadline: today.plus({ weeks: 2 }).toSQLDate(),
						statusFk: sentStatus.statusId,
					})
					.then(() => {
						this.sendNotificationService.sendNotification({
							title: 'Pending payment for affiliate',
							kind: 'reminder',
							content: {
								description: `Under Affiliates, an invoice has been created for ${contractAff.Employee.firstName} ${contractAff.Employee.lastName}'s pay, for $${invoiceAmount}.`,
							},
						})
					})
					.catch((e) => {
						logger.error(
							`Couldn't add invoice to affiliate (attempt ${attemps}) [${e.message}]`
						)
						attemps += 1
						if (attemps < 3) {
							this.checkAffiliatePayment(receipt, attemps)
						}
					})
			})
		}
	}
}
