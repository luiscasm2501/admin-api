module.exports = class addAffiliateTemplateService {
	constructor(affiliateTemplateRepository) {
		this.affiliateTemplateRepository = affiliateTemplateRepository
	}
	async addAffiliateTemplate(affiliateId, templateId) {
		if (affiliateId && templateId) {
			await this.affiliateTemplateRepository.remove(
				affiliateId,
				true,
				'affiliateFk'
			)
			return await this.affiliateTemplateRepository.create({
				affiliateFk: affiliateId,
				fileTemplateFk: templateId,
			})
		}
	}
}
