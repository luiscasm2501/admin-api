const { DataTypes } = require('sequelize')

module.exports = function makeModel(apiDb) {
	const ContractAffiliate = apiDb.define(
		'ContractAffiliate',
		{
			gain: {
				type: DataTypes.FLOAT,
				allowNull: false,
				validate: {
					min: 0.01,
					max: 0.5,
				},
				default: 0.2,
			},
			mode: {
				type: DataTypes.STRING,
				defaultValue: 'financed',
				validate: {
					isIn: [['financed', 'up-front']],
				},
			},
			contractAffiliateId: {
				type: DataTypes.UUID,
				allowNull: false,
				primaryKey: true,
				defaultValue: DataTypes.UUIDV4,
			},
		},
		{
			paranoid: true,
		}
	)
	return ContractAffiliate
}
