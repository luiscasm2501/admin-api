const SequelizeRepo = require('../../common/persistence/sequilize/sequelizeRepo')

class affiliateTemplateRepository extends SequelizeRepo {
	constructor(apiDb) {
		super(apiDb.models.AffiliateTemplate, apiDb)
	}
}

module.exports = affiliateTemplateRepository
