const SequelizeRepo = require('../../common/persistence/sequilize/sequelizeRepo')
const statuses = require('../../common/persistence/status/statuses')

class ContractAffiliateRepository extends SequelizeRepo {
	constructor(apiDb) {
		super(apiDb.models.ContractAffiliate, apiDb, statuses.DRAFT)
	}
}

module.exports = ContractAffiliateRepository
