module.exports = function setupModel(db) {
	const { Employee, Contract, ContractAffiliate, Status, FileTemplate } =
    db.models

	Employee.belongsToMany(Contract, {
		through: { model: ContractAffiliate, unique: false },
		foreignKey: 'affiliateFk',
		otherKey: 'contractFk',
	})
	Contract.belongsToMany(Employee, {
		through: { model: ContractAffiliate, unique: false },
		foreignKey: 'contractFk',
		otherKey: 'affiliateFk',
	})

	Employee.hasMany(ContractAffiliate, {
		foreignKey: 'affiliateFk',
	})

	ContractAffiliate.belongsTo(Employee, {
		foreignKey: 'affiliateFk',
		allowNull: false,
	})

	Contract.hasMany(ContractAffiliate, {
		foreignKey: 'contractFk',
	})

	ContractAffiliate.belongsTo(Contract, {
		foreignKey: 'contractFk',
		allowNull: false,
	})

	Status.hasMany(ContractAffiliate, {
		foreignKey: 'statusFk',
	})

	ContractAffiliate.belongsTo(Status, {
		foreignKey: 'statusFk',
	})
	Employee.belongsToMany(FileTemplate, {
		through: 'AffiliateTemplate',
		foreignKey: 'affiliateFk',
		otherKey: 'fileTemplateFk',
	})

	FileTemplate.belongsToMany(Employee, {
		otherKey: 'affiliateFk',
		through: 'AffiliateTemplate',
		foreignKey: 'fileTemplateFk',
	})
}
