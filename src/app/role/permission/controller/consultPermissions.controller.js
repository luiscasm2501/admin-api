const { createController } = require('awilix-router-core')

const consultControllers = (getPermissionsService) => ({
	getPerms: async (req, res, next) => {
		try {
			const result = await getPermissionsService.getPermissions(req.query)
			next(result)
		} catch (error) {
			next(error)
		}
	},
})

module.exports = createController(consultControllers)
	.prefix('/perms')
	.get('', 'getPerms')

/**
 * @swagger
 * /perms:
 *  get:
 *    security: []
 *    tags: [Roles]
 *    summary: get list of all existent permissions
 *    responses:
 *      200:
 *        description: list of permissions
 *        content:
 *          application/json:
 *            schema:
 *              type: array
 *              properties:
 *                name:
 *                  type: string
 *                type:
 *                  type: string
 *                permissionId:
 *                  type: string
 *              example:
 *                - name: Contract
 *                  type: R
 *                  permissionId: <uuid>
 *                - name: Client
 *                  type: C
 *                  permissionId: <uuid>
 */
