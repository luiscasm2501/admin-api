const { createController } = require('awilix-router-core')
const NotFoundError = require('../../common/controllers/error-handling/notFoundError')

const consultControllers = (getRolesService) => ({
	getRolesPermissions: async (req, res, next) => {
		try {
			const result = await getRolesService.getRolesPermissions(req.query)
			next(result)
		} catch (error) {
			next(error)
		}
	},
	getRoles: async (req, res, next) => {
		try {
			const result = await getRolesService.getRoles(req.query)
			next(result)
		} catch (error) {
			next(error)
		}
	},
	getRole: async (req, res, next) => {
		try {
			const id = req.params.id
			const result = await getRolesService.getRole(id)
			if (result) res.send(result)
			else next(NotFoundError(`role ${id}`))
		} catch (error) {
			next(error)
		}
	},
})

module.exports = createController(consultControllers)
	.prefix('/roles')
	.get('', 'getRoles')
	.get('/perms', 'getRolesPermissions')
	.get('/:id', 'getRole')

/**
 * @swagger
 * /roles:
 *  get:
 *    tags: [Roles]
 *    summary: retrieve a list of roles
 *    parameters:
 *      - $ref: '#/components/parameters/page'
 *      - $ref: '#/components/parameters/size'
 *      - $ref: '#/components/parameters/sort'
 *    responses:
 *      200:
 *        description: list of roles
 *        content:
 *          application/json:
 *            schema:
 *              type: array
 *              $ref: '#/components/schemas/Role'
 * /roles/{roleId}:
 *  get:
 *    tags: [Roles]
 *    summary: get a role by id
 *    parameters:
 *      - $ref: '#/components/parameters/roleId'
 *    responses:
 *      200:
 *        description: role record
 *        content:
 *          application/json:
 *            schemas:
 *              $ref: '#/components/schemas/Role'
 *      400:
 *        $ref: '#/components/responses/notFound'
 */
