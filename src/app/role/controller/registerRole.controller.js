const { createController } = require('awilix-router-core')

const registerControllers = (
	createRoleService,
	updateRoleService,
	deleteRoleService
) => ({
	createRole: async (req, res, next) => {
		try {
			const result = await createRoleService.createRole(req.body)
			res.send(result)
		} catch (error) {
			next(error)
		}
	},
	updateRole: async (req, res, next) => {
		try {
			const result = await updateRoleService.updateRole(req.body)
			res.send(result)
		} catch (error) {
			next(error)
		}
	},
	deleteRole: async (req, res, next) => {
		try {
			const result = await deleteRoleService.deleteRole(req.params.id)
			res.send(result)
		} catch (error) {
			next(error)
		}
	},
})

module.exports = createController(registerControllers)
	.prefix('/roles')
	.post('', 'createRole')
	.put('/:id', 'updateRole')
	.delete('/:id', 'deleteRole')

/**
 * @swagger
 * /roles:
 *  post:
 *    tags: [Roles]
 *    summary: create a new role
 *    requestBody:
 *      required: true
 *      content:
 *        application/json:
 *          schema:
 *            $ref: '#/components/schemas/Role'
 *    responses:
 *      200:
 *        description: role created
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/Role'
 *
 * /roles/{roleId}:
 *  put:
 *    tags: [Roles]
 *    summary: update a role by id
 *    parameters:
 *      - $ref: '#/components/parameters/roleId'
 *    requestBody:
 *      required: true
 *      content:
 *        application/json:
 *          schema:
 *            $ref: '#/components/schemas/Role'
 *    responses:
 *      200:
 *        description: role succesfully updated
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/Role'
 *  delete:
 *    tags: [Roles]
 *    summary: delete a role by id, certain roles mark as not deletable cannot perform this
 *    parameters:
 *      - $ref: '#/components/parameters/roleId'
 *    responses:
 *      200:
 *        description: role succesfully deleted
 *      400:
 *        $ref: '#/components/responses/notFound'
 *
 */
