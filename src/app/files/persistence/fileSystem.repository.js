const mongoRepo = require('../../common/persistence/mongo/mongoRepo')

module.exports = class fileSystemRepository extends mongoRepo {
	constructor(nosqlDb) {
		super(nosqlDb, 'file_system')
	}
}
