function makeSchema(Schema) {
	const fileSchema = new Schema({
		path: {
			type: String,
			required: true,
		},
		parent: {
			type: String,
		},
		type: {
			type: String,
			required: true,
		},
		size: {
			type: String,
		},
		ext: {
			type: String,
		},
		name: {
			type: String,
			required: true,
		},
		upload_date: {
			type: Date,
			required: true,
		},
		deletable: {
			type: Boolean,
			default: false,
		},
		entity_id: { //id of the entity it's related to
			type:String,
			required:false,			
		},
		entity_type:{//type of the entity it's related to
			type:String,
			required:false,			
		}
	})
	return {
		name: 'file_system',
		schema: fileSchema,
	}
}
module.exports = makeSchema
