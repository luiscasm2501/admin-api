function registerEvent(bus, container) {
	const event = 'fileCreated'
	const ctx = 'createInFileSystemService'
	const context = container.resolve(ctx)
	bus.register(event, (file, response) => {
		context.createFile(file, undefined, response)
	})
}
module.exports = registerEvent
