const trycatchevent = require('../../common/events/trycatchevent')

function registerEvent(bus, container) {
	const event = 'fileTemplateUpdated'
	const ctx = 'updateFileMetaService'
	const context = container.resolve(ctx)
	bus.register(event, (file) => {
		trycatchevent(() => {
			context.updateFileMeta(file)
		})
	})
}
module.exports = registerEvent
