function registerEvent(bus, container) {
	const event = 'fileTemplateDeleted'
	const ctx = 'removeFileMetaService'
	const context = container.resolve(ctx)
	bus.register(event, (file) => {
		context.removeFileMeta(file)
	})
}
module.exports = registerEvent
