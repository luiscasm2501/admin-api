const { unlink, existsSync, rm } = require('fs')
const path = require('path')
const logger = require('../../common/controllers/logger/logger')
const NotFoundError = require('../../common/controllers/error-handling/notFoundError')
require('dotenv').config()
module.exports = class deleteInFileSystemService {
	constructor(fileSystemRepository) {
		this.repo = fileSystemRepository
	}
	async deleteInFileSystem(fileId, forceDelete = false, callback = 1 ) {
		const fileMeta = await this.repo.getById(fileId)
		if (fileMeta) {
			if (!fileMeta.deletable && !forceDelete) {
				return
			}
			const url = path.join(
				fileMeta.parent.replace(`${process.env.FILE_FOLDER}`, ''),
				fileMeta.name
			)
			const file_path = path.join(process.env.FILE_SRC, url)
			const type = fileMeta.type
			if (type == 'file' && callback == 1)
				existsSync(file_path) &&
          unlink(file_path, (err) => {
          	if (err) {
          		logger.error(`Couldn't remove file [${err.message}]`)
          	}
          })
			else if (type == 'folder' && callback == 1)
				existsSync(file_path) &&
          rm(file_path, { recursive: true, force: true }, (err) => {
          	if (err) {
          		logger.error(`Couldn't  delete folder [${e.message}]`)
          	}
          })

			if (fileMeta.type == 'folder') {
				const parent = path.join(fileMeta.parent, fileMeta.name)
				const deletables = await this.repo.getAll({
					conditions: { parent },
				})
				deletables.forEach((deletable) => {
					callback += 1
					this.deleteInFileSystem(deletable._id, forceDelete, callback)
				})
			}
			return await this.repo.remove(fileId)
		} else if (callback == 1) {
			throw new NotFoundError('file or folder not found')
		}
	}
}
