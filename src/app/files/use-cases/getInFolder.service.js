const cleanPath = require('../domain/cleanPath')

require('dotenv').config()
module.exports = class getInFolderService {
	constructor(fileSystemRepository) {
		this.fileSystemRepository = fileSystemRepository
	}
	async getInFolder(folder, options = {}) {
		folder = folder || process.env.FILE_FOLDER
		folder = cleanPath(folder)
		return await this.fileSystemRepository.getAll({
			conditions: { parent: folder },
			count: true,
			settings: options,
		})
	}
}
