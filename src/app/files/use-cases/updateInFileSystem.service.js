const NotFoundError = require('../../common/controllers/error-handling/notFoundError')
const mapForFile = require('../domain/mapForFile')
const { existsSync, unlink } = require('fs')
const logger = require('../../common/controllers/logger/logger')
require('dotenv').config()

module.exports = class updateInFileSystemService {
	constructor(fileSystemRepository) {
		this.fileSystemRepository = fileSystemRepository
	}
	async updateFile(fileId, file, data = {}) {		
		const mappedFile = mapForFile(file, undefined, data)
		delete mappedFile.deletable

		const oldFile = await this.fileSystemRepository.getById(fileId)
		if (!oldFile) {
			throw new NotFoundError(`file ${fileId}`)
		}
		const updatedFile = await this.fileSystemRepository.update({
			...mappedFile,
			_id: fileId,
		})
		// if old file path is different than new path, delete old file
		if (oldFile.path !== updatedFile.path) {
			const file_path = oldFile.path.replace(
				process.env.FILE_FOLDER,
				process.env.FILE_SRC
			)
			existsSync(file_path) &&
			unlink(file_path, (err) => {
				if (err) {
					logger.error(`Couldn't remove file [${err.message}]`)
				}
			})
		}
		return updatedFile
	}
}
