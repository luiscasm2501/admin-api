const mapForFile = require('../domain/mapForFile')
const mapForFolder = require('../domain/mapForFolder')
module.exports = class createInFileSystemService {
	constructor(fileSystemRepository) {
		this.fileSystemRepository = fileSystemRepository
	}
	async createFile(file, deletable = false, data = {} ) {
		const mappedFile = mapForFile(file, deletable, data)
		return await this.fileSystemRepository.create(mappedFile)
	}
	async createFolder(
		folder = { name: '', destination: '' },
		deletable = false
	) {
		const mappedFolder = mapForFolder(folder, deletable)
		return await this.fileSystemRepository.create(mappedFolder)
	}
}
