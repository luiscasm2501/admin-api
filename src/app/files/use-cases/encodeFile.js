var fs = require('fs')

module.exports = function base64_encode(file) {
	// read binary data
	var base64 = fs.readFileSync(file, 'base64')

	// convert binary data to base64 encoded string
	return base64
}
