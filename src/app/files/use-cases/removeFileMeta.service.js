const { unlink } = require('fs')
const path = require('path')
const logger = require('../../common/controllers/logger/logger')
require('dotenv').config()
module.exports = class updateFileMetaService {
	constructor(fileTemplateMetaRepository) {
		this.repo = fileTemplateMetaRepository
	}
	async removeFileMeta(fileId) {
		const fileMeta = await this.repo.getById(fileId)
		if (fileMeta) {
			//TODO: CHECK IF IT WORKS
			const url = fileMeta.url.split('/storage/')[1]
			const file_path = path.join(process.env.FILE_SRC, url)
			unlink(file_path, (err) => {
				if (err) {
					logger.error(`Couldn't remove file [${err.message}]`)
				}
			})
			return await this.repo.remove(fileId)
		}
	}
}
