const fs = require('fs')
const JSZip = require('jszip')

const addFilesFromDirectoryToZip = (zip, fileObjects) => {
	fileObjects.forEach((fileObject) => {
		const fileToZip =
      fs.statSync(fileObject.url).isFile() &&
      fs.readFileSync(fileObject.url, {
      	withFileTypes: true,
      })
		zip.file(fileObject.filename, fileToZip)
	})
}

module.exports = async function makeZip(...fileObjects) {
	const zip = new JSZip()
	addFilesFromDirectoryToZip(zip, fileObjects)
	const zipBase64 = await zip.generateAsync({ type: 'base64' })
	return zipBase64
}
