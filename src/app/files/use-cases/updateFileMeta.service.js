const mapFileData = require('../domain/mapFileData')
const { unlink } = require('fs')
const path = require('path')
const logger = require('../../common/controllers/logger/logger')
module.exports = class updateFileMetaService {
	constructor(fileTemplateMetaRepository) {
		this.repo = fileTemplateMetaRepository
	}
	async updateFileMeta(file) {
		const fileMeta = await this.repo.getById(file.id)
		if (fileMeta) {
			const url = fileMeta.url.split('/storage/')[1]
			const file_path = path.join(process.env.FILE_SRC, url)
			if (file.filename !== fileMeta.name) {
				unlink(file_path, (err) => {
					if (err) {
						logger.error(`Couldn't remove file [${err.message}]`)
					}
				})
			}
		}
		const parsedFile = mapFileData(file)
		return await this.repo.update({ ...parsedFile, id: file.id })
	}
}
