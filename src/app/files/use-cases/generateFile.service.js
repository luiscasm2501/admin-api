const fs = require('fs')
const path = require('path')
const HTMLtoDOCX = require('html-to-docx')
const HTMLtoPDF = require('html-pdf')
require('dotenv').config()

module.exports = class generateFileService {
	constructor(hbs) {
		this.hbs = hbs
	}
	async generateFile(
		template,
		data,
		filename = 'file',
		outFolder = 'invoices',
		options = {
			layout: path.join(
				__dirname,
				'../../files/static/layouts/base.handlebars'
			),
		},
		pdfOptions = {}
	) {
		const htmlTemplate = await this.hbs.renderView(template, {
			...options,
			...data,
			api: process.env.API_ROOT,
		})
		const fileBuffer = await HTMLtoDOCX(htmlTemplate, null, {
			footer: true,
			pageNumber: true,
		})

		let url = path.join(process.env.FILE_SRC, outFolder, filename)

		await new Promise(function (resolve, reject) {
			HTMLtoPDF.create(htmlTemplate, pdfOptions).toFile(
				url + '.pdf',
				function (err, res) {
					if (err) return reject(err)
					resolve(res)
				}
			)
		})
		await fs.writeFileSync(url + '.docx', fileBuffer)
		return {
			pdf: { url: url + '.pdf', filename: filename + '.pdf' },
			doc: { url: url + '.docx', filename: filename + '.docx' },
		}
	}
}
