const fs = require('fs')
const { resolve } = require('path')
const path = require('path')
const rimraf = require('rimraf')
const { error } = require('winston')
const logger = require('../../common/controllers/logger/logger')

require('dotenv').config()
module.exports = function cleanContractFiles() {
	const deletePath = path.join(process.env.FILE_SRC, 'invoices', '*.*')
	rimraf(deletePath, (error) => {
		if (error) {
			logger.error(`error at deleting contract files ${error.message}`)
		}
		logger.info('deleted contract files')
	})
}
