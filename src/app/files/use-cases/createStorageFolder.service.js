const fs = require('fs')
const path = require('path')
const logger = require('../../common/controllers/logger/logger')
const cleanPath = require('../domain/cleanPath')
require('dotenv').config()

const storageSrc = process.env.FILE_SRC
const storageName = process.env.FILE_FOLDER
module.exports = class createStorageFolderService {
	constructor(createInFileSystemService) {
		this.createInFileSystemService = createInFileSystemService
	}
	createStorageFolder(
		folder = { name: '', path: '' },
		createInDB = true,
		deletable = false
	) {
		let folderDest = folder.path || ''
		//? path.join(folder.path, folder.name)
		//: folder.name
		if (folderDest && folderDest.length) {
			folderDest = folderDest.replace(`${storageName}`, '')
			folderDest = cleanPath(folderDest)
		}
		const folderParent = path.join(storageName, folderDest)
		folderDest = folder.path ? path.join(folderDest, folder.name) : folder.name
		const folderSrc = path.join(storageSrc, folderDest)
		if (!fs.existsSync(folderSrc)) {
			fs.mkdir(folderSrc, { recursive: true }, (err) => {
				if (err) {
					logger.error(`Couldn't create folder ${folder.name} [${err.message}]`)
				} else {
					if (createInDB) {
						this.createInFileSystemService
							.createFolder(
								{
									name: folder.name,
									destination: folderParent,
								},
								deletable
							)
							.catch((e) => {
								fs.rm(folderSrc, { recursive: true, force: true }, (err) => {
									if (err) {
										logger.error(`Couldn't even delete folder [${e.message}]`)
									}
								})
								logger.error(`Couldn't create folder [${e.message}]`)
								return
							})
					}
					logger.info(`${folder.name} folder created`)
				}
			})
		}
	}
}
