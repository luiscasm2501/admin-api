const mapFileData = require('../domain/mapFileData')

module.exports = class createFileMetaService {
	constructor(
		fileTemplateMetaRepository,
		fileReceiptMetaRepository,
		verificationFileMetaRepository
	) {
		this.repo = fileTemplateMetaRepository
		this.receiptRepo = fileReceiptMetaRepository
		this.verificationRepo = verificationFileMetaRepository
	}
	async createFileMeta(file) {
		const parsedFile = mapFileData(file)
		const id =
      file.fileTemplateId || file.fileReceiptId || file.verificationFileId
		if (file.fileTemplateId) {
			return await this.repo.create({ ...parsedFile, _id: id })
		}
		if (file.verificationFileId) {
			return await this.verificationRepo.create({ _id: id, ...parsedFile })
		}
		return await this.receiptRepo.create({ ...parsedFile, _id: id })
	}
}
