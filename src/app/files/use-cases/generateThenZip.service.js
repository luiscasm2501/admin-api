const makeZip = require('./makeZip')
const path = require('path')

module.exports = class generateThenZipService {
	constructor(generateFileService) {
		this.generateFileService = generateFileService
	}
	async generateThenZip(
		template,
		data,
		filename = 'file',
		outFolder = 'invoices',
		options = {
			layout: path.join(
				__dirname,
				'../../files/static/layouts/base.handlebars'
			),
		},
		pdfOptions = {}
	) {
		const fileUrls = await this.generateFileService.generateFile(
			template,
			data,
			filename,
			outFolder,
			options,
			pdfOptions
		)
		return await makeZip(fileUrls.pdf, fileUrls.doc)
	}
}
