module.exports = function makeMiddleware(container) {
	const bus = container.resolve('bus')
	return function digitalizeFileMiddleware(response, req, res, next) {
		const files = req.file ? [req.file] : req.files
		const emitFiles = (files) => {
			if (Array.isArray(files)) {
				files.forEach(emitFiles)
			} else {
				bus.emit('fileCreated', files, response)
			}
		}
		if (files && !(response instanceof Error)) {
			if (response) {
				(Array.isArray(files) ? files : Object.values(files)).forEach(
					emitFiles
				)
				res.send(response)
				return
			}
			res.send('OK FILE')
			return
		}
		next(response)
	}
}
