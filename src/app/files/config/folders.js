const fs = require('fs')
require('dotenv').config()

const FOLDERS = {
	INVOICE: 'invoices',
	RECEIPT: 'receipt',
	TEMPLATE: 'template',
	SIGNATURE: 'signature',
	CONTRACT: 'contracts',
	BULLETIN: 'bulletins',
	MESSAGE: 'messages',
	CLIENT_VERIFY: 'client-verification',
}

const storageSrc = process.env.FILE_SRC

module.exports = {
	makeStorageFolders: (container) => {
		//Create base storage folder
		const createStorageFolderService = container.resolve(
			'createStorageFolderService'
		)
		!fs.existsSync(storageSrc) && fs.mkdirSync(storageSrc, { recursive: true })
		for (let folder of Object.values(FOLDERS)) {
			//create folders inside storage
			createStorageFolderService.createStorageFolder({ name: folder, path: '' })
		}
	},
	FOLDERS,
}
