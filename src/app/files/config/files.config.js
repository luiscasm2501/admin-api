require('dotenv').config()
const { DateTime } = require('luxon')
const multer = require('multer')
const {
	VERIFY_FILE_FIELDS,
	GET_VERIFY_FOLDER,
} = require('../../client-verification/constants')
const { FOLDERS } = require('./folders')
const reducer = (previousValue, currentValue) => previousValue + currentValue

function generateFileName(req, file) {
	let filename = file.originalname
	const splitFile = filename.split('.')
	const extension = splitFile.pop()
	const ms = DateTime.now().toMillis()
	const body = { ...req.body }
	if (Object.values(VERIFY_FILE_FIELDS).includes(file.fieldname)) {
		return `client-${file.fieldname}-proof.${extension}`
	}
	switch (file.fieldname) {
	case 'receipt':
		filename = `${body.paymentMethod}_${body.amount}_${ms}`
		break
	case 'signature':
		filename = `${body.name}_${body.title}`
		break
	case 'template':
		filename = body.name
		break
	case 'contract':
		filename = `CONTRACT-${body.client}-${body.product}-SIGNED.${extension}`
		return filename
	case 'bulletins':
		filename = `${body.title}_${ms}.${extension}`
		return filename
	case 'messages':
		if (body.body) filename = `${body.body.split(' ')[0]}_${ms}.${extension}`
		else if (body.subject)
			filename = `${body.contractFk}_${
				body.subject.split(' ')[0]
			}_${ms}.${extension}`
		else filename = `${ms}.${extension}`
		return filename
	default:
		filename = splitFile.reduce(reducer)
	}
	const transformedName = `${file.fieldname}_` + filename + `.${extension}`
	return transformedName.replace(/\s+/g, '-')
}

function generatePath(req, file) {
	const kind = file.fieldname || 'other'
	let path = `${process.env.FILE_SRC}/`
	const contractId = req.params.contractId

	if (Object.values(VERIFY_FILE_FIELDS).includes(kind)) {
		return GET_VERIFY_FOLDER(req._client)
	}
	if (contractId && kind == 'contract') {
		path += `contracts/${contractId}`
	} else if (req.body?.filepath) {
		const filePath = req.body.filepath.replace(`${process.env.FILE_FOLDER}`, '')
		path += filePath
	} else {
		path += kind.toLowerCase()
	}
	return path
}

const storage = multer.diskStorage({
	destination: function (req, file, cb) {
		const path = generatePath(req, file)
		cb(null, path)
	},
	filename: function (req, file, cb) {
		cb(null, generateFileName(req, file))
	},
})

const upload = multer({ storage: storage })

module.exports = upload
