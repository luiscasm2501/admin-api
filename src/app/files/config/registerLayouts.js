const fs = require('fs')
const path = require('path')
const partials = {
	base: '../static/layouts/base.handlebars',
	base_hiring: '../static/layouts/base_hiring.handlebars',
}
module.exports = function registerLayouts(handlebars) {
	for (let [name, filepath] of Object.entries(partials))
		handlebars.registerPartial(
			name,
			fs.readFileSync(path.join(__dirname, filepath), 'utf-8')
		)
}
