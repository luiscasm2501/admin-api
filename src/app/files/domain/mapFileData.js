require('dotenv').config()
const mapForFile = require('./mapForFile')

module.exports = function mapFileData(file) {
	if (!file) return {}
	const mappedFile = mapForFile(file)
	return {
		url: `${process.env.API_ROOT}${
			process.env.API_PREFIX
		}/${mappedFile.path.replace(/\\/g, '/')}`,
		type: mappedFile.ext,
		size: file.size,
		name: file.filename,
		upload_date: mappedFile.upload_date,
	}
}
