require('dotenv').config()
const { DateTime } = require('luxon')
const path = require('path')
const cleanPath = require('./cleanPath')
module.exports = function mapForFolder(
	folder = { name: '', destination: '' },
	deletable = false
) {
	if (!folder) return {}
	//const nextPath = folder.destination.split(process.env.FILE_SRC).pop()
	let destination = folder.destination
	destination = cleanPath(destination)
	return {
		path: cleanPath(path.join(destination, folder.name)),
		type: 'folder',
		name: folder.name,
		parent: destination,
		deletable,
		upload_date: DateTime.now().toString(),
	}
}
