module.exports = function cleanPath(path) {
	let destination = path
	if (!destination) {
		return destination
	}
	while (true) {
		if (destination.endsWith('\\') || destination.endsWith('/')) {
			destination = destination.slice(0, -1)
		} else {
			break
		}
	}
	//remove repeating backslashes and frontslashes and set them to a single type of slash
	return destination.replace(/[/\\\\]/g, '\\').replace(/([\s/\\\\,])\1+/g, '\\')
}
