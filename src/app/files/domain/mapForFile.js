require('dotenv').config()
const { DateTime } = require('luxon')
const path = require('path')
const cleanPath = require('./cleanPath')
module.exports = function mapForFile(file, deletable = false, data = {}) {
	if (!file) return {}
	const nextPath = file.destination.replace(process.env.FILE_SRC, '')
	let destination = path.join(process.env.FILE_FOLDER, nextPath)
	destination = cleanPath(destination)

	let mappedFile = {
		path: cleanPath(path.join(destination, file.filename)),
		ext: file.filename.split('.').pop(),
		type: 'file',
		size: file.size,
		name: file.filename,
		parent: destination,
		deletable,
		upload_date: DateTime.now().toString(),
	}
	if (data.entity_id) mappedFile['entity_id'] = data.entity_id
	if (data.entity_type) mappedFile['entity_type'] = data.entity_type

	return mappedFile
}
