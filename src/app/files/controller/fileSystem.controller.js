const { createController } = require('awilix-router-core')
const upload = require('../config/files.config')

const consultControllers = (
	getInFolderService,
	createInFileSystemService,
	createStorageFolderService,
	deleteInFileSystemService
) => ({
	getInFolder: async (req, res, next) => {
		try {
			const result = await getInFolderService.getInFolder(
				req.query.r,
				req.query
			)
			next(result)
		} catch (error) {
			next(error)
		}
	},
	uploadFile: async (req, res, next) => {
		try {
			if (!req.file) {
				throw new ValidationException('A file is required')
			}

			const result = await createInFileSystemService.createFile(req.file, true)
			res.send(result)
		} catch (error) {
			next(error)
		}
	},
	uploadFolder: async (req, res, next) => {
		try {
			await createStorageFolderService.createStorageFolder(req.body, true, true)

			res.send('FOLDER OK')
		} catch (error) {
			next(error)
		}
	},
	deleteItem: async (req, res, next) => {
		try {
			await deleteInFileSystemService.deleteInFileSystem(req.params.id)

			res.send('FOLDER OK')
		} catch (error) {
			next(error)
		}
	},
})

module.exports = createController(consultControllers)
	.prefix('/file-system')
	.get('', 'getInFolder')
	.delete('/folder/:id', 'deleteItem')
	.delete('/file/:id', 'deleteItem')
	.post('/folder', 'uploadFolder')
	.post('/file', 'uploadFile')
	.before(upload.single('file'))
