const { createController } = require('awilix-router-core')

const upload = require('../config/files.config')

const registerControllers = (updateFileTemplateService) => ({
	updateTemplate: async (req, res, next) => {
		try {
			const file = req.files
			const result = await updateFileTemplateService.updateFileTemplate(
				{
					...req.body,
					...file[0],
				},
				file[0]
			)
			res.send(result)
		} catch (error) {
			next(error)
		}
	},
})

module.exports = createController(registerControllers)
	.prefix('/templates')
	.put('/:id', 'updateTemplate')
	.before(upload.array('template', 5))

/**
 * @swagger
 * /templates/{fileTemplateId}:
 *  put:
 *    tags: [Templates]
 *    summary: update a file template. supports reupload of its file
 *    requestBody:
 *      content:
 *        multipart/form-data:
 *          schema:
 *            $ref: '#/components/schemas/FileTemplate'
 *      required: true
 *    responses:
 *      200:
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/FileTemplate'
 *        description: succesfully updated file
 */
