const { createController } = require('awilix-router-core')
const NotFoundError = require('../../common/controllers/error-handling/notFoundError')

const consultControllers = (getFileTemplatesService) => ({
	getTemplates: async (req, res, next) => {
		try {
			const result = await getFileTemplatesService.getFileTemplates(req.query)
			next(result)
		} catch (error) {
			next(error)
		}
	},
	getTemplateFile: async (req, res, next) => {
		try {
			const id = req.params.id
			const file = await getFileTemplatesService.getFileTemplate(id)
			if (file) {
				res.send(file)
			} else throw new NotFoundError(`file ${id}`)
		} catch (error) {
			next(error)
		}
	},
})

module.exports = createController(consultControllers)
	.prefix('/templates')
	.get('', 'getTemplates')
	.get('/:id', 'getTemplateFile')

/**
 * @swagger
 * /templates:
 *  get:
 *    summary: retrieve a list of file template records
 *    tags: [Templates]
 *    parameters:
 *      - $ref: '#/components/parameters/page'
 *      - $ref: '#/components/parameters/sort'
 *    responses:
 *      200:
 *        description: list of file templates
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/FileTemplate'
 *              type: array
 *
 * /templates/{fileTemplateId}:
 *  get:
 *    summary: get a file template by id
 *    tags: [Templates]
 *    parameters:
 *      - $ref: '#/components/parameters/fileTemplateId'
 *    responses:
 *      200:
 *        description: file template record
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/FileTemplate'
 *
 */
