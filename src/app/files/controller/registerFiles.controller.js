const { createController } = require('awilix-router-core')
const ValidationException = require('../../common/domain/validationException')
const upload = require('../config/files.config')

const registerControllers = (
	uploadTemplateService,
	deleteFileTemplateService
) => ({
	uploadTemplate: async (req, res, next) => {
		try {
			if (!req.file) {
				throw new ValidationException('A template file is required')
			}
			const result = await uploadTemplateService.uploadTemplate({
				...req.body,
				...req.file,
			})
			next(result)
		} catch (error) {
			next(error)
		}
	},

	deleteTemplate: (req, res, next) => {
		const id = req.params.id
		if (id)
			deleteFileTemplateService
				.deleteFileTemplate(id)
				.then((result) => {
					if (result) {
						res.status(200).send(`Succesfully deleted template ${id}`)
					} else {
						next(new NotFoundError(`Template ${id}`))
					}
				})
				.catch((err) => {
					next(err)
				})
		else throw new ValidationException('Missing id')
	},
	restoreTemplate: (req, res, next) => {
		const id = req.params.id
		if (id)
			deleteFileTemplateService
				.restoreFileTemplate(id)
				.then((result) => {
					if (result) {
						res.status(200).send(`Succesfully restored template ${id}`)
					} else {
						next(new NotFoundError(`Template ${id}`))
					}
				})
				.catch((err) => {
					next(err)
				})
		else throw new ValidationException('Missing id')
	},
})

module.exports = createController(registerControllers)
	.prefix('/templates')
	.delete('/:id', 'deleteTemplate')
	.patch('/:id', 'restoreTemplate')
	.post('', 'uploadTemplate')
	.before(upload.single('template'))

/**
 * @swagger
 * /templates:
 *  post:
 *    tags: [Templates]
 *    summary: upload a new file template record, with file included
 *    requestBody:
 *      required: true
 *      content:
 *        multipart/form-data:
 *          schema:
 *            $ref: '#/components/schemas/FileTemplate'
 *    responses:
 *      200:
 *        description: succesfully registered file template
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/FileTemplate'
 * /templates/{fileTemplateId}:
 *  delete:
 *    tags: [Templates]
 *    summary: delete a file template record
 *    description: on the first go, it marks it a deleted, if used again, deletes it permanently
 *    parameters:
 *      - $ref: '#/components/parameters/fileTemplateId'
 *    responses:
 *      200:
 *        description: file template successfully deleted / marked as "deleted"
 *  patch:
 *    tags: [Templates]
 *    summary: restores a file template record marked as "deleted"
 *    parameters:
 *      - $ref: '#/components/parameters/fileTemplateId'
 *    responses:
 *      200:
 *        description: file template successfully restored
 */
