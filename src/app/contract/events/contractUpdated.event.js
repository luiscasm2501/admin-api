const logger = require('../../common/controllers/logger/logger')

function registerEvent(bus, container) {
	const event = 'contractUpdated'

	const context = container.resolve('generateContractService')
	const sender = container.resolve('sendContractFileService')
	bus.register(event, (contract) => {
		context
			.generateContract(contract.contractId)
			.then(() => {
				if (contract.prepare) {
					sender.sendContractFile(contract.contractId).catch((e) => {
						logger.error(`Couldn't send contract file to client [${e.message}]`)
					})
				}
			})
			.catch((e) => {
				logger.error(`Couldn't generate contract file [${e.message}]`)
			})
	})
}
module.exports = registerEvent
