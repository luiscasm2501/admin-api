module.exports = function mapContractDownload(contract) {
	if (!contract) {
		return {}
	}
	/*

    clientName, clientLegalIdentifier, productName
    */
	const client = contract.ClientProduct.Client
	const product = contract.ClientProduct.Product
	return {
		clientName: client.name,
		clientEmail: client.email,
		clientLegalIdentifier: client.legalIdentifier,
		productName: product.name,
		startDate: contract.startDate,
		expirationDate: contract.expirationDate,
		totalPayment: contract.totalPayment,
		contractId: contract.contractId,
	}
}
