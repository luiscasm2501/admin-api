module.exports = class generateContractService {
	constructor(getContractDownloadService, generateFileService) {
		this.getContractDownloadService = getContractDownloadService
		this.generateFileService = generateFileService
	}
	async generateContract(contractId) {
		const contractInfo =
      await this.getContractDownloadService.getContractDownload(contractId)
		await this.generateFileService.generateFile(
			contractInfo.template,
			contractInfo,
			`CONTRACT-${contractInfo.clientName}-${contractInfo.productName}`,
			`contracts/${contractId}`
		)
	}
}
