const UnauthorizedError = require('../../common/controllers/error-handling/unauthorizedError')

module.exports = class verifyContractService {
	constructor(contractRepository) {
		this.contractRepository = contractRepository
	}
	async verifyContract(contractId) {
		const contract = await this.contractRepository.getById(contractId)
		console.log(contractId, contract?.contractId)
		if (contract) {
			return true
		}
		throw new UnauthorizedError('Invalid contract ID')
	}
}
