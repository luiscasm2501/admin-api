const ValidationException = require('../../common/domain/validationException')
const statuses = require('../../common/persistence/status/statuses')
const contractEntity = require('../domain/contractEntity')

class updateContractService {
	constructor(contractRepository, assignAffiliateService, bus) {
		this.contractRepository = contractRepository
		this.assignAffiliateService = assignAffiliateService
		this.bus = bus
	}

	async updateContract(contract) {
		contractEntity(contract, true)
		const status = await this.checkStatus(contract)
		if (contract.Affiliate) {
			await this.assignAffiliateService.updateAffiliate(contract.Affiliate)
		}

		const updated = await this.contractRepository.update(contract)
		const prepare = status.name == statuses.DRAFT && contract.startDate

		this.bus.emit('contractUpdated', {
			contractId: contract.contractId,
			prepare,
		})
		return updated
	}

	async checkStatus(contract) {
		const status = await this.contractRepository.getStatus(contract.contractId)
		if (![statuses.DRAFT, statuses.PENDING_REVIEW].includes(status.name)) {
			throw new ValidationException(
				`${status.name} status. Only drafted contracts can be modified`
			)
		}

		return status
	}
}
module.exports = updateContractService
