const { DateTime } = require('luxon')
const statuses = require('../../common/persistence/status/statuses')
const NotFoundException = require('../../common/controllers/error-handling/notFoundError')
module.exports = class signContractService {
	constructor(
		contractRepository,
		getContractsService,
		sendNotificationService,
		bus
	) {
		this.contractRepository = contractRepository
		this.getContractsService = getContractsService
		this.sendNotificationService = sendNotificationService
		this.bus = bus
	}
	async signContract(contractId) {
		const contract = await this.getContractsService.getContract(contractId)
		if (contract) {
			const updated = await this.contractRepository.update({
				contractId,
				signedDate: DateTime.now().toSQLDate(),
			})
			const client = contract.ClientProduct.Client.name
			const product = contract.ClientProduct.Product.name
			this.sendNotificationService.sendNotification({
				title: `${client} signed their ${product} contract`,
				kind: 'info',
				content: {
					description: `Under Contracts, our client ${client} has submitted their signed contract for their ${product} subscription (contract ${contractId}).`,
				},
			})
			this.bus.emit('contractSigned')

			return updated
		}
		throw new NotFoundException('contract ' + contractId)
	}
}
