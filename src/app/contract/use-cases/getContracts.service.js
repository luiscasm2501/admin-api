class GetContractsService {
	constructor(contractRepository, getContractProductService) {
		this.contractRepo = contractRepository
		this.getContractProduct = getContractProductService
	}

	async getAllContracts(options) {
		const result = await this.contractRepo.getAll({
			...options,
			include: [
				'Status',
				'ContractTemplate',
				['ContractAffiliates', ['Employee']],
				['ClientProduct', ['Client', 'Product']],
			],
		})
		return { rows: this.mapAffiliate(result.rows), count: result.count }
	}

	async getContractsByClient(clientId, options) {
		const validEntries = await this.getContractProduct.getContractProduct(
			{
				clientFk: clientId,
			},
			'clientProductId'
		)
		if (validEntries.length) {
			const res = await this.contractRepo.getAll({
				clientProductFk: validEntries,
				include: [
					'Status',
					'ContractTemplate',
					['ContractAffiliates', ['Employee']],

					['ClientProduct', ['Client', 'Product']],
				],
				...options,
			})
			return { rows: this.mapAffiliate(res.rows), count: res.count }
		}
		return { rows: [], count: [] }
	}
	async getContract(contractId, options) {
		const contract = await this.contractRepo.getById(contractId, {
			include: [
				'Status',
				'ContractTemplate',
				['ContractAffiliates', ['Employee']],
				['ClientProduct', ['Client', 'Product']],
			],
			...options,
		})
		return this.mapAffiliate(contract)
	}
	map(contract) {
		const mapped = contract.dataValues
		let ContractAffiliate = undefined
		if (contract.ContractAffiliates?.length) {
			ContractAffiliate = contract.ContractAffiliates[0].dataValues
			ContractAffiliate.Affiliate = ContractAffiliate.Employee.dataValues
			delete ContractAffiliate.Employee
			delete mapped.ContractAffiliates
		}

		mapped.ContractAffiliate = ContractAffiliate
		return mapped
	}
	mapAffiliate(contracts) {
		if (Array.isArray(contracts)) {
			return contracts.map(this.map)
		}
		return this.map(contracts)
	}
}
module.exports = GetContractsService
