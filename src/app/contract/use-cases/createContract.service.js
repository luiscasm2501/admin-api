const ValidationException = require('../../common/domain/validationException')
const contractTemplateEntity = require('../../templates/contract-template/domain/contractTemplateEntity')
const contractEntity = require('../domain/contractEntity')
class createContractService {
	constructor(
		contractRepository,
		clientProductRepository,
		assignAffiliateService,
		createStorageFolderService
	) {
		this.contractRepo = contractRepository
		this.clientProdRepo = clientProductRepository
		this.assignAffiliateService = assignAffiliateService
		this.createStorageFolderService = createStorageFolderService
	}
	async createContract(clientId, productId, contract) {
		contractEntity(contract)
		let clientProductId = await this.checkClientProduct(clientId, productId)
		clientProductId =
      typeof clientProductId === 'object'
      	? clientProductId.clientProductId
      	: clientProductId

		contract.ContractTemplate &&
      contractTemplateEntity(contract.ContractTemplate)

		const createdContract = await this.contractRepo.create({
			...contract,
			clientProductFk: clientProductId,
		})
		await this.createStorageFolderService.createStorageFolder({
			name: createdContract.contractId,
			path: 'contracts',
		})
		if (contract.Affiliate)
			try {
				await this.assignAffiliateService.assignAffiliate(
					createdContract.contractId,
					contract.Affiliate
				)
			} catch (error) {
				this.contractRepo.remove(createdContract.contractId, true)
				throw ValidationException(
					'Couldn\'t associate affiliate, try again later'
				)
			}
		return createdContract
	}

	async checkClientProduct(clientId, productId) {
		const clientProd = {
			clientFk: clientId,
			productFk: productId,
		}

		const existsRelation = await this.clientProdRepo.getAll({
			clientFk: clientId,
			productFk: productId,
		})
		const id = existsRelation.count
			? existsRelation.rows[0].clientProductId
			: await this.clientProdRepo.create(clientProd)
		return id
	}
}
module.exports = createContractService
