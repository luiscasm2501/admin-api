const { DateTime } = require('luxon')
const ValidationException = require('../../common/domain/validationException')
const statuses = require('../../common/persistence/status/statuses')
const contractEntity = require('../domain/contractEntity')

class startContractService {
	constructor(
		contractRepository,
		templateRepository,
		createAutomaticInvoicesService,
		contractAffiliateRepository,
		bus
	) {
		this.contractRepository = contractRepository
		this.contTempRepo = templateRepository
		this.createAutomaticInvoicesService = createAutomaticInvoicesService
		this.contractAffiliateRepository = contractAffiliateRepository
		this.bus = bus
	}

	async startContract(contract) {
		const today = DateTime.now()
		contract.signedDate = contract.signedDate
			? contract.signedDate
			: today.toSQL()
		if (!contract.startDate) {
			throw new ValidationException(
				'A start date must be provided to start a contract'
			)
		}
		contractEntity(contract, true)
		await this.checkStatus(contract)

		const fullContract = await this.contractRepository.update(contract)

		const template = await this.contTempRepo.getById(
			fullContract.contractTemplateFk
		)
		if (fullContract.automaticInvoice) {
			await this.createAutomaticInvoicesService.createAutomaticInvoices(
				fullContract,
				template,
				contract.deadlineDays
			)
		}
		const contractAffiliates = await fullContract.getContractAffiliates()
		const affiliateContract = contractAffiliates[0]
		if (affiliateContract) {
			await this.contractAffiliateRepository.updateStatus(
				affiliateContract.contractAffiliateId,
				statuses.ACTIVE
			)
		}
		this.bus.emit('contractUpdated', { contractId: contract.contractId })
		return await this.contractRepository.updateStatus(
			fullContract.contractId,
			statuses.APPROVED
		)
	}

	async checkStatus(contract) {
		const status = await this.contractRepository.getStatus(contract.contractId)
		if (![statuses.DRAFT, statuses.PENDING_REVIEW].includes(status.name)) {
			throw new ValidationException(
				`${status.name} status. Only DRAFT contracts can be started`
			)
		}
		return status
	}
}
module.exports = startContractService
