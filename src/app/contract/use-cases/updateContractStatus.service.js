const { DateTime } = require('luxon')
const statuses = require('../../common/persistence/status/statuses')
module.exports = class updateContractStatusService {
	constructor(
		contractRepository,
		hiringRepository,
		getStatusesService,
		contractAffiliateRepository,
		bus
	) {
		this.contractRepository = contractRepository
		this.hiringRepository = hiringRepository
		this.contractAffiliateRepository = contractAffiliateRepository
		this.getStatusesService = getStatusesService
		this.repo = undefined
		this.bus = bus
	}
	async updateContractStatus(areHirings = false) {
		const now = DateTime.now()
		this.repo = areHirings ? this.hiringRepository : this.contractRepository
		const foundStatuses = await this.getStatusesService.getStatuses()

		const startResult = await this.repo.getAll({
			startDate: { lte: now.toSQLDate() },
			statusFk: this.findStatus(foundStatuses, statuses.APPROVED),
		})
		const startContracts = startResult.rows
		const expiredResult = await this.repo.getAll({
			expirationDate: { lte: now.toSQLDate() },
			include: ['ContractAffiliates'],
			statusFk: [
				this.findStatus(foundStatuses, statuses.ACTIVE),
				this.findStatus(foundStatuses, statuses.APPROVED),
			],
		})
		const expiredContracts = expiredResult.rows

		const startedNumber = await this.handleStart(startContracts, areHirings)
		const expiredNumber = await this.handleExpired(expiredContracts, areHirings)
		return [startedNumber, expiredNumber]
	}

	async handleStart(contracts = [], isHiring) {
		contracts.forEach((contract) => {
			this.repo.updateStatus(
				contract[`${isHiring ? 'hiringId' : 'contractId'}`],
				statuses.ACTIVE
			)
		})
		return contracts.length
	}
	async handleExpired(contracts = [], isHiring) {
		contracts.forEach((contract) => {
			this.repo.updateStatus(
				contract[`${isHiring ? 'hiringId' : 'contractId'}`],
				statuses.EXPIRED
			)
			contract.ContractAffiliates.forEach((contractAff) => {
				this.contractAffiliateRepository.updateStatus(
					contractAff.contractAffiliateId,
					statuses.EXPIRED
				)
			})
		})

		return contracts.length
	}

	findStatus(statusList = [], statusName = '') {
		return statusList.find((status) => {
			return status[statusName]
		})[statusName]
	}
}
