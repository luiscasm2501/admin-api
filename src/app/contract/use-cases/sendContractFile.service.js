const { DateTime, Interval } = require('luxon')
const createToken = require('../../common/authentication/use-cases/createToken')
const { sendEmail } = require('../../common/services/mailer/mailer')
require('dotenv').config()

module.exports = class sendContractFileService {
	constructor(getContractDownloadService) {
		this.getContractDownloadService = getContractDownloadService
	}
	async sendContractFile(contractId) {
		const contractInfo =
      await this.getContractDownloadService.getContractDownload(contractId)
		const startDate = DateTime.fromSQL(contractInfo.startDate)

		const interval = Interval.fromDateTimes(DateTime.now(), startDate)

		const duration = interval.count('days') > 14 ? interval.count('days') : 14

		const token = createToken({ contract: contractId }, duration + 'd')
		contractInfo.url = `${process.env.WEB_APP_URL}/contract-sign/${contractId}/${token}`

		await sendEmail(
			contractInfo.clientEmail,
			`Your ${contractInfo.productName} contract is ready to sign / Su contrato de ${contractInfo.productName} está listo para firmar`,
			'contract_sign',
			{
				...contractInfo,
				button: 'Sign Contract / Firmar Contrato',
				title: 'Your contract is ready / Tu contrato está listo',
			},
			[
				{
					filename: contractInfo.filename + '.pdf',
					path: contractInfo.outputPath + '.pdf',
				},
			]
		)
	}
}
