const ValidationException = require('../../common/domain/validationException')
const statuses = require('../../common/persistence/status/statuses')
const NotFoundError = require('../../common/controllers/error-handling/notFoundError')
const { sendEmail } = require('../../common/services/mailer/mailer')
const createToken = require('../../common/authentication/use-cases/createToken')
require('dotenv').config()

module.exports = class preCancelContractService {
	constructor(contractRepository) {
		this.contractRepository = contractRepository
	}
	async preCancelContract(contractId) {
		const contract = await this.contractRepository.getById(contractId, {
			include: ['Status', ['ClientProduct', ['Client', 'Product']]],
		})
		if (contract) {
			if (
				![statuses.ACTIVE, statuses.APPROVED].includes(contract.Status.name)
			) {
				throw new ValidationException(
					`${contract.Status.name} status. Only active contracts can be cancelled`
				)
			}
			const token = createToken({ contract: contractId }, '30d')

			const mappedContract = {
				...contract.dataValues,
				title: 'Cancel Contract / Cancelar contrato',
				button: 'Check Contract / Revisar contrato',
				url: `${process.env.WEB_APP_URL}/contract-cancel/${contractId}/${token}`,
			}
			mappedContract.Client = mappedContract.ClientProduct.Client
			mappedContract.product = mappedContract.ClientProduct.Product.name
			await this.contractRepository.updateStatus(
				contractId,
				statuses.PENDING_CANCEL
			)
			try {
				await sendEmail(
					mappedContract.Client.email,
					`Cancelling of ${mappedContract.product} issued / Cancelación de ${mappedContract.product} emitido`,
					'cancel_contract',
					mappedContract
				)
			} catch (error) {
				await this.contractRepository.updateStatus(
					contractId,
					contract.Status.name
				)
				throw new Error('Couldn\'t send email to client')
			}

			return { status: 'PENDING-CANCEL', contractId: contractId }
		}
		throw new NotFoundError('contract ' + contractId)
	}
}
