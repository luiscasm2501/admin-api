const ValidationException = require('../../common/domain/validationException')
const statuses = require('../../common/persistence/status/statuses')

class deleteContractService {
	constructor(contractRepository, invoiceRepository) {
		this.contractRepository = contractRepository
		this.invoiceRepository = invoiceRepository
	}

	async deleteContract(contractId) {
		await this.checkStatus(contractId)

		return await this.contractRepository.remove(contractId)
	}

	async checkStatus(contractId) {
		const status = await this.contractRepository.getStatus(contractId)
		if (
			![statuses.DRAFT, statuses.PENDING_REVIEW, statuses.DELETED].includes(
				status.name
			)
		) {
			throw new ValidationException(
				`${status.name} status. Only DRAFT contracts can be deleted`
			)
		}
		return status
	}
	async restoreContract(id) {
		if (id) return await this.contractRepository.restore(id)
	}
}
module.exports = deleteContractService
