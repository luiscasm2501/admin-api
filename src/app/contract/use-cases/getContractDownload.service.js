const path = require('path')
const mapContractDownload = require('../domain/mapContractDownload')
const NotFoundError = require('../../common/controllers/error-handling/notFoundError')
const { fstat, existsSync } = require('fs')
module.exports = class getContractDownloadService {
	constructor(getContractsService, getFileTemplatesService) {
		this.getContractsService = getContractsService
		this.getFileTemplatesService = getFileTemplatesService
	}
	async getContractDownload(contractId) {
		const contract = await this.getContractsService.getContract(contractId, {
			include: ['ContractTemplate', ['ClientProduct', ['Client', 'Product']]],
		})
		const template = await this.getFileTemplatesService.getFileTemplate(
			contract.ContractTemplate.fileTemplateFk
		)

		if (!contract) {
			throw new NotFoundError('Contract')
		}
		if (!template) {
			throw new NotFoundError('Contract template')
		}
		const mappedContract = mapContractDownload(contract)
		const url = template.name
		const file_path = path.join(process.env.FILE_SRC + '/template', url)
		let filename = `CONTRACT-${mappedContract.clientName}-${mappedContract.productName}`
		const outputPath = path.join(
			process.env.FILE_SRC,
			'contracts',
			contractId,
			filename
		)

		const result = {
			...mappedContract,
			filename,
			template: file_path,
			outputPath,
		}
		try {
			if (existsSync(outputPath + '-SIGNED.pdf')) {
				result.filename += '-SIGNED'
				result.outputPath += '-SIGNED'
			}
		} catch (error) {}
		return result
	}
}
