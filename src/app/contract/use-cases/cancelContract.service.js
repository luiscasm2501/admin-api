const ValidationException = require('../../common/domain/validationException')
const statuses = require('../../common/persistence/status/statuses')

class cancelContractService {
	constructor(
		contractRepository,
		invoiceRepository,
		hiringRepository,
		contractAffiliateRepository
	) {
		this.contractRepository = contractRepository
		this.invoiceRepository = invoiceRepository
		this.hiringRepository = hiringRepository
		this.contractAffiliateRepository = contractAffiliateRepository
		this.repo = undefined
	}

	async cancelContract(contractId, isHiring = false) {
		this.repo = isHiring ? this.hiringRepository : this.contractRepository
		await this.checkStatus(contractId)
		const result = await this.invoiceRepository.getAll({
			include: [
				'Status',
				{
					model: isHiring ? 'HiringInvices' : 'ContractInvoices',
					conditions: {
						[`${isHiring ? 'hiringFk' : 'contractFk'}`]: contractId,
					},
				},
			],
		})
		const invoices = result.rows
		Array.isArray(invoices) &&
      invoices.forEach(async (invoice) => {
      	if (invoice.Status.name !== statuses.PAID) {
      		await this.invoiceRepository.updateStatus(
      			invoice.invoiceId,
      			statuses.CANCELLED
      		)
      	}
      })
		await this.contractAffiliateRepository.updateStatus(
			contractId,
			statuses.CANCELLED,
			'contractFk'
		)
		return await this.repo.updateStatus(contractId, statuses.CANCELLED)
	}

	async checkStatus(contractId) {
		const status = await this.repo.getStatus(contractId)
		if (
			![
				statuses.DRAFT,
				statuses.ACTIVE,
				statuses.APPROVED,
				statuses.PENDING_CANCEL,
			].includes(status.name)
		) {
			throw new ValidationException(
				`${status.name} status. Only DRAFT, APPROVED or ACTIVE contracts can be cancelled`
			)
		}
		return status
	}
}
module.exports = cancelContractService
