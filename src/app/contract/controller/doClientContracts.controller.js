const { createController } = require('awilix-router-core')
const UpdateError = require('../../common/controllers/error-handling/updateError')

const consultControllers = (
	getContractsService,
	updateContractService,
	startContractService,
	deleteContractService,
	cancelContractService
) => ({
	getContractsByClient: async (req, res, next) => {
		try {
			const { id } = req.params
			const result = await getContractsService.getContractsByClient(
				id,
				req.query
			)
			next(result)
		} catch (error) {
			next(error)
		}
	},
	getContractById: async (req, res, next) => {
		try {
			const { contractId } = req.params
			const result = await getContractsService.getContract(
				contractId,
				req.query
			)
			res.send(result)
		} catch (error) {
			next(error)
		}
	},
	updateContract: async (req, res, next) => {
		try {
			const paramId = req.params.contractId
			const bodyId = req.body.contractId
			if (paramId === bodyId) {
				const result = await updateContractService.updateContract(req.body)
				res.send(result)
			} else {
				next(new UpdateError(paramId, bodyId))
			}
		} catch (error) {
			next(error)
		}
	},
	startContract: async (req, res, next) => {
		try {
			const paramId = req.params.contractId
			const bodyId = req.body.contractId
			if (paramId === bodyId) {
				const result = await startContractService.startContract(req.body)
				res.send(result)
			} else {
				next(new UpdateError(paramId, bodyId))
			}
		} catch (error) {
			next(error)
		}
	},
	cancelContract: async (req, res, next) => {
		try {
			const paramId = req.params.contractId
			const result = await cancelContractService.cancelContract(paramId)
			res.send(result)
		} catch (error) {
			next(error)
		}
	},
	removeContract: async (req, res, next) => {
		try {
			const id = req.params.contractId
			const result = await deleteContractService.deleteContract(id)
			if (result) {
				res.send(`Contract ${id} succesfully marked as deleted`)
			} else {
				next(new NotFoundError(`Contract ${id}`))
			}
		} catch (error) {
			next(error)
		}
	},
	restoreContract: async (req, res, next) => {
		try {
			const id = req.params.contractId
			const result = await deleteContractService.restoreContract(id)
			if (result) {
				res.send(`Contract ${id} succesfully restored`)
			} else {
				next(new NotFoundError(`Contract ${id}`))
			}
		} catch (error) {
			next(error)
		}
	},
})

module.exports = createController(consultControllers)
	.prefix('/clients/:id/contracts')
	.get('', 'getContractsByClient')
	.get('/:contractId', 'getContractById')
	.put('/:contractId', 'updateContract')
	.put('/:contractId/started', 'startContract')
	.put('/:contractId/canceled', 'cancelContract')
	.delete('/:contractId', 'removeContract')
	.patch('/:contractId', 'restoreContract')

/**
 * @swagger
 * /clients/{clientId}/contracts:
 *  get:
 *    summary: get a list of contracts of a client
 *    parameters:
 *      - $ref: '#/components/parameters/clientId'
 *      - $ref: '#/components/parameters/page'
 *      - $ref: '#/components/parameters/sort'
 *    tags: [Contracts]
 *    responses:
 *      200:
 *        description: list of contracts by client
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/Contract'
 *              type: array
 * 
 */
