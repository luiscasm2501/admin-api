const { createController } = require('awilix-router-core')

const registerControllers = (createContractService) => ({
	createContract: async (req, res, next) => {
		try {
			const { prodId, clientId } = req.params
			const result = await createContractService.createContract(
				clientId,
				prodId,
				req.body
			)
			res.send(result)
		} catch (error) {
			next(error)
		}
	},
})

module.exports = createController(registerControllers)
	.prefix('/clients/:clientId/contracts/products/:prodId')
	.post('', 'createContract')

/**
 * @swagger
 * /clients/{clientId}/contracts/products/{productId}:
 *  post:
 *    summary: create a contract between a client and a product
 *    parameters:
 *      - $ref: '#/components/parameters/clientId'
 *      - $ref: '#/components/parameters/productId'
 *    tags: [Contracts]
 *    requestBody:
 *      description: to submit a new contract only total payment, contract template and automatic invoice fields a required
 *      required: true
 *      content:
 *        application/json:
 *          schema:
 *            $ref: '#/components/schemas/Contract'
 *    responses:
 *      200:
 *        description: created contract record
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/Contract'
 */
