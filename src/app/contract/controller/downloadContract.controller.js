const { createController } = require('awilix-router-core')
const { DateTime } = require('luxon')
const path = require('path')
const registerControllers = (
	getContractDownloadService,
	generateThenZipService
) => ({
	downloadContract: async (req, res, next) => {
		try {
			const { contractId } = req.params
			const fileInfo = await getContractDownloadService.getContractDownload(
				contractId
			)

			res.download(fileInfo.outputPath + '.pdf')
		} catch (error) {
			next(error)
		}
	},
})

module.exports = createController(registerControllers).get(
	'/contracts/:contractId/download',
	'downloadContract'
)

/**
 * @swagger
 * /contracts/{contractId}/download:
 *  get:
 *    tags: [Contracts]
 *    summary: download this contract's zip with all information included
 *    parameters:
 *      - $ref: '#/components/parameters/contractId'
 *    responses:
 *      200:
 *        description: contract zip (containing doc and pdf)
 *        content:
 *          application/pdf:
 *            schema:
 *              type: string
 *              format: binary
 *
 */
