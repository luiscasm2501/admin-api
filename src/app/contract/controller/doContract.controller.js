const { createController } = require('awilix-router-core')

const UpdateError = require('../../common/controllers/error-handling/updateError')
const consultControllers = (
	getContractsService,
	updateContractService,
	startContractService,
	preCancelContractService,
	cancelContractService,
	logoutUserService,
	deleteContractService
) => ({
	getAllContracts: async (req, res, next) => {
		try {
			const result = await getContractsService.getAllContracts(req.query)
			next(result)
		} catch (error) {
			next(error)
		}
	},
	getContractById: async (req, res, next) => {
		try {
			const { id } = req.params
			const result = await getContractsService.getContract(id, req.query)
			res.send(result)
		} catch (error) {
			next(error)
		}
	},
	updateContract: async (req, res, next) => {
		try {
			const paramId = req.params.id
			const bodyId = req.body.contractId
			if (paramId === bodyId) {
				const result = await updateContractService.updateContract(req.body)
				res.send(result)
			} else {
				next(new UpdateError(paramId, bodyId))
			}
		} catch (error) {
			next(error)
		}
	},
	startContract: async (req, res, next) => {
		try {
			const paramId = req.params.id
			const bodyId = req.body.contractId
			if (paramId === bodyId) {
				const result = await startContractService.startContract(req.body)
				res.send(result)
			} else {
				next(new UpdateError(paramId, bodyId))
			}
		} catch (error) {
			next(error)
		}
	},
	cancelContract: async (req, res, next) => {
		try {
			const paramId = req.params.id
			const result = await cancelContractService.cancelContract(paramId)
			const token = req.headers.authorization.split(' ')[1]
			logoutUserService.logoutUser(token)
			res.send(result)
		} catch (error) {
			next(error)
		}
	},
	preCancelContract: async (req, res, next) => {
		try {
			const paramId = req.params.id
			const result = await preCancelContractService.preCancelContract(paramId)
			res.send(result)
		} catch (error) {
			next(error)
		}
	},
	removeContract: async (req, res, next) => {
		try {
			const id = req.params.id
			const result = await deleteContractService.deleteContract(id)
			if (result) {
				res.send(`Contract ${id} succesfully marked as deleted`)
			} else {
				next(new NotFoundError(`Contract ${id}`))
			}
		} catch (error) {
			next(error)
		}
	},
	restoreContract: async (req, res, next) => {
		try {
			const id = req.params.id
			const result = await deleteContractService.restoreContract(id)
			if (result) {
				res.send(`Contract ${id} succesfully restored`)
			} else {
				next(new NotFoundError(`Contract ${id}`))
			}
		} catch (error) {
			next(error)
		}
	},
})

module.exports = createController(consultControllers)
	.prefix('/contracts')
	.get('', 'getAllContracts')
	.get('/:id', 'getContractById')
	.put('/:id', 'updateContract')
	.put('/:id/started', 'startContract')
	.put('/:id/canceled', 'cancelContract')
	.put('/:id/pre-cancel', 'preCancelContract')
	.delete('/:id', 'removeContract')
	.patch('/:id', 'restoreContract')

/**
 * @swagger
 * /contracts:
 *  get:
 *    summary: get a list from all contracts
 *    parameters:
 *      - $ref: '#/components/parameters/page'
 *      - $ref: '#/components/parameters/sort'
 *    tags: [Contracts]
 *    responses:
 *      200:
 *        description: list of contracts
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/Contract'
 *              type: array
 *
 * /contracts/{contractId}:
 *  get:
 *    summary: get contract information by id
 *    parameters:
 *      - $ref: '#/components/parameters/contractId'
 *    tags: [Contracts]
 *    responses:
 *      200:
 *        description: contract record, if found
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/Contract'
 *  put:
 *    summary: update a contract record by id
 *    description: please note that only contracts with 'DRAFT' or 'PENDING REVIEW' statuses can be modified. Once a contract is approved, modifications are disabled.
 *    parameters:
 *      - $ref: '#/components/parameters/contractId'
 *    tags: [Contracts]
 *    requestBody:
 *      required: true
 *      content:
 *        application/json:
 *          schema:
 *            $ref: '#/components/schemas/Contract'
 *    responses:
 *      200:
 *        description: updated contract record
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/Contract'
 *  delete:
 *    summary: deletes a contract that's still in the drafts
 *    description: marks as "deleted" first, then, permanently deletes the record if called again
 *    parameters:
 *      - $ref: '#/components/parameters/contractId'
 *    tags: [Contracts]
 *    responses:
 *      200:
 *        description: contract record marked as deleted
 *  patch:
 *    summary: returns a contract record marked as deleted  to its original status
 *    parameters:
 *      - $ref: '#/components/parameters/contractId'
 *    tags: [Contracts]
 *    responses:
 *      200:
 *        description: contract record restored
 *
 * /contracts/{contractId}/started:
 *  put:
 *    summary: approve a contract
 *    description: please note that only contracts with 'DRAFT' or 'PENDING REVIEW' statuses can be approved. Once a contract is approved, modifications are disabled.
 *    parameters:
 *      - $ref: '#/components/parameters/contractId'
 *    tags: [Contracts]
 *    requestBody:
 *      required: true
 *      description: to submit, only start and expiration dates are required, as well as contract id
 *      content:
 *        application/json:
 *          schema:
 *            $ref: '#/components/schemas/Contract'
 *    responses:
 *      200:
 *        description: started contract record
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/Contract'
 *
 * /contracts/{contractId}/canceled:
 *  put:
 *    summary: cancel a contract that's been approved
 *    description: only contracts with 'APPROVED' or 'ACTIVE' statuses can be canceled. Once this action is performed, it cannot be backtracked, so be cautious!
 *    tags: [Contracts]
 *    parameters:
 *      - $ref: '#/components/parameters/contractId'
 *    responses:
 *      200:
 *        description: contract record canceled
 */
