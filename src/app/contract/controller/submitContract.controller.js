const { createController } = require('awilix-router-core')
const ValidationException = require('../../common/domain/validationException')
const upload = require('../../files/config/files.config')

const registerControllers = (signContractService) => ({
	createContract: async (req, res, next) => {
		try {
			if (!req.file) {
				throw new ValidationException('A contract file is required')
			}
			const result = await signContractService.signContract(
				req.params.contractId
			)
			next(result)
		} catch (error) {
			next(error)
		}
	},
})

module.exports = createController(registerControllers)
	.prefix('/contracts/:contractId/sign')
	.put('', 'createContract')
	.before(upload.single('contract'))
